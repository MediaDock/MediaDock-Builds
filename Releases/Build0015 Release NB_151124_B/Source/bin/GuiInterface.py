# -*- coding: utf-8 -*-

__author__ = 'Peter'


import codecs
import os
import time
import json
from PySide.QtCore import QObject, Slot
import re
import sys

import JsScripts
import MDConst

#relpath_index_html = u'index.html'
#abspath_index_html = os.path.abspath(relpath_index_html).replace('\\', '/')
#relpath_mikro_ordner_png = u'static/mikro_ordner.png'
#abspath_mikro_ordner_png = os.path.abspath(relpath_mikro_ordner_png).replace('\\', '/')
#relpath_start_html = u'static/start.html'
#relpath_scripts_html = u'static/scripts.html'

class GuiInterface(object):
    def __init__(self, master):
        self.master = master
        self.ps = PServer(self)
        self.jsi = JSInterface(self)
        self.pah = PageHandler(self, self.ps)


class JSInterface(object):
    def __init__(self, master):
        self.master = master
        self.return_values = {}

    def executeJs(self, js, return_value=False, timeout=10):
        if not return_value:
            return_id = None
        else:
            i = 0
            while self.return_values.has_key(i):
                i += 1
            return_id = i
            self.return_values[return_id] = NoClass()
        self.master.master.b.execJS(js, return_id, self.return_values)
        if not return_id is None:
            wait_time = 0.001
            time_counter = int(timeout / wait_time)
            for x in range(time_counter):
                time.sleep(wait_time)
                value = self.return_values[return_id]
                if not isinstance(value, NoClass):
                    del self.return_values[return_id]
                    return value
            raise

class Page(object):

    replacers = {}
    for root, dirs, files in os.walk(u'static'):
        #print root, dirs, files
        for i in files:
            relpath = os.path.join(root, i)
            abspath = os.path.abspath(relpath)
            replacers[relpath] = u'file:///' + abspath
    replacers2 = dict()
    for key, value in replacers.iteritems():
        replacers2[key.replace('\\', '/')] = value.replace('\\', '/')
    '''
    replacers = {
            relpath_index_html: u'file:///' + abspath_index_html,
            relpath_mikro_ordner_png : u'file:///' + abspath_mikro_ordner_png
        }
    '''

    def __init__(self,name, filename, needServer=False, jsFiles=None, jsInit=None):
        if jsFiles is None:
            jsFiles = ()
        if jsInit is None:
            self.jsInit = ''
        else:
            self.jsInit = jsInit
        self.name = name
        self.needServer = needServer
        self.jsFiles = jsFiles

        html_file = codecs.open(filename, 'r', 'utf-8')
        content = html_file.read()
        html_file.close()
        #print re.search('<script>.*</script>', content)

        for key, value in self.replacers.iteritems():
            content = content.replace(key, value)
        for key, value in self.replacers2.iteritems():
            content = content.replace(key, value)

        self.html = content

class PageHandler(object):
    pages = {
        'index': Page('index', u'index.html', jsFiles=[JsScripts.MediaDockLibrary, JsScripts.modalMessages,
                                                       JsScripts.check_locked,
                                                       JsScripts.calendar, JsScripts.session],
                      jsInit=JsScripts.index_init),
        'menu': Page('menu', u'menu.html', jsInit=JsScripts.menu_init),
        'about': Page('about', u'about.html', jsInit=JsScripts.about_init),
        'collections': Page('collections', u'collections.html', jsInit=JsScripts.collections_init),
        'default': Page('default', u'default.html', jsInit=JsScripts.default_init),
        'drives': Page('drives', u'drives.html', jsInit=JsScripts.drives_init),
        'explorer': Page('explorer', u'explorer.html', jsInit=JsScripts.explorer_init),
        'fsm': Page('fsm', u'fsm.html', jsInit=JsScripts.fsm_init),
        'info': Page('info', u'info.html', jsInit=JsScripts.info_init),
        'license': Page('license', u'license.html', jsInit=JsScripts.license_init),
        'lock': Page('lock', u'lock.html', jsInit=JsScripts.lock_init),
        'messages': Page('messages', u'messages.html', jsInit=JsScripts.messages_init),
        'search': Page('search', u'search.html', jsInit=JsScripts.search_init),
        'service': Page('service', u'service.html', jsInit=JsScripts.service_init),
        'settings': Page('settings', u'settings.html', jsInit=JsScripts.settings_init),
        'statistics': Page('statistics', u'statistics.html', jsInit=JsScripts.statistics_init),
        'terms_of_use': Page('terms_of_use', u'terms_of_use.html', jsInit=JsScripts.terms_or_use_init)
    }
    def __init__(self, master, server):
        self.master = master
        #self.browser = master.master.b
        self.server = server
        self.previous = []
        self.next = [] # todo replace by other name
        self.history = list()
        self.page = 'index'

    def loadPage(self, par_page):
        if not par_page in self.pages.keys():
            # todo error message
            page = 'index'
        page = self.pages[par_page]
        #self.master.master.b.execJS('clear();')
        self.master.master.b.setContent(page.html)
        self.master.master.b.b.html_viewer.page().mainFrame().addToJavaScriptWindowObject('pserver', self.server)
        #time.sleep(2)
        '''
        for x in page.jsFiles:
            #print x
            self.master.master.b.execJS(x)
        '''
        for i in page.jsInit.split('\n'):
           self.master.jsi.executeJs(i, return_value=True), i
           #print self.master.jsi.executeJs(i, return_value=True), i

        self.page = page.name

    def loadStartPage(self):
        self.loadPage('index')

    def loadPrevious(self):
        pass

    def loadNext(self):
        pass

class NoClass(object): pass

class PServer(QObject):
    def __init__(self, master):
        super(PServer, self).__init__()
        self.master = master
        self.ei = master.master.e.si
        self.client_address = ['127.0.0.1']

    def encodeAjax(self, call_str):
        '''print call_str, 'call str'
        for i in call_str:
            print i
        if not '&' in call_str and not '=' in call_str:
            return {'function': call_str}
        d = dict()
        for i in call_str.split('&'):
            data = i.split('=')
            d[data[0]] =  data[1]'''
        d =  json.loads(call_str)
        #print 'd', d, type(d)
        if not isinstance(d, dict):
            raise
            d = {'function': d}
        if not d.has_key('function'):
            raise
        #print 'd', d, type(d)
        return d

    @Slot(str, str)
    def send(self, func, args):
        #print args
        #args = json.loads(args)
        #print type(args)
        if not MDConst.FROZEN:
            try:
                print 'request ', func, args
            except:
                print 'request unknown'
        if func == 'getTime':
            pass
        elif func == 'print':
            print args
        elif func == 'loadPage':
            self.master.pah.loadPage(args)
        elif func == 'execJs':
            #print 'execJs text', args
            if not  MDConst.FROZEN:
                temp = self.master.jsi.executeJs('alert(%s)' % args, True)
            #print 'execJs', temp
        elif func == 'log':
            data = json.loads(args)
            self.master.master.l.log('GuiInterface', data['message'], data['level'])
        elif func == 'ajax':
            parameters = self.encodeAjax(args)
            #print parameters
            json_answer = json.dumps({'state': 1})
            func = parameters['function']
            if func == 'getVolumes':
                json_answer = json.dumps({"status": 1, "info": self.ei.getDriveMessagesSidebar()})
            elif func == 'getMessages':
                json_answer = json.dumps({"status": 1, "info": self.ei.getMessageMessagesSidebar()})
            elif func == 'getModalMessages':
                info =  self.ei.getModalMessages(self.client_address[0])
                json_answer = json.dumps({"status": 1, 'info': info})
            elif func == 'getJobs':
                json_answer = json.dumps({"status": 1, "info": self.ei.getJobMessagesSidebar()})
            elif func == 'getDrives':
                json_answer = json.dumps({"status": 1, "info": self.ei.getDrives()})
            elif func == 'getDriveMessagesDrive':
                json_answer = json.dumps({"status": 1, "info": self.ei.getDriveMessagesDrives()})
            elif func == 'getExplorerDropdown':
                json_answer = json.dumps({"status": 1, "info": self.ei.getExplorerDropdown()})
            elif func == 'explorerGetDir':
                info, path_above, record_data = self.ei.explorerGetDir(parameters['path'])
                json_answer = json.dumps({"status": 1, "info": info, "prev_path": path_above,
                                          "record_data": record_data})
            elif func.startswith('eject_drive'):
                if self.client_address[0] == '127.0.0.1':
                    self.ei.ejectDrive(func.split('_')[2])
                json_answer = json.dumps({"status": 1, "info": ""})
            elif func == 'quit':
                if self.client_address[0] == '127.0.0.1':
                    self.ei.quit(self.client_address[0])
                json_answer = json.dumps({'status': 1})
            elif func == 'shutdown':
                self.ei.shutdown(self.client_address[0])
                #self.ei.master.master.start_shutdown_thread()
                json_answer = json.dumps({'status': 1})
            elif func == 'reload_filesettings':
                self.ei.master.fh.load_filesettings()
                json_answer = ''
            elif func == 'getCompoundData':
                json_answer = json.dumps({"status": 1, "info": self.ei.getCompoundData()})
            elif func == 'lock':
                self.ei.lock(self.client_address[0])
                json_answer = json.dumps({"status": 1})
            elif func == 'unlock':
                self.ei.unlock(self.client_address[0])
                json_answer = json.dumps({'status': 1})
            elif func == 'getRecord':
                json_answer = json.dumps({"status": 1, "info": self.ei.getRecord(parameters['date'])})
            elif func == 'getAllRecords':
                json_answer = json.dumps({"status": 1, "info": self.ei.getAllRecords()})
            elif func == 'startRecordJob':
                if self.client_address[0] == '127.0.0.1':
                    records_list = parameters['records']
                    check_result = self.ei.checkRecordJob(int(parameters['drive_id']), records_list)
                    #if check_result[0]:
                    self.ei.startRecordJob(int(parameters['drive_id']), records_list)
                    json_answer = json.dumps({"status": 1, 'check': str(check_result[0]), 'message': check_result[1]})
                else:
                    json_answer = json.dumps({'status': 1, 'check': 'False', 'message': u'Aufträge können nur vom '
                                                                                        'lokalhost gestartet werden.'})
            elif func == 'getAllMessages':
                json_answer = json.dumps({"status": 1, "info": self.ei.getAllMessages()})
            elif func == 'cancelRecordJob':
                if self.client_address[0] == '127.0.0.1':
                    try:
                        drive_id_int = int(parameters['drive_id'])
                    except:
                        answer = False
                    else:
                        answer = self.ei.cancelRecordJob(drive_id_int)
                    json_answer = json.dumps({"status": 1, "info": answer})
                else:
                    json_answer = json.dumps({'status': 1, 'info': False})
            elif func == 'deleteDrive':
                try:
                    drive_id_int = int(parameters['drive_id'])
                except:
                    check_result = False, 'Fehler in der Laufwerks-ID'
                else:
                    check_result = self.ei.checkDelete(drive_id_int)
                    if check_result[0]:
                        self.ei.deleteDrive(drive_id_int)
                json_answer = json.dumps({"status": 1, 'check': check_result[0], 'message': check_result[1]})
            elif func == 'stopDeleteDrive':
                if self.client_address[0] == '127.0.0.1':
                    try:
                        drive_id_int = int(parameters['drive_id'])
                    except:
                        pass
                    else:
                        self.ei.stopDeleteDrive(drive_id_int)
                json_answer = json.dumps({"status": 1})
            elif func == 'reload_dh_settings':
                self.ei.reloadDhSettings()
                json_answer = json.dumps({"status": 1})
            elif func == 'reload_time_settings':
                self.ei.reloadTimeSettings()
                json_answer = json.dumps({"status": 1})
            elif func == 'isLocked':
                ans = False
                if True: #todo
                #if self.client_address[0] == '127.0.0.1':
                    ans = self.ei.isLocked()
                json_answer = json.dumps({"status": 1, 'info': ans})
            elif func == 'reload_records':
                self.ei.reloadRecords()
                json_answer = json.dumps({"status": 1})
            elif func == 'getAllInfos':
                json_answer = json.dumps({"status": 1, 'info': self.ei.getAllInfos()})
            elif func == 'set_auto_shutdown':
                self.ei.set_auto_shutdown(self.client_address[0])
                json_answer = json.dumps({'status': 1})
            elif func == 'unset_auto_shutdown':
                self.ei.unset_auto_shutdown(self.client_address[0])
                json_answer = json.dumps({'status': 1})
            elif func == 'set_output_file':
                self.ei.set_logger_output(self.client_address[0], MDConst.LOGGERFILE)
                json_answer = json.dumps({'status': 1})
            elif func == 'set_output_stdout':
                self.ei.set_logger_output(self.client_address[0], MDConst.LOGGERSTDOUT)
                json_answer = json.dumps({'status': 1})
            elif func == 'set_output_file_stdout':
                self.ei.set_logger_output(self.client_address[0], MDConst.LOGGERFILESTDOUT)
                json_answer = json.dumps({'status': 1})
            elif func == 'logOn':
                result = self.ei.logOn(self.client_address[0], parameters['name'], parameters['password'])
                json_answer = json.dumps({'status': 1, 'result': result})
            elif func == 'logOff':
                self.ei.logOff(self.client_address[0])
                json_answer = json.dumps({'status': 1})
            elif func == 'checkSession':
                result = self.ei.checkSession(self.client_address[0])
                json_answer = json.dumps({'status': 1, 'info': result})
            elif func == 'onLocalhost':
                json_answer = json.dumps({'status': 1, 'info': self.client_address[0] == '127.0.0.1'})
            elif func == 'checkState':
                json_answer = json.dumps({'status': 1, 'html': self.ei.get_state_html()})
            elif func == 'get_fsm_state':
                json_answer = json.dumps({'status': 1, 'state': self.ei.get_fsm_state()})
            elif func == 'get_fsm_image_path':
                state = self.ei.get_fsm_state()
                run_path = os.path.abspath('.')
                path = 'file:///' + run_path
                if state in range(11):
                    path = os.path.join(path, 'static/images/fsm/fsm%i.svg' % state)
                else:
                    path = os.path.join(path, 'static/images/fsm/fsm.svg')
                json_answer = json.dumps({'path': path})
            elif func == 'cancel_sd_start':
                self.ei.cancel_sd_start()
                json_answer = json.dumps({'status': 1})
            elif func == 'cancel_sd_timer':
                self.ei.cancel_sd_timer()
                json_answer = json.dumps({'status': 1})
            elif func == 'cancel_sd':
                self.ei.cancel_sd()
                json_answer = json.dumps({'status': 1})
            elif func == 'cancel_quit':
                self.ei.cancel_quit()
                json_answer = json.dumps({'status': 1})
            elif func == 'set_run':
                self.ei.set_run(self.client_address[0])
                json_answer = json.dumps({'status': 1})
            elif func == 'get_records_stat':
                info = self.ei.get_records_stat(self.client_address[0])
                json_answer = json.dumps({'status': 1, 'info': info})
            elif func == 'get_files_stat':
                info = self.ei.get_files_stat(self.client_address[0])
                json_answer = json.dumps({'status': 1, 'info': info})
            elif func == 'get_calendar_stat':
                info = self.ei.get_calendar_stat(self.client_address[0])
                json_answer = json.dumps({'status': 1, 'check': info[0], 'info': info[1]})
            elif func == 'get_records_stat_month_table':
                info = self.ei.get_records_stat_month_table(self.client_address[0], parameters['month'], parameters['year'])
                json_answer = json.dumps({'status': 1, 'info': info})
            elif func == 'get_root_dir':
                if self.client_address[0] == '127.0.0.1':
                    self.ei.getRootDir() # todo just for local host
                json_answer = json.dumps({'status': 1})
            elif func == 'get_general_settings':
                json_answer = json.dumps({'status': 1})
            elif func == 'get_timer_settings':
                json_answer = json.dumps({'status': 1})
            elif func == 'get_files_settings':
                json_answer = json.dumps({'status': 1})
            elif func == 'check_general_settings':
                json_answer = json.dumps({'status': 1})
            elif func == 'check_timer_settings':
                json_answer = json.dumps({'status': 1})
            elif func == 'check_files_settings':
                json_answer = json.dumps({'status': 1})
            elif func == 'set_general_settings':
                json_answer = json.dumps({'status': 1})
            elif func == 'set_timer_settings':
                json_answer = json.dumps({'status': 1})
            elif func == 'set_files_settings':
                json_answer = json.dumps({'status': 1})
            elif func == 'get_users_data':
                # todo
                json_answer = json.dumps({})
            elif func == 'getChurches':
                json_answer = json.dumps({'info': self.ei.getChurches()})
            elif func == 'showNormal':
                self.ei.showNormal(self.client_address[0])
                json_answer = json.dumps({'status': 1})
            elif func == 'showFullScreen':
                self.ei.showFullScreen()
                json_answer = json.dumps({'status': 1})
            else:
                self.ei.master.l.logPServer(u'Server - Unbekannte Anfrage: ' + unicode(parameters),
                                           logLevel=MDConst.LOG_INFO)
                json_answer = json.dumps({"status": 1, "info": "Unbekannte Anfrage"})
            #print json_answer
            #return json_answer
            #print parameters, 'parameters'
            if parameters.has_key('return_id'):
                #print 'return now'
                js_string = 'return_object[%s] = %s;' % (parameters['return_id'], json_answer)
                #print js_string
                self.master.jsi.executeJs(js_string)

        else :
            print 'pserver unknown function', func, args
            pass # todo message

