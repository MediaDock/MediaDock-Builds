now = new Date();
calendar_year = now.getFullYear();
calendar_month = now.getMonth() + 1;
calendar_data = [];

var monthTable = {  1: 'Januar',
                    2: 'Februar',
                    3: 'März',
                    4: 'April',
                    5: 'Mai',
                    6: 'Juni',
                    7: 'Juli',
                    8: 'August',
                    9: 'September',
                    10: 'Oktober',
                    11: 'November',
                    12: 'Dezember'};

function getRecordsStat(){
    data = getJsonData({'function': 'get_records_stat'}, true);
    document.getElementById("records_stat").innerHTML = data.info;
}

function getFilesStat(){
    data = getJsonData({'function': 'get_files_stat'}, true);
    document.getElementById("files_stat").innerHTML = data.info;
    }

function getCalendarStat(){
    data = getJsonData({'function': 'get_calendar_stat'}, true);
    if (data.check){
        calendar_data = data.info;
        document.getElementById("calendar_stat").innerHTML = ''
    }
    else{
        calendar_data = [];
        document.getElementById("calendar_stat").innerHTML = data.info
    }
    calendar_plot(calendar_month, calendar_year)
}

function getStats(){
    getRecordsStat();
    getFilesStat();
    getCalendarStat();
}

function calendar_set_today() {
    now = new Date();
    calendar_year = now.getFullYear();
    calendar_month = now.getMonth() + 1;
    calendar_plot(calendar_month, calendar_year);
}

function calendar_month_plus(){
    now = new Date();
    if (!(calendar_year >= now.getFullYear() && calendar_month >= now.getMonth() + 1)){
        if (calendar_month >= 12){
            calendar_month = 1;
            calendar_year++;
        }
        else{
            calendar_month++;
        }
    }
    calendar_plot(calendar_month, calendar_year);
}

function calendar_month_minus(){
    calendar_month--;
    if (calendar_month <= 0){
        calendar_month = 12;
        calendar_year--;
    }
    calendar_plot(calendar_month, calendar_year);
}

function calendar_year_plus(){
    now = new Date();
    if (! (calendar_year >= now.getFullYear())){
        calendar_year++;
    }
    calendar_plot(calendar_month, calendar_year);
}

function calendar_year_minus(){
    calendar_year--;
    calendar_plot(calendar_month, calendar_year);
}

function calendar_plot(month, year) {
    now = new Date();
    if (year > now.getFullYear()){
        year = now.getFullYear();
        if (month > now.getMonth() + 1){
            month = now.getMonth() + 1;
        }
    }
    year_before = year;
    month_before = month - 1;
    if (month_before == 0){
        month_before = 12;
        year_before--;
    }
    document.getElementById("label-year").innerHTML = calendar_year.toString();
    document.getElementById('label-month').innerHTML = monthTable[calendar_month];
    $.plot("#stat-calendar-placeholder", [calendar_data], {
        xaxis: {
            mode: "time",
            timeformat: "%e%a",
            dayNames: ['So', 'Mo', 'Di', 'Mi', 'Do', 'Fr', 'Sa'],
            tickSize: [1, "day"],
            min: (new Date(year_before, month_before, 1)).getTime(),
            max: (new Date(year, month, 1)).getTime()
        },
        series: {
            bars: {
                show: true,
                barWidth: 24 * 60 * 60 * 1000,
                align: "right"
            }
        }
    });
    getRecordsStatMonthTable(calendar_month, calendar_year);
}

function getRecordsStatMonthTable(month, year){
    data = getJsonData({'function': 'get_records_stat_month_table', 'month': month, 'year': year}, true);
    document.getElementById("records_stat_month_table").innerHTML = data.info;
}

getStats();
calendar_set_today();