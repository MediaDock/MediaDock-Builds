var modal_counter = 0;

function showNewModal(message, levelName, level){
    var modal_bg_color = '#FFFFFF';
    switch(level) {
    case 1: //Info
        modal_bg_color = 'white';
        break;
    case 2:  // Warnung
        modal_bg_color = 'orange';
        break;
    case 3: // Fehler
        modal_bg_color = 'red';
        break;
    default:
        break;
    }
	var modal_text = '<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" ><div class="modal-dialog modal-lg"><div class="modal-content" style="background-color:'
	                 + modal_bg_color
	                 + '"><div class="modal-header"><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><h4 class="modal-title" id="myModalLabel">'
                     + levelName + '</h4></div><div class="modal-body">'
                     + '<div align="center">'
                     + message
                     + '</div>'
                     + '<div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button></div></div></div></div>';
    modal_counter++;
    $(modal_text).modal();
    modal_text = '';
}

function getModalMessages(){
    data = getJsonData({'function': 'getModalMessages'}, true);
    var modal_messages_loop_counter = 0;
    for(;modal_messages_loop_counter < data.info.length; modal_messages_loop_counter++){
        showNewModal(data.info[modal_messages_loop_counter].message,
                     data.info[modal_messages_loop_counter].levelName,
                     data.info[modal_messages_loop_counter].level)
    }
}

setInterval(getModalMessages, 1000);