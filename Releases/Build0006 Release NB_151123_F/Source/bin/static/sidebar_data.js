function ajax_request(parurl, pardata, callback) {
    data = getJsonData(pardata, true);
    callback(data);
}

function get_volumes() {
    data = getJsonData({'function': 'getVolumes'}, true);
    handle_volumes(data);
}

function get_jobs() {
    data = getJsonData({'function': 'getJobs'}, true);
    handle_jobs(data);
}

function get_messages() {
    data = getJsonData({'function': 'getMessages'}, true);
    handle_messages(data);
}

function handle_volumes(data) {
    var elem = document.getElementById('volumes');
    elem.innerHTML = data.info;
}

function handle_jobs(data) {
    var elem = document.getElementById('jobs');
    elem.innerHTML = data.info;
}

function handle_messages(data) {
    var elem = document.getElementById('messages');
    elem.innerHTML = data.info;
}

get_volumes();
get_jobs();
get_messages();
setInterval(get_volumes, 3000);
setInterval(get_jobs, 3000);
setInterval(get_messages, 3000);
