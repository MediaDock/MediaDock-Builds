function checkSession(){
    data = getJsonData({'function': 'checkSession'}, true);
    var elem_state = document.getElementById("session-state");
    var elem_name = document.getElementById("session-name");
    if (data.info == ''){
        elem_name.innerHTML = '';
        elem_state.innerHTML = '<a onclick="logInModal()"> Einloggen </a>';
    }
    else{
        elem_name.innerHTML = '<a>Eingeloggt als: ' + data.info + '</a>';
        elem_state.innerHTML = '<a onclick="logOut()">  Ausloggen </a>';
    }
}

var session_modal_counter = 0;

function logInModal(){
    var session_modal_text = '<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-dialog modal-lg"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><h4 class="modal-title" id="myModalLabel">'
                     + 'Einloggen </h4></div><div class="modal-body"><div class="form-group"><label for="inputName">Name</label><input type="text" class="form-control" id="inputNameSession'
                     + session_modal_counter + '" placeholder="Name"></div><div class="form-group"><label for="exampleInputPassword1">Passwort</label><input type="password" class="form-control" id="inputPasswordSession'
                     + session_modal_counter + '" placeholder="Passwort"></div>'
                     + '<div align="center"><div id="keyboard'
                     + session_modal_counter
                     + '"></div></div>'
                     + '<script type="text/javascript">'
                     + 'var kb = new keyboard("keyboard'
                     + session_modal_counter
                     + '", { width: 40, height: 40, keymap: ' + "'" + 'ge' + "'" + ' });'
                     + 'kb.add_keymap(' + "'" + 'ge' + "'" + ', maps[' + "'" + 'ge' + "'" + ']); '
                     + 'kb.show();</script>'
                     + '</div><div class="modal-footer"><button type="button" class="btn btn-default" '
                     + 'data-dismiss="modal" onclick="logOn('
                     + session_modal_counter + ')">Abschicken</button><button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button></div></div></div></div>';
    session_modal_counter++;
    $(session_modal_text).modal();
    session_modal_text = '';
}

function logOn(var_session_modal_counter){
    data = getJsonData({ 'function': 'logOn',
                        'name': document.getElementById('inputNameSession' + var_session_modal_counter).value,
                        'password': document.getElementById('inputPasswordSession' + var_session_modal_counter).value}
        , true);

    if(! data.result[0]){
        showNewModal(data.result[1], 'Info', 1);
    }
}

function logOut(){
    getJsonData({'function': 'logOff'});
    checkSession();
}

function send_ajax_request(var_function){
    getJsonData({'function': var_function});
}

function checkState(){
    data = getJsonData({'function': 'checkState'}, true);
    elem = document.getElementById('timer_state');
    elem.innerHTML = data.html;
}

checkSession();
setInterval(checkSession, 1000);
checkState();
setInterval(checkState, 1000);