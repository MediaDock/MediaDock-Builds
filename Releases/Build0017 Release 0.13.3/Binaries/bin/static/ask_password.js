var modal_counter = 0;

function ask_password_func(title, var_function){

var modal_text = '<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-dialog modal-lg"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><h4 class="modal-title" id="myModalLabel">'
                     + title + '</h4></div><div class="modal-body"><div class="form-group"><label for="inputName">Name</label><input type="text" class="form-control" id="inputName'
                     + modal_counter + '" placeholder="Name"></div><div class="form-group"><label for="exampleInputPassword1">Passwort</label><input type="password" class="form-control" id="inputPassword'
                     + modal_counter + '" placeholder="Passwort"></div>'
                     + '<div align="center"><div id="keyboard'
                     + modal_counter
                     + '"></div></div>'
                     + '<script type="text/javascript">'
                     + 'var kb = new keyboard("keyboard'
                     + modal_counter
                     + '", { width: 40, height: 40, keymap: ' + "'" + 'ge' + "'" + ' });'
                     + 'kb.add_keymap(' + "'" + 'ge' + "'" + ', maps[' + "'" + 'ge' + "'" + ']); '
                     //+ 'for(var i in maps)'
                     //+ '  { kb.add_keymap(i, maps[i]);}'
                     + 'kb.show();</script>'
                     + '</div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal" onclick="send_pw_request('
                     + "'" + var_function + "'" + ', '
                     + modal_counter + ')">Abschicken</button><button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button></div></div></div></div>';
    modal_counter++;
    $(modal_text).modal();
    modal_text = '';
}

function send_pw_request(var_function, var_modal_counter){
    data = getJsonData({'function': var_function, 'name': document.getElementById('inputName' + var_modal_counter).value, 'password': document.getElementById('inputPassword' + var_modal_counter).value}, true);
    if(! data.result == true){
        answer_fail();
    }
}

function answer_fail() {
    var text = '<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><h4 class="modal-title" id="myModalLabel">Fehler</h4></div><div class="modal-body">'
               + 'Authentifizierung fehlgeschlagen. Versuchen Sie es noch einmal.</div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button></div></div></div></div>';
    $(text).modal();
}

function answer_ok() {
    var text = '<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><h4 class="modal-title" id="myModalLabel">Alles ok</h4></div><div class="modal-body">' +
                                   data.message + '</div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button></div></div></div></div>';
                                   $(text).modal();

}