# coding=utf-8

import sys
import time
import platform
import os
from datetime import datetime, timedelta
import threading
import ctypes
import copy
import json
import codecs
import subprocess

import Logger
import MDConst
import DefaultSettings

__author__ = u'Peter Klassen'
__copyright__ = u'Copyright +++ replace year +++, Peter Klassen'
__license__ = u'Apache 2'
__date__ = u'+++ replace full date +++'


def about():
    return u'MediaDock Author: %s Copyright: %s License: %s Date: %s' % (__author__, __copyright__, __license__,
                                                                        __date__)

class Watchdog():
    def __init__(self, master):
        self.master = master
        self.__run = True
        self.thread = threading.Thread(target=self.update, name=u'WatchdogUpdater')
        self.thread.daemon = True

    def start(self):
        self.thread.start()

    def write_crash_dump(self):
        now = datetime.now()
        file_name = u'engine_crash_dump_%s.cd' % str(now)[:10]
        dump_file = open(MDConst.LOGPATH + '/' + file_name, 'a')
        e = self.master.e
        dump_file.write(unicode(now) + u' -- ' + repr(unicode(e.dh)) + u' -- ' + repr(unicode(e.jh)) + '\n')
        dump_file.close()

    def update(self):
        while self.__run:
            time.sleep(0.1)
            if not self.master.e.getThreadRunning():
                self.master.e.mh.addMessage(u'Watchdog: Restart Engine Thread', level=MDConst.MESSAGE_ERROR)
                try:
                    self.master.i.register_engine_restart()
                    self.write_crash_dump()
                    self.master.e.restart()
                except Exception as exc:
                    self.master.e.mh.addMessage(u'Watchdog: Restart Engine failed ' + unicode(exc),
                                                level=MDConst.MESSAGE_ERROR)

        pass # todo job überwachung -> zukünftiges release

class MDSystem:
    pass

    def __init__(self, master):
        self.master = master
        self.__info = {u'System Start': datetime.now(), u'Engine Restart': 0}
        # get system information
        # python version
        # os version (kernel version ...)
        # pid
        # abort if mediadock is already running

    def write_info(self, key, info):
        self.__info[key] = info

    def get_info(self, key):
        if self.__info.has_key(key):
            return copy.deepcopy(self.__info[key])
        return None

    def get_all_infos(self):
        temp = copy.deepcopy(self.__info)
        if temp.has_key(u'System Start'):
            temp[u'Uptime'] = unicode(datetime.now() - temp[u'System Start'])
        temp[u'System Start'] = unicode(temp[u'System Start'])
        '''
        if temp.has_key('NumberOfServerErrors') and temp.has_key('NumberOfServerRequests') \
                and temp['NumberOfServerRequests'] != 0:
            temp['ServerErrorRate'] = '%f%%' % (float(100* temp['NumberOfServerErrors']) /
                                                temp['NumberOfServerRequests'])
        '''
        if temp.has_key(u'NumberOfServerErrors'):
            temp[u'NumberOfServerErrors'] = unicode(temp[u'NumberOfServerErrors'])
        if temp.has_key(u'NumberOfServerRequests'):
            temp[u'NumberOfServerRequests'] = unicode(temp[u'NumberOfServerRequests'])
        return temp

    def write_to_file(self):
        file_name = u'quit-%s-' % unicode(datetime.now())[:10]
        i = 0
        while file_name + unicode(i) + u'.json' in os.listdir(MDConst.LOGPATH):
            i += 1
        file_name += unicode(i) + u'.json'
        try: # todo checken ob das immer geht (sonderzeichen)
            report_file = codecs.open(os.path.join(MDConst.LOGPATH, file_name), 'a', 'utf-8')
            json.dump(self.get_all_infos(), report_file, encoding='utf-8')
            report_file.close()
        except:
            pass

    def register_engine_restart(self):
        self.__info[u'Engine Restart'] += 1

    def startup(self):
        pass
        #
        # check all lib's
        # check firstrun

    def register_start_update(self, element):
        '''
        :param element: element witch starts update
        :return:
        '''
        pass

    def register_stop_update(self, element):
        pass # mintes

class MediaDock:
    def __init__(self):
        self.run = True
        self.shutdown = False
        self.th = None
        self.e = None
        self.s = None
        self.w = None

    def checkQt(self):
        try:
            #import PyQt4
            import PySide
            #import  PySide.QtCore as QtCore
            #import  PySide.QtGui as QtGui
            #import  PySide.QtWebKit as QtWebKit
        except:
            self.l.logMediaDock(u'Could not load library: PySide', logLevel=MDConst.LOG_ERROR)
            import tkMessageBox
            tkMessageBox.showerror(u'Schwerwiegender Feher', 'Die Bibliothek PySide konnte nicht geladen werden.')
            sys.exit(-1)

    def exec_(self):

        if MDConst.FROZEN:
            os.chdir(os.path.abspath(os.path.realpath(os.path.dirname(sys.executable))))

        run = True

        if '-autostart' in sys.argv:
            settings_file_path = os.path.join(MDConst.SETTINGSPATH, 'general.json')
            if os.path.isfile(settings_file_path):
                try:
                    settings_file = codecs.open(settings_file_path, 'r', encoding='utf-8')
                    settings = json.load(settings_file, encoding='utf-8')
                    settings_file.close()
                    if settings.has_key('autostart'):
                        if not settings['autostart']:
                            run = False
                    else:
                        run = False
                except:
                    run = False
            else:
                run = False

        if not run:
            return

        try:
            self.checkDirs()
        except:
            self.start_up_error(u'Fehler', u'Kein Zugriff auf benötigte Ordner.',
                                u'No access to some folders.')

        if MDConst.FROZEN:
            #sys.stdout = codecs.open(MDConst.LOGPATH + '/stdout-%s.log' % str(datetime.now())[:10], 'a', 'utf-8')
            sys.stderr = codecs.open(MDConst.LOGPATH + '/stderr-%s.log' % str(datetime.now())[:10], 'a', 'utf-8')

        self.l = Logger.Logger(self, MDConst.LOGPATH)
        self.mh = MessageHandler(self, self.l)

        self.l.logMediaDock(u'Start MediaDock Version: +++ replace release +++', logLevel=MDConst.LOG_INFO)

        # check PyQt
        self.checkQt()

        import Browser

        # start splash
        self.b = Browser.Browser(self)
        self.b.startSplashScreen()

        self.i = MDSystem(self)
        self.i.write_info(u'Release', u'+++ replace release +++')

        #time.sleep(2)

        # check single instance
        self.checkSingleInstance()

        # check environment
        self.checkEnvironment()

        self.sth = SettingsHandler(self)
        self.l.load_settings()
        self.l.cleanup()
        self.mh.load_settings()

        # check modules

        # check first run


        import Engine
        #import Server

        self.th = TimeHandler(self)
        self.e = Engine.Engine(self, self.l, self.mh)

        #server_port = self.sth.get_server_port()
        #self.s = Server.Server(self.l, self.e.si, server_port)

        self.w = Watchdog(self)

        self.th.start()
        self.e.start()
        #self.s.start()
        self.w.start()

        import GuiInterface
        self.gi = GuiInterface.GuiInterface(self)

        self.b.exec_()

        self.e.stat.flush()

        self.i.write_to_file()
        self.l.logMediaDock(u'Stop', logLevel=MDConst.LOG_INFO)

        if self.shutdown:
            self.l.logMediaDock(u'Shutdown', logLevel=MDConst.LOG_INFO)
            system = platform.system()
            if system == 'Windows':
                subprocess.call(['shutdown', '/s', '/t', '1'])
            elif system == 'Linux':
                pass # todo a bash file to shutdown linux

    def checkEnvironment(self):
        # check os
        #if not platform.system() == 'Windows':  # todo testen
        #    self.start_up_error(u'Fehler', u'Fasches Betriebssystem. Verwenden sie Windows 7 oder 8 (bzw. 10).',
        #                        u'Wrong OS %s' % platform.system())

        # check os (windows 7/8)
        if platform.system() == 'Windows' and not platform.release() in ['7', '8']:
            self.start_up_error(u'Fehler', u'Fasches Betriebssystem. Verwenden sie Windows 7, 8 oder 10.',
                                u'Wrong OS %s' % platform.version())

        # check interpreter
        if not sys.subversion[0] == 'CPython':
            self.start_up_error(u'Fehler', u'Falscher Interpreter. Er sollte CPython sein.',
                                u'Wrong interpreter %s' % sys.subversion[0])

        # check python interpreter 2.7.9
        if not sys.version.startswith('2.7.10 '): # getestet beim umstellen auf 2.7.10
            self.start_up_error(u'Fehler', u'Falsche Interpeterversion. Sie sollte 2.7.10 sein',
                                u'Wrong Python version: %s' % sys.version)

        # check 32 bit environment
        if not ctypes.sizeof(ctypes.c_voidp) == 4:
            self.start_up_error(u'Fehler', u'Falsche Interpreterversion. Sie sollte 32-Bit sein.',
                                u'Wrong Python version: Not 32-Bit.')

    def start_up_error(self, header, show_message, log_message):
        self.l.logMediaDock(log_message, logLevel=MDConst.LOG_ERROR)
        self.b.stopSplashScreen()
        self.b.messageBox(header, show_message)
        sys.exit(-1)

    def checkDirs(self):
        if not os.path.isdir(MDConst.SETTINGSPATH):
            os.makedirs(MDConst.SETTINGSPATH)
        if not os.path.isdir(MDConst.LOGPATH):
            os.makedirs(MDConst.LOGPATH)
        if not os.path.isdir(MDConst.STATPATH):
            os.makedirs(MDConst.STATPATH)
        #if not os.path.isdir(u'../Media'):
        #    os.makedirs(u'../Media')

    def checkSingleInstance(self):
        lock_file_name = MDConst.LOCALAPPDATAPATH + u'/.lock'
        try:
            if os.path.isfile(lock_file_name):
                os.remove(lock_file_name)
            self.lock_file = open(lock_file_name, 'w')
        except EnvironmentError as e:
            #if e.errno == 13:
            self.start_up_error(u'Fehler', u'Das Programm läuft bereits', u'Instance is already running')
            #else:
            #    print e

class TimeHandler():

    state_dict = {0: u'start', 1: u'run', 2: u'wait_for_engine_stop_timer', 3: u'wait_for_engine_stop_hand',
                  4: u'askshutdown_timer', 5: u'run_timer', 6: u'askshutdown_start', 7: u'shutdown',
                  8: u'wait_engine_stop_quit', 9: u'quit', 10: u'end'}

    def __init__(self, master):
        self.master = master
        self.sd_time = {u'weekly': {0: [u'23:59'], 1: [u'23:59'], 2: [u'23:59'], 3: [u'23:59'], 4: [u'23:59'],
                                    5: [u'23:59'], 6: [u'23:59']},
                        u'yearly': {}}
        self.lc_time = {u'weekly': {}, u'yearly': {}}
        self.ul_time = {u'weekly': {}, u'yearly': {}}
        self.nr_days = [0, 1, 3, 5]
        self.time_transition_4_7 = 5
        self.time_transition_5_2 = 10
        self.time_transition_6_7 = 5
        self.is_locked = False
        self.manual_lock = False
        self.init_scan_done = False
        self.run_day = True
        self.auto_shutdown = True
        self.master.i.write_info(u'auto_shutdown', self.auto_shutdown)
        self.thread = threading.Thread(target=self.update, name=u'TimeHandlerUpdate')
        self.thread.daemon = True
        #self.check_shutdown_thread = threading.Thread(target=self.check_shutdown, name='CheckShutdownThread')
        #self.check_shutdown_thread.daemon = True
        self.state = 0
        self.switch_timer = None
        self.load_timer_settings()

    def set_auto_shutdown(self):
        self.auto_shutdown = True
        self.master.i.write_info(u'auto_shutdown', self.auto_shutdown)

    def unset_auto_shutdown(self):
        self.auto_shutdown = False
        self.master.i.write_info(u'auto_shutdown', self.auto_shutdown)

    def start(self):
        self.load_timer_settings()
        self.thread.start()

    def init_scan(self):
        self.init_scan_done = True
        locked = False
        now = datetime.now()
        now_day = unicode(now.weekday())
        now_date = u'%02i.%02i' % (now.day, now.month)
        now_time = u'%02i:%02i' % (now.hour, now.minute)
        loop_break = False

        if self.lc_time[u'yearly'].has_key(now_date) or self.ul_time[u'yearly'].has_key(now_date):
            for i in range(24):
                for j in range(60):
                    test_time = u'%02i:%02i' % (i, j)
                    if test_time in self.lc_time[u'yearly'][now_date]:
                        locked = True
                    if test_time in self.ul_time[u'yearly'][now_date]:
                        locked = False
                    if test_time == now_time:
                        loop_break = True
                    if loop_break:
                        break
                if loop_break:
                    break

            return locked

        else:
            if not self.lc_time[u'weekly'].has_key(now_day):
                return False

            for i in range(24):
                for j in range(60):
                    test_time = u'%02i:%02i' % (i, j)
                    if test_time in self.lc_time[u'weekly'][now_day]:
                        locked = True
                    if self.ul_time[u'weekly'].has_key(now_day) and test_time in self.ul_time[u'weekly'][now_day]:
                            locked = False
                    if test_time == now_time:
                        loop_break = True
                    if loop_break:
                        break
                if loop_break:
                    break

        return locked

    def load_timer_settings(self):
        settings = self.master.sth.get_timer_settings()
        self.sd_time = settings[u'sd_time']
        self.lc_time = settings[u'lc_time']
        self.ul_time = settings[u'ul_time']
        self.nr_days = settings[u'nr_days']
        self.time_transition_4_7 = settings[u'time_transition_4_7']
        self.time_transition_5_2 = settings[u'time_transition_5_2']
        self.time_transition_6_7 = settings[u'time_transition_6_7']

        self.master.i.write_info(u'sd_time', str(self.sd_time))
        self.master.i.write_info(u'lc_time', str(self.lc_time))
        self.master.i.write_info(u'ul_time', str(self.ul_time))
        self.master.i.write_info(u'nr_days', str(self.nr_days))
        self.master.i.write_info(u'time_transition_4_7', self.time_transition_4_7)
        self.master.i.write_info(u'time_transition_5_2', self.time_transition_5_2)
        self.master.i.write_info(u'time_transition_6_7', self.time_transition_6_7)

    def reload_timer(self):
        self.load_timer_settings()
        if not self.manual_lock:
            self.is_locked = self.init_scan()

    def lock_manually(self):
        self.is_locked = True
        self.manual_lock = True

    def unlock(self):
        self.is_locked = False
        self.manual_lock = False

    def shutdown(self):
        self.master.l.logMediaDock(u'Shutdown by timer', logLevel=MDConst.LOG_INFO)
        self.master.start_shutdown_thread()

    def update_fsm(self):
        if self.state == 0:
            if datetime.now().weekday() in self.nr_days:
                self.state = 6
                self.switch_timer = datetime.now()
            else:
                self.state = 1
        elif self.state == 1:
            if self.check_timer_shutdown():
                self.state = 2
        elif self.state == 2:
            if self.master.e.is_stop_ready():
                self.state = 4
                self.switch_timer = datetime.now()
        elif self.state == 3:
            if not self.auto_shutdown:
                self.state = 5
                self.switch_timer = datetime.now()
            if self.master.e.is_stop_ready():
                self.state = 7
        elif self.state == 4:
            if not self.auto_shutdown:
                self.state = 5
                self.switch_timer = datetime.now()
            if (datetime.now() - self.switch_timer) > timedelta(minutes=self.time_transition_4_7):
                self.state = 7
        elif self.state == 5:
            if self.auto_shutdown and (datetime.now() - self.switch_timer) > timedelta(minutes=self.time_transition_5_2):
                self.state = 2
        elif self.state == 6:
            if (datetime.now() - self.switch_timer) > timedelta(minutes=self.time_transition_6_7):
                self.state = 7
        elif self.state == 7:
            self.master.l.logMediaDock(u'Shutdown by FSM Timer', logLevel=MDConst.LOG_INFO)
            self.master.shutdown = True
            self.master.b.stop()
            self.state = 10
        elif self.state == 8:
            if self.master.e.is_stop_ready():
                self.state = 9
        elif self.state == 9:
            self.master.l.logMediaDock(u'Quit by FSM Timer', logLevel=MDConst.LOG_INFO)
            self.master.b.stop()
            self.state = 10
        elif self.state == 10:
            try:
                self.master.b.stop()
            except:
                pass
        else:
            self.master.mh.addMessage(u'Fehler in FSM Timer: Unbekannter Zustand. Zustand auf run timer gesetzt',
                                      level=MDConst.MESSAGE_WARNING)
            self.master.l.logMediaDock(u'Error in FSM Timer: Unknown state. State set to run timer',
                                       logLevel=MDConst.MESSAGE_ERROR)
            self.state = 5
            self.switch_timer = datetime.now()
        self.master.i.write_info(u'timer_fsm_state', TimeHandler.state_dict[self.state])

    def get_left_time(self):
        if self.state in (6, 4):
            time = datetime.now() - self.switch_timer
            if self.state == 6:
                time = timedelta(minutes=self.time_transition_6_7) - time
            elif self.state == 4:
                time = timedelta(minutes=self.time_transition_4_7) - time
            time = time.total_seconds()
            time = divmod(time, 60)
            if time[0] == 0:
                if time[1] < 30:
                    return u'<30s'
                else:
                    return u'<1min'
            else:
                return u'%imin' % time[0]
        else:
            return u'---'

    def request_quit(self):
        if self.state in (1, 2, 3, 4, 5, 6):
            self.state = 8

    def request_shutdown(self):
        if self.state in (1, 2, 4, 5, 6, 8):
            self.state = 3

    def set_run_timer(self):
        if self.state in (3, 4, 8):
            self.state = 5
            self.switch_timer = datetime.now()

    def cancel_sd_start(self):
        if self.state == 6:
            self.state = 1

    def set_run(self):
        if self.state == 5:
            self.state = 1

    def check_job_ready(self):
        if self.state in (1, 5):
            return True, u'Auftrag kann gestartet werden.'
        if self.state == 0:
            return False, u'Das Programm wird gestartet.'
        if self.state == 2:
            return False, u'Warten Sie bis alle Aufträge beendet sind und brec' \
                          u'hen sie dann das Herunterfahren ab.'
        if self.state == 3:
            return False, u'Brechen Sie das Herunterfahren ab.'
        if self.state == 4:
            return False, u'Brechen Sie das Herunterfahren ab.'
        if self.state == 6:
            return False, u'Brechen Sie das Herunterfahren ab.'
        if self.state == 7:
            return False, u'Das Programm wird gleich beendet.'
        if self.state == 8:
            return False, u'Brechen Sie das Herunterfahren ab.'
        if self.state == 9:
            return False, u'Das Programm wird gleich beendet.'
        if self.state == 10:
            return False, u'Das Programm wird gleich beendet.'
        return False, u'Es kann kein Auftrag gestartet werden.'

    def update(self):
        self.lock = self.init_scan()

        while True:
            self.update_lock()

            old_state = self.state
            self.update_fsm()
            while old_state != self.state:
                old_state = self.state
                self.update_fsm()

            time.sleep(1)

    def check_timer_shutdown(self):
        now = datetime.now()
        now_date = u'%02i.%02i' % (now.day, now.month)
        now_time = u'%02i:%02i' % (now.hour, now.minute)
        now_day = unicode(now.weekday())
        if self.sd_time[u'yearly'].has_key(now_date):
            if now_time in self.sd_time[u'yearly'][now_date]:
                return True
        elif self.sd_time[u'weekly'].has_key(now_day):
            if now_time in self.sd_time[u'weekly'][now_day]:
                return True
        return False

    def update_lock(self):

        # check shutdown
        now = datetime.now()
        now_date = u'%02i.%02i' % (now.day, now.month)
        now_time = u'%02i:%02i' % (now.hour, now.minute)
        now_day = unicode(now.weekday())

        if now_time == u'00:00':
            self.is_locked = False

        # check lock
        lock = None
        if self.lc_time[u'yearly'].has_key(now_date) and now_time in self.lc_time[u'yearly'][now_date]:
            lock = True
        elif self.lc_time[u'weekly'].has_key(now_day) and now_time in self.lc_time[u'weekly'][now_day]:
            lock = True
        if self.ul_time[u'yearly'].has_key(now_date) and now_time in self.ul_time[u'yearly'][now_date]:
            lock = False
        elif self.ul_time[u'weekly'].has_key(now_day) and now_time in self.ul_time[u'weekly'][now_day]:
            lock = False

        if not lock is None:
            self.is_locked = lock

        #time.sleep(30)

    def set_run_day(self):
        self.run_day = True

class Message():
    def __init__(self, message, id, level, lifespan, section, requested=True, ip=None):
        self.message = message
        self.id = id
        self.level = level
        self.lifespan = lifespan
        self.section = section
        self.requested = requested
        if ip is None:
            self.ip = '127.0.0.1'
        else:
            self.ip = ip
        self.startTime = datetime.now()

    def __unicode__(self):
        return self.message

class MessageHandler():
    maxId = 0
    levelDict = {0: u'Debug', 1: u'Info', 2: u'Warnung', 3: u'Fehler'}

    def __init__(self, master, logger):
        self.master = master
        self.logger = logger
        self.messages = list()
        self.allMessages = list()
        self.default_live_span = 10000
        self.level = 0

    def load_settings(self):
        settings = self.master.sth.get_messages_settings()
        self.default_live_span = settings[u'default_live_span']
        self.level = settings[u'level']

    def remove(self, message_id):
        for i in self.messages:
            if i.id == message_id:
                self.messages.remove(i)
                break

    '''
    sections:
        0 all
        1 volumes
        2 new job
    '''
    def addMessage(self, message, level=0, lifespan=None, section=(0,), requested=True, ip=None):
        if lifespan is None:
            lifespan = self.default_live_span
        self.messages.append(Message(message, self.maxId, level, lifespan, section, requested, ip))
        self.allMessages.append({u'id': self.maxId, u'message': message, u'level': MessageHandler.levelDict[level],
                                 u'level_int': level, u'section': unicode(section).strip('[](),'),
                                 u'time': str(datetime.now()), u'requested': requested})
        requested_str = ''
        if not requested:
            requested_str = u'Modal'
        self.logger.logMessage(u'ID: %i / %sMessage: %s / Section: %s' % (self.maxId, requested_str, message,
                                                                          str(section).strip('[]')),
                               logLevel=level)
        MessageHandler.maxId += 1

    def getSortetMessages(self, section=0, level=0):
        if level >= self.level:
            sortet_messages = []
            for i in self.messages[::-1]:
                if section in i.section and i.level == level:
                    sortet_messages.append(i)
            return sortet_messages
        return []

    def getAllMessages(self):
        messages = list()
        for i in self.allMessages[::-1]:
            if i[u'level_int'] >= self.level:
                messages.append(i)
        return messages

    def update(self):
        self.master.i.write_info(u'Total Messages', str(len(self.allMessages)))
        self.master.i.write_info(u'Current Messages', str(len(self.messages)))
        for i in self.messages:
            d = datetime.now() - i.startTime
            if d.total_seconds() * 1000 > i.lifespan and i.requested:
                self.messages.remove(i)

    def getModalMessages(self, ip):
        data = []
        for i in self.messages:
            if i.requested == False and i.ip == ip:
                data.append({u'message': i.message, u'levelName': MessageHandler.levelDict[i.level], u'level': i.level})
                i.requested = True
        return data

class SettingsHandler():
    def __init__(self, master):
        self.master = master
        self.settings_path_available = False
        self.general_json_path_available = False
        self.timer_json_path_available = False
        self.files_json_path_available = False
        self.general_json_path = os.path.join(MDConst.SETTINGSPATH, MDConst.GENERAL_JSON)
        self.timer_json_path = os.path.join(MDConst.SETTINGSPATH, MDConst.TIMER_JSON)
        self.files_json_path = os.path.join(MDConst.SETTINGSPATH, MDConst.FILES_JSON)
        self.general_settings = None
        self.timer_settings = None
        self.files_settings = None
        self.checkSettingsPath()
        self.load_all_settings()

    def checkSettingsPath(self):
        self.settings_path_available = False
        if not os.path.isdir(MDConst.SETTINGSPATH):
            try:
                os.makedirs(MDConst.SETTINGSPATH)
            except:
                self.master.mh.addMessage(u'Pfad für Einstellungen konnte nicht erstellt werden: %s' %
                                          MDConst.SETTINGSPATH, level=MDConst.MESSAGE_WARNING)
            else:
                self.settings_path_available = True
        else:
            self.settings_path_available = True

        if self.settings_path_available:
            if not os.path.isfile(self.general_json_path):
                try:
                    settings_file = codecs.open(self.general_json_path, 'w', encoding='utf-8')
                    json.dump(DefaultSettings.general, settings_file)
                    settings_file.close()
                except:
                    self.master.mh.addMessage(u'Allgemeine Einstellungen konnten nicht erstellt werden: %s' %
                                              self.general_json_path, level=MDConst.MESSAGE_WARNING)
                    self.general_json_path_available = False
                else:
                    self.general_json_path_available = True
            else:
                self.general_json_path_available = True
            if not os.path.isfile(self.timer_json_path):
                try:
                    settings_file = codecs.open(self.timer_json_path, 'w', encoding='utf-8')
                    json.dump(DefaultSettings.timer, settings_file)
                    settings_file.close()
                except:
                    self.master.mh.addMessage(u'Einstellungen des Timers konnten nicht erstellt werden: %s' %
                                              self.timer_json_path, level=MDConst.MESSAGE_WARNING)
                    self.timer_json_path_available = False
                else:
                    self.timer_json_path_available = True
            else:
                self.timer_json_path_available = True
            if not os.path.isfile(self.files_json_path):
                try:
                    settings_file = codecs.open(self.files_json_path, 'w', encoding='utf-8')
                    json.dump(DefaultSettings.files, settings_file)
                    settings_file.close()
                except:
                    self.master.mh.addMessage(u'Dateieinstellungen konnten nicht erstellt werden: %s' %
                                              self.files_json_path, level=MDConst.MESSAGE_WARNING)
                    self.files_json_path_available = False
                else:
                    self.files_json_path_available = True
            else:
                self.files_json_path_available = True
        else:
            self.general_json_path_available = False
            self.timer_json_path_available = False
            self.files_json_path_available = False

    def load_all_settings(self):
        self.general_settings, general_errors = self.check_general_settings(self.load_general_settings()) # todo error handling
        self.timer_settings, timer_errors = self.check_timer_settings(self.load_timer_settings())
        self.files_settings, files_errors = self.check_files_settings(self.load_files_settings())
        if self.files_settings['rootdir'] is None:
            self.master.mh.addMessage('Aufnahmenverzeichnis wurde nicht gefunden.', level=MDConst.LOG_ERROR,
                                      requested=False)

    def load_general_settings(self):
        try:
            settings_file = codecs.open(self.general_json_path, 'r', encoding='utf-8')
            settings = json.load(settings_file, encoding='utf-8')
            settings_file.close()
        except:
            settings = DefaultSettings.general
            self.master.mh.addMessage(u'Allgemeine Einstellungen konnten nicht aus Datei geladen werden.',
                                      level=MDConst.LOG_ERROR)
        return settings

    def load_timer_settings(self):
        try:
            settings_file = codecs.open(self.timer_json_path, 'r', encoding='utf-8')
            settings = json.load(settings_file, encoding='utf-8')
            settings_file.close()
        except:
            settings = DefaultSettings.timer
            self.master.mh.addMessage(u'Einstellungen des Timers konnten nicht aus Datei geladen werden.',
                                      level=MDConst.LOG_ERROR)
        return settings

    def load_files_settings(self):
        try:
            settings_file = codecs.open(self.files_json_path, 'r', encoding='utf-8')
            settings = json.load(settings_file, encoding='utf-8')
            settings_file.close()
        except:
            settings = DefaultSettings.files
            self.master.mh.addMessage(u'Einstellungen zu Dateien konnten nicht aus Datei geladen werden.',
                                      level=MDConst.LOG_ERROR)
        return settings

    def check_all_settings(self, settings):
        general = self.check_general_settings(settings[u'timer'])
        timer = self.check_timer_settings(settings[u'timer'])
        files = self.check_files_settings(settings[u'files'])

        return {u'general': general, u'timer': timer, u'files': files}

    def check_general_settings(self, settings):
        #temp = []
        #for i in settings[u'nr_days']:
        #    if i in (0, 1, 2, 3, 4, 5, 6) and not i in temp:
        #        temp.append(i)
        #settings[u'nr_days'] = temp
        return settings, [] # todo

    def check_timer_settings(self, settings):
        #temp = []
        #for i in settings[u'nr_days']:
        #    if i in (0, 1, 2, 3, 4, 5, 6) and not i in temp:
        #        temp.append(i)
        #settings[u'nr_days'] = temp
        return settings, [] # todo

    def check_files_settings(self, settings):
        if not isinstance(settings[u'rootdir'], (unicode)):
            try:
                settings[u'rootdir'] = unicode(settings[u'rootdir'])
            except:
                settings[u'rootdir'] = None
        settings[u'rootdir'] = os.path.realpath(settings[u'rootdir'])
        if not os.path.isdir(settings[u'rootdir']):
            settings[u'rootdir'] = None
        if not settings[u'rootdir'] is None:
            settings[u'rootdir'] = settings[u'rootdir'].replace('\\', '/')
        #temp = []
        #for i in settings[u'explorer_file_extensions']:
        #    if isinstance(i, unicode):
        #        if not '.' + i in temp:
        #            temp.append('.' + i)
        #settings[u'explorer_file_extensions'] = temp
        #temp = []
        #for i in settings[u'copy_file_extensions']:
        #    if isinstance(i, unicode):
        #        if not '.' + i in temp:
        #            temp.append('.' + i)
        #settings[u'copy_file_extensions'] = temp
        return settings, [] # todo

    def get_general_settings(self):
        return self.general_settings

    def get_timer_settings(self):
        return self.timer_settings

    def get_files_settings(self):
        return self.files_settings

    def get_logger_settings(self):
        return self.general_settings[u'logger']

    def get_messages_settings(self):
        return self.general_settings[u'messages']

    def get_drives_settings(self):
        return self.general_settings[u'drives']

    #def get_server_port(self):
    #    return self.general_settings[u'server_port']

if __name__ == '__main__':
    md = MediaDock()
    md.exec_()
    sys.exit()