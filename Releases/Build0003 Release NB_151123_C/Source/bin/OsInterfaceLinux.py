# -*- coding: utf-8 -*-

import os
import MDConst

def getVolumeFreeSpace(device):
    temp = os.popen('df /dev/%s' % device).read().split('\n')[1].split(' ')
    while '' in temp:
        temp.remove('')
    return int(temp[3]) * 1024

def getVolumeTotalSpace(letter):
    return 1 # todo for linux
        # lsblk -b

def getVolumeName(device):
    temp = os.popen('df /dev/%s' % device).read().split('\n')[1]
    name = temp.split('/')[5]
    return name

def getVolumeInfo(device):
    temp = os.popen('df /dev/%s' % device).read().split('\n')[1].split(' ')
    while '' in temp:
        temp.remove('')
    name = ' '.join(temp).split('/')[5]
    mountpoint = ' '.join(temp[5:]).strip(' ')
    return [mountpoint, name, int(temp[1]) * 1024, int(temp[3]) * 1024]

def getVolumesList(self):
    volume_list_str = os.popen('lsblk -o NAME,TYPE').read().decode('utf-8').split('\n')
    idx_type = volume_list_str[0].index('TYPE')
    volume_list = []
    for i in volume_list_str[2:]:
        if 'part' in i and i[idx_type:idx_type+4] == 'part':  # todo linux removable drives only / use exclusion list
            j = i.split(' ')[0].strip(u'├└─')
            if not j.startswith('sda'):
                volume_list.append(j)
    return volume_list

def isHiddenPath(self, dir_name):
    if not os.path.isdir(dir_name):
        return False
    return dir_name.startswith('.')

def ejectDrive(device, message_handler):
    temp = os.popen('udisks --unmount /dev/%s' % device).read()
    if temp == '':
        temp2 = os.popen('udisks --detach /dev/%s' % device.strip('0123456789')).read()
        if temp2 == '':
            return True
        else:
            message_handler.addMessage(u'Laufwerk wurde entfehrnt, aber es sind noch andere Laufwerke dieses '
                                      u'Datenträgers angeschlosssen', section=[0, 1],
                                      level=MDConst.MESSAGE_INFO) # todo Laufwerke ersetzen linux
            pass # todo message stick nicht entfehrnen  rückgabewert??  linux
            return True
    else:
        return False