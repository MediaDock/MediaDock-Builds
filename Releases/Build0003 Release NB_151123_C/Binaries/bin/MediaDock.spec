# -*- mode: python -*-
a = Analysis(['MediaDock.py'],
             pathex=['E:\\Softwareentwicklung\\PyCharmProjects\\MediaDock\\releases\\Build0003 Release NB_151123_C\\Binaries\\bin'],
             hiddenimports=['_scrypt'],
             hookspath=None,
             runtime_hooks=None)
pyz = PYZ(a.pure)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='MediaDock.exe',
          debug=False,
          strip=None,
          upx=True,
          console=False , icon='E:\\Softwareentwicklung\\PyCharmProjects\\MediaDock\\files\\music-folder.ico')
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=None,
               upx=True,
               name='MediaDock')
