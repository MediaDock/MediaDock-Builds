function getAllMessages(){
    data = getJsonData({'function': 'getAllMessages'}, true);

    var elem = document.getElementById("messages-list");
    var html = '';
    for (var i in data.info){
        html += '<tr><td>' +
         data.info[i].id + '</td><td>' +
         data.info[i].message + '</td><td>' +
         data.info[i].time + '</td><td>' +
         data.info[i].level + '</td><td>' +
         data.info[i].section + '</td></tr>';
    }
    elem.innerHTML = html;
}

getAllMessages();
setInterval(getAllMessages, 3000);