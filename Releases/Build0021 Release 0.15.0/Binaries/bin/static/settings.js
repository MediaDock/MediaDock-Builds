
function load_all_settings(){
    load_general_settings();
    load_files_settings();
    load_db_settings();
}

function load_general_settings(){
    data = getJsonData({'function': 'getGeneralSettings'}, true).data;
    show_general_settings(data);
}

function show_general_settings(data){
    $('#autostart').prop('checked', data.autostart);
    $('#statistics').prop('checked', data.statistics);
    $('#drives_use_exclusion_list').prop('checked', data.drives_use_exclusion_list);
    $('#drives_removable_drives_only').prop('checked', data.drives_removable_drives_only);
    $('#log_everything').val(data.log_everything.toString());
    $('#order_cd_activated').prop('checked', data.order_cd_activated);
    $('#default_church').val(data.default_church);
    ed.extensions = data.drives_excluded_drives;
    ed.show();
}

function get_general_settings_from_gui(){
    settings = {};
    ed.show();
    settings['zoom'] = 1.0;
    settings['autostart'] = $('#autostart').prop('checked');
    settings['statistics'] = $('#statistics').prop('checked');
    settings['drives_use_exclusion_list'] = $('#drives_use_exclusion_list').prop('checked');
    settings['drives_removable_drives_only'] = $('#drives_removable_drives_only').prop('checked');
    settings['log_everything'] = $('#log_everything').val() == 'true';
    settings['order_cd_activated'] = $('#order_cd_activated').prop('checked');
    settings['default_church'] = $('#default_church').val();
    settings['drives_excluded_drives'] = ed.extensions;
    return settings;
}

function check_general_settings(){
    gui_settings = get_general_settings_from_gui();
    data = getJsonData({'function': 'checkGeneralSettings', 'data': gui_settings}, true);
    show_general_settings(data.settings);
}

function revert_general_settings(){
    data = getJsonData({'function': 'getGeneralDefaultSettings'}, true).data;
    show_general_settings(data);
}

function save_general_settings(){
    settings = get_general_settings_from_gui();
    data = getJsonData({'function': 'saveGeneralSettings', 'data': settings}, true);
    show_general_settings(data.settings);
}

function ask_delete_stats(){
    modal_text = '<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" ><div class="modal-dialog modal-lg"><div class="modal-content" style="background-color:'
                 + 'white'
                 + '"><div class="modal-header"><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><h4 class="modal-title" id="myModalLabel">'
                 + 'Statistik löschen' + '</h4></div><div class="modal-body">'
                 + '<div align="center">'
                 + 'Soll die gesamte Statistik unwiederbringlich gelöscht werden?'
                 + '</div>'
                 + '<div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal" onclick="delete_stats()">Ja, wirklich löschen</button><button type="button" class="btn btn-default" data-dismiss="modal">Nein</button></div></div></div></div>';
    $(modal_text).modal();
}

function delete_stats(){
    getJsonData({'function': 'deleteStatistics'})
}

function load_files_settings(){
    data = getJsonData({'function': 'getFileSettings'}, true).data;
    $('#root_dir').val(data.rootdir);
    cfe.extensions = data.copy_file_extensions;
    cfe.show();
}

function CopyFileExtensions (var_name, html_id, prompt_message){
    this.var_name = var_name;
    this.html_id = html_id;
    this.prompt_message = prompt_message;
    this.extensions = [];
}

CopyFileExtensions.prototype.add_extension = function(ext){
    if ($.inArray(ext, this.extensions) == -1 && ext != null && ext != ''){
        this.extensions.push(ext);
    }
}

CopyFileExtensions.prototype.add_show_extension = function(){
    ext = prompt(this.prompt_message, '')
    this.add_extension(ext);
    this.show();
}

CopyFileExtensions.prototype.remove_extension = function(ext){
    var index = this.extensions.indexOf(ext);
    this.extensions.splice(index, 1);
}

CopyFileExtensions.prototype.remove_show_extension = function(ext){
    this.remove_extension(ext);
    this.show();
}

CopyFileExtensions.prototype.show = function(){
    html = '';
    for (var i in this.extensions){
        html += '<tr>';
        html += '   <td>' + this.extensions[i] + '</td>';
        html += '   <td><button onclick="' + this.var_name + '.remove_show_extension(\'' + this.extensions[i].replace(/\\/g, '\\\\') + '\')"><span class="glyphicon glyphicon-remove-circle"></span></button></td>';
        html += '</tr>';
    }
    $('#' + this.html_id).html(html);
}

function get_new_rootdir(){
    data = getJsonData({'function': 'getExistingDir'}, true);
    if (data.dir != ''){
        $('#root_dir').val(data.dir);
    }
}

function check_files_settings(){
    settings = {};
    settings['rootdir'] = $('#root_dir').val();
    settings['copy_file_extensions'] = cfe.extensions;
    data = getJsonData({'function': 'checkFileSettings', 'data': settings}, true);
    $('#root_dir').val(data.settings.rootdir);
    cfe.extensions = data.settings.copy_file_extensions;
    cfe.show();
}

function revert_files_settings(){
    data = getJsonData({'function': 'getFileDefaultSettings'}, true).data;
    $('#root_dir').val(data.rootdir);
    cfe.extensions = data.copy_file_extensions;
    cfe.show();
}

function save_files_settings(){
    settings = {};
    settings['rootdir'] = $('#root_dir').val();
    settings['copy_file_extensions'] = cfe.extensions;
    data = getJsonData({'function': 'saveFileSettings', 'data': settings}, true);
    $('#root_dir').val(data.settings.rootdir);
    cfe.extensions = data.settings.copy_file_extensions;
    cfe.show();
}

function show_db_settings(data){
    $('#db_host').val(data.host);
    $('#db_port').val(data.port);
    $('#db_user').val(data.user);
    $('#db_password').val(data.passwd);
    $('#db_charset').val(data.charset);
}

function get_db_settings_from_gui(){
    data = {};
    data.host = $('#db_host').val();
    data.port = parseInt($('#db_port').val());
    data.user = $('#db_user').val();
    data.passwd = $('#db_password').val();
    data.charset = $('#db_charset').val();
    return data;
}

function load_db_settings(){
    data = getJsonData({'function': 'getDbSettings'}, true).data;
    show_db_settings(data);
}


function check_db_settings(){
    gui_settings = get_db_settings_from_gui();
    data = getJsonData({'function': 'checkDbSettings', 'data': gui_settings}, true);
    show_db_settings(data.settings);
}

function revert_db_settings(){
    data = getJsonData({'function': 'getDbDefaultSettings'}, true).data;
    show_db_settings(data);
}

function save_db_settings(){
    settings = get_db_settings_from_gui();
    data = getJsonData({'function': 'saveDbSettings', 'data': settings}, true);
    show_db_settings(data.settings);
}

function check_db_connection(){
    gui_settings = get_db_settings_from_gui();
    data = getJsonData({'function': 'checkDbConnection', 'data': gui_settings}, true);
    if (data.result){
        $("#button_check_db_connection").css("background-color","green");
        $("#button_check_db_connection").css("color","white");
    }
    else{
        $("#button_check_db_connection").css("background-color","red");
        $("#button_check_db_connection").css("color","white");
    }
}

var cfe = new CopyFileExtensions('cfe', 'tbody_copy_file_extensions', 'Bitte geben sie einen Dateityp ein:');
var ed = new CopyFileExtensions('ed', 'tbody_excluded_drives', 'Bitte geben sie einen Laufwerksbezeichner ein:');
load_all_settings();
