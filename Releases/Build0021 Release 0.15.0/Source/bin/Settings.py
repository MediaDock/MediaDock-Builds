# coding=utf-8

import os
import json
import codecs
import socket
import re

import MDConst
import DefaultSettings
import OsInterface as Osi


class SettingsHandler(object):
    def __init__(self, master):
        self.master = master
        self.settings_path_available = False
        self.general_json_path_available = False
        self.timer_json_path_available = False
        self.files_json_path_available = False
        self.db_json_path_available = False
        self.general_json_path = os.path.join(MDConst.SETTINGSPATH, MDConst.GENERAL_JSON)
        self.timer_json_path = os.path.join(MDConst.SETTINGSPATH, MDConst.TIMER_JSON)
        self.files_json_path = os.path.join(MDConst.SETTINGSPATH, MDConst.FILES_JSON)
        self.db_json_path = os.path.join(MDConst.SETTINGSPATH, MDConst.DB_JSON)
        self.general_settings = DefaultSettings.general
        self.timer_settings = DefaultSettings.timer
        self.files_settings = DefaultSettings.files
        self.db_settings = DefaultSettings.db
        self.checkSettingsPath()
        self.load_all_settings()
        # self.write_all_settings()

    def checkSettingsPath(self):
        self.settings_path_available = False
        if not os.path.isdir(MDConst.SETTINGSPATH):
            try:
                os.makedirs(MDConst.SETTINGSPATH)
            except:
                self.master.mh.addMessage(u'Pfad für Einstellungen konnte nicht erstellt werden: %s' %
                                          MDConst.SETTINGSPATH, level=MDConst.MESSAGE_WARNING)
            else:
                self.settings_path_available = True
        else:
            self.settings_path_available = True

        if self.settings_path_available:
            if not os.path.isfile(self.general_json_path):
                try:
                    settings_file = codecs.open(self.general_json_path, 'w', encoding='utf-8')
                    json.dump(DefaultSettings.general, settings_file)
                    settings_file.close()
                except:
                    self.master.mh.addMessage(u'Allgemeine Einstellungen konnten nicht erstellt werden: %s' %
                                              self.general_json_path, level=MDConst.MESSAGE_WARNING)
                    self.general_json_path_available = False
                else:
                    self.general_json_path_available = True
            else:
                self.general_json_path_available = True
            if not os.path.isfile(self.timer_json_path):
                try:
                    settings_file = codecs.open(self.timer_json_path, 'w', encoding='utf-8')
                    json.dump(DefaultSettings.timer, settings_file)
                    settings_file.close()
                except:
                    self.master.mh.addMessage(u'Einstellungen des Timers konnten nicht erstellt werden: %s' %
                                              self.timer_json_path, level=MDConst.MESSAGE_WARNING)
                    self.timer_json_path_available = False
                else:
                    self.timer_json_path_available = True
            else:
                self.timer_json_path_available = True
            if not os.path.isfile(self.files_json_path):
                try:
                    settings_file = codecs.open(self.files_json_path, 'w', encoding='utf-8')
                    json.dump(DefaultSettings.files, settings_file)
                    settings_file.close()
                except:
                    self.master.mh.addMessage(u'Dateieinstellungen konnten nicht erstellt werden: %s' %
                                              self.files_json_path, level=MDConst.MESSAGE_WARNING)
                    self.files_json_path_available = False
                else:
                    self.files_json_path_available = True
            else:
                self.files_json_path_available = True
            if not os.path.isfile(self.db_json_path):
                try:
                    settings_file = codecs.open(self.db_json_path, 'w', encoding='utf-8')
                    json.dump(DefaultSettings.db, settings_file)
                    settings_file.close()
                except:
                    self.master.mh.addMessage(u'Datenbankeinstellungen konnten nicht erstellt werden: %s' %
                                              self.db_json_path, level=MDConst.MESSAGE_WARNING)
                    self.db_json_path_available = False
                else:
                    self.db_json_path_available = True
            else:
                self.db_json_path_available = True
        else:
            self.general_json_path_available = False
            self.timer_json_path_available = False
            self.files_json_path_available = False
            self.db_json_path_available = False

    def load_all_settings(self):
        self.general_settings, general_errors = self.check_general_settings(self.read_general_settings())
        self.timer_settings, timer_errors = self.check_timer_settings(self.read_timer_settings())
        self.files_settings, files_errors = self.check_files_settings(self.read_files_settings())
        self.db_settings, db_errors = self.check_db_settings(self.read_db_settings())
        errors = general_errors + timer_errors + files_errors + db_errors
        if errors:
            self.master.mh.addMessage(u'Einige Einstellungen sind fehlerhaft. Bitte informieren sie den Administrator.',
                                      level=MDConst.MESSAGE_WARNING,
                                      requested=False)
        for error in errors:
            self.master.mh.addMessage(error, level=MDConst.MESSAGE_WARNING)

    def read_general_settings(self):
        try:
            settings_file = codecs.open(self.general_json_path, 'r', encoding='utf-8')
            settings = json.load(settings_file, encoding='utf-8')
            settings_file.close()
        except:
            settings = DefaultSettings.general
            self.master.mh.addMessage(u'Allgemeine Einstellungen konnten nicht aus Datei gelesen werden.',
                                      level=MDConst.MESSAGE_ERROR, requested=False)
        return settings

    def read_timer_settings(self):
        try:
            settings_file = codecs.open(self.timer_json_path, 'r', encoding='utf-8')
            settings = json.load(settings_file, encoding='utf-8')
            settings_file.close()
        except:
            settings = DefaultSettings.timer
            self.master.mh.addMessage(u'Einstellungen des Timers konnten nicht aus Datei gelesen werden.',
                                      level=MDConst.MESSAGE_ERROR, requested=False)
        return settings

    def read_files_settings(self):
        try:
            settings_file = codecs.open(self.files_json_path, 'r', encoding='utf-8')
            settings = json.load(settings_file, encoding='utf-8')
            settings_file.close()
        except:
            settings = DefaultSettings.files
            self.master.mh.addMessage(u'Einstellungen zu Dateien konnten nicht aus Datei gelesen werden.',
                                      level=MDConst.MESSAGE_ERROR, requested=False)
        return settings

    def read_db_settings(self):
        try:
            settings_file = codecs.open(self.db_json_path, 'r', encoding='utf-8')
            settings = json.load(settings_file, encoding='utf-8')
            settings_file.close()
        except:
            settings = DefaultSettings.db
            self.master.mh.addMessage(u'Einstellungen zur Datenbank konnten nicht aus Datei gelesen werden.',
                                      level=MDConst.MESSAGE_ERROR, requested=False)
        return settings

    def check_general_settings(self, settings):
        messages = []
        new_settings = {}

        # zoom
        if u'zoom' in settings:
            if isinstance(settings[u'zoom'], (float, int)):
                zoom = float(settings[u'zoom'])
                if 0.1 <= zoom <= 10.0 :
                    new_settings[u'zoom'] = zoom
                else:
                    new_settings[u'zoom'] = DefaultSettings.general[u'zoom']
                    messages.append(u'Allgemein-Parameter "zoom" muss vom zwischen 0.1 und 10 liegen.')
            else:
                new_settings[u'zoom'] = DefaultSettings.general[u'zoom']
                messages.append(u'Allgemein-Parameter "zoom" muss vom Typ float sein.')
        else:
            new_settings[u'zoom'] = DefaultSettings.general[u'zoom']
            messages.append(u'Allgemeine Einstellungen brauchen einen Parameter "zoom".')

        # autostart
        if u'autostart' in settings:
            if isinstance(settings[u'autostart'], bool):
                new_settings[u'autostart'] = settings[u'autostart']
            else:
                new_settings[u'autostart'] = DefaultSettings.general[u'autostart']
                messages.append(u'Allgemein-Parameter "autostart" muss vom Typ bool sein.')
        else:
            new_settings[u'autostart'] = DefaultSettings.general[u'autostart']
            messages.append(u'Allgemeine Einstellungen brauchen einen Parameter "autostart".')

        # statistics
        if u'statistics' in settings:
            if isinstance(settings[u'statistics'], bool):
                new_settings[u'statistics'] = settings[u'statistics']
            else:
                new_settings[u'statistics'] = DefaultSettings.general[u'statistics']
                messages.append(u'Allgemein-Parameter "statistics" muss vom Typ bool sein.')
        else:
            new_settings[u'statistics'] = DefaultSettings.general[u'statistics']
            messages.append(u'Allgemeine Einstellungen brauchen einen Parameter "statistics".')

        # default_church
        if u'default_church' in settings:
            if isinstance(settings[u'default_church'], unicode):
                new_settings[u'default_church'] = settings[u'default_church']
            else:
                new_settings[u'default_church'] = DefaultSettings.general[u'default_church']
                messages.append(u'Allgemein-Parameter "default_church" muss vom Typ unicode sein.')
        else:
            new_settings[u'default_church'] = DefaultSettings.general[u'default_church']
            messages.append(u'Allgemeine Einstellungen brauchen einen Parameter "default_church".')

        # drives_removable_drives_only
        if u'drives_removable_drives_only' in settings:
            if isinstance(settings[u'drives_removable_drives_only'], bool):
                new_settings[u'drives_removable_drives_only'] = settings[u'drives_removable_drives_only']
            else:
                new_settings[u'drives_removable_drives_only'] = DefaultSettings.general[u'drives_removable_drives_only']
                messages.append(u'Allgemein-Parameter "drives_removable_drives_only" muss vom Typ bool sein.')
        else:
            new_settings[u'drives_removable_drives_only'] = DefaultSettings.general[u'drives_removable_drives_only']
            messages.append(u'Allgemeine Einstellungen brauchen einen Parameter "drives_removable_drives_only".')

        # drives_use_exclusion_list
        if u'drives_use_exclusion_list' in settings:
            if isinstance(settings[u'drives_use_exclusion_list'], bool):
                new_settings[u'drives_use_exclusion_list'] = settings[u'drives_use_exclusion_list']
            else:
                new_settings[u'drives_use_exclusion_list'] = DefaultSettings.general[u'drives_use_exclusion_list']
                messages.append(u'Allgemein-Parameter "drives_use_exclusion_list" muss vom Typ bool sein.')
        else:
            new_settings[u'drives_use_exclusion_list'] = DefaultSettings.general[u'drives_use_exclusion_list']
            messages.append(u'Allgemeine Einstellungen brauchen einen Parameter "drives_use_exclusion_list".')

        # drives_excluded_drives
        if u'drives_excluded_drives' in settings:
            if isinstance(settings[u'drives_excluded_drives'], list):
                drives_excluded_drives = list()
                count_false = 0
                for identifier in settings[u'drives_excluded_drives']:
                    if isinstance(identifier, unicode) and Osi.isValidDriveIdentifier(identifier):
                        drives_excluded_drives.append(identifier)
                    else:
                        count_false += 1
                if count_false:
                    messages.append(u'Eine oder mehrere Laufwerkskennzeichner in Allgemeine Einstellugen '
                                    u'"drives_excluded_drives" sind fehlerhaft.')
                new_settings[u'drives_excluded_drives'] = drives_excluded_drives
            else:
                new_settings[u'drives_excluded_drives'] = DefaultSettings.general[u'drives_excluded_drives']
                messages.append(u'Allgemein-Parameter "drives_excluded_drives" muss vom Typ list sein.')
        else:
            new_settings[u'drives_excluded_drives'] = DefaultSettings.general[u'drives_excluded_drives']
            messages.append(u'Allgemeine Einstellungen brauchen einen Parameter "drives_excluded_drives".')

        # logger
        if u'log_everything' in settings:
            if isinstance(settings[u'log_everything'], bool):
                new_settings[u'log_everything'] = settings[u'log_everything']
            else:
                new_settings[u'log_everything'] = DefaultSettings.general[u'log_everything']
                messages.append(u'Allgemein-Parameter "log_everything" muss vom Typ bool sein.')
        else:
            new_settings[u'log_everything'] = DefaultSettings.general[u'log_everything']
            messages.append(u'Allgemeine Einstellungen brauchen einen Parameter "logger.log_everything".')

        # order_cd_activated
        if u'order_cd_activated' in settings:
            if isinstance(settings[u'order_cd_activated'], bool):
                new_settings[u'order_cd_activated'] = settings[u'order_cd_activated']
            else:
                new_settings[u'order_cd_activated'] = DefaultSettings.general[u'order_cd_activated']
                messages.append(u'Allgemein-Parameter "order_cd_activated" muss vom Typ bool sein.')
        else:
            new_settings[u'order_cd_activated'] = DefaultSettings.general[u'order_cd_activated']
            messages.append(u'Allgemeine Einstellungen brauchen einen Parameter "order_cd_activated".')

        return new_settings, messages

    def check_timer_settings(self, settings):
        messages = []
        new_settings = {}
        #for i in settings[u'nr_days']:
        #    if i in (0, 1, 2, 3, 4, 5, 6) and not i in temp:
        #        temp.append(i)
        #settings[u'nr_days'] = temp
        return settings, messages # todo

    def check_files_settings(self, settings):
        messages = []
        new_settings = {}
        if u'rootdir' in settings:
            if isinstance(settings[u'rootdir'], unicode):
                rootdir = os.path.realpath(settings[u'rootdir'])
                if os.path.isdir(rootdir):
                    new_settings[u'rootdir'] = rootdir.replace('\\', '/')
                else:
                    new_settings[u'rootdir'] = DefaultSettings.files[u'rootdir']
                    messages.append(u'Der Datei-Parameter "rootdir" muss ein existierendes Verzeichnis sein.')
            else:
                new_settings[u'rootdir'] = DefaultSettings.files[u'rootdir']
                messages.append(u'Datei-Parameter "rootdir" muss vom Typ unicode sein.')
        else:
            new_settings[u'rootdir'] = DefaultSettings.files[u'rootdir']
            messages.append(u'Einstellungen zu Dateien brauchen einen Parameter "rootdir".')


        # copy_file_extensions
        if u'copy_file_extensions' in settings:
            if isinstance(settings[u'copy_file_extensions'], list):
                copy_file_extensions = list()
                count_false = 0
                for extension in settings[u'copy_file_extensions']:
                    if isinstance(extension, unicode) and re.match(r'[a-zA-Z0-9]+\Z', extension):
                        copy_file_extensions.append(extension)
                    else:
                        count_false += 1
                if count_false:
                    messages.append(u'Eine oder mehrere Dateiendung in Dateieinstellungen "copy_file_extensions" '
                                    u'fehlerhaft.')
                if copy_file_extensions:
                    new_settings[u'copy_file_extensions'] = copy_file_extensions
                else:
                    new_settings[u'copy_file_extensions'] = DefaultSettings.files[u'copy_file_extensions']
                    messages.append(u'Es wurden keine gültigen Dateiendungen in Dateieinstellungen '
                                    u'"copy_file_extensions" gefunden')
            else:
                new_settings[u'copy_file_extensions'] = DefaultSettings.files[u'copy_file_extensions']
                messages.append(u'Datei-Parameter "copy_file_extensions" muss vom Typ list sein.')
        else:
            new_settings[u'copy_file_extensions'] = DefaultSettings.files[u'copy_file_extensions']
            messages.append(u'Einstellungen zu Dateien brauchen einen Parameter "copy_file_extensions".')

        return new_settings, messages

    def is_valid_ipv4(self, ip):
        try:
            socket.inet_aton(ip)
            return True
        except socket.error:
            return False

    def check_db_settings(self, settings):
        messages = []
        new_settings = {}
        if u'host' in settings:
            if settings[u'host'] == u'localhost' or self.is_valid_ipv4(settings[u'host']):
                new_settings[u'host'] = settings[u'host']
            else:
                new_settings[u'host'] = DefaultSettings.db[u'host']
                messages.append(u'Datenbank-Parameter "host" muss "localhost" oder eine gültige IPv4 Adresse sein.')
        else:
            new_settings[u'host'] = DefaultSettings.db[u'host']
            messages.append(u'Datenbankeinstellungen brauch einen Parameter "host".')
        if u'user' in settings:
            if isinstance(settings[u'user'], unicode):
                new_settings[u'user'] = settings[u'user']
            else:
                new_settings[u'user'] = DefaultSettings.db[u'user']
                messages.append(u'Datenbank-Parameter "user" muss vom Typ unicode sein.')
        else:
            new_settings[u'user'] = DefaultSettings.db[u'user']
            messages.append(u'Datenbankeinstellungen brauch einen Parameter "user".')
        if u'passwd' in settings:
            if isinstance(settings[u'passwd'], unicode):
                new_settings[u'passwd'] = settings[u'passwd']
            else:
                new_settings[u'passwd'] = DefaultSettings.db[u'passwd']
                messages.append(u'Datenbank-Parameter "passwd" muss vom Typ unicode sein.')
        else:
            new_settings[u'passwd'] = DefaultSettings.db[u'passwd']
            messages.append(u'Datenbankeinstellungen brauch einen Parameter "passwd".')
        if u'port' in settings:
            if isinstance(settings[u'port'], int):
                if settings[u'port'] in range(65536):
                    new_settings[u'port'] = settings[u'port']
                else:
                    new_settings[u'port'] = DefaultSettings.db[u'port']
                    messages.append(u'Datenbank-Parameter "port" außerhalb des zulässigen Wertebereichs.')
            else:
                new_settings[u'port'] = DefaultSettings.db[u'port']
                messages.append(u'Datenbank-Parameter "port" muss vom Typ int sein.')
        else:
            new_settings[u'port'] = DefaultSettings.db[u'port']
            messages.append(u'Datenbankeinstellungen brauch einen Parameter "port".')
        if u'charset' in settings:
            if isinstance(settings[u'charset'], unicode):
                new_settings[u'charset'] = settings[u'charset']
            else:
                new_settings[u'charset'] = DefaultSettings.db[u'charset']
                messages.append(u'Datenbank-Parameter "charset" muss vom Typ unicode sein.')
        else:
            new_settings[u'charset'] = DefaultSettings.db[u'charset']
            messages.append(u'Datenbankeinstellungen brauch einen Parameter "charset".')
        return new_settings, messages

    def get_general_settings(self):
        return self.general_settings

    def get_timer_settings(self):
        return self.timer_settings

    def get_files_settings(self):
        return self.files_settings

    def get_db_settings(self):
        return self.db_settings

    def write_general_settings(self):
        try:
            settings_file = codecs.open(self.general_json_path, 'w', encoding='utf-8')
            json.dump(self.general_settings, settings_file)
            settings_file.close()
        except:
            self.master.mh.addMessage(u'Allgemeine Einstellungen konnten nicht in Datei geschrieben werden.',
                                      level=MDConst.MESSAGE_ERROR, requested=False)

    def write_timer_settings(self):
        try:
            settings_file = codecs.open(self.timer_json_path, 'w', encoding='utf-8')
            json.dump(self.timer_settings, settings_file)
            settings_file.close()
        except:
            self.master.mh.addMessage(u'Zeiteinstellungen konnten nicht in Datei geschrieben werden.',
                                      level=MDConst.MESSAGE_ERROR, requested=False)

    def write_files_settings(self):
        try:
            settings_file = codecs.open(self.files_json_path, 'w', encoding='utf-8')
            json.dump(self.files_settings, settings_file)
            settings_file.close()
        except:
            self.master.mh.addMessage(u'Einstellungen zur Dateien konnten nicht in Datei geschrieben werden.',
                                      level=MDConst.MESSAGE_ERROR, requested=False)

    def write_db_settings(self):
        try:
            settings_file = codecs.open(self.db_json_path, 'w', encoding='utf-8')
            json.dump(self.db_settings, settings_file)
            settings_file.close()
        except:
            self.master.mh.addMessage(u'Einstellungen zur Datenbank konnten nicht in Datei geschrieben werden.',
                                      level=MDConst.MESSAGE_ERROR, requested=False)

    def write_all_settings(self):
        self.write_general_settings()
        self.write_timer_settings()
        self.write_files_settings()
        self.write_db_settings()
