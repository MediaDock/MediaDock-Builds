# -*- coding: utf-8 -*-

import os

WINDOWS = False
LINUX = True

def getLocalAppDataPath():
    return os.path.abspath(os.path.join(os.environ[u'HOME'], u'.MediaSuite', u'MediaDock'))

def getVolumeFreeSpace(device):
    #temp = os.popen('df /dev/%s' % device).read().split('\n')[1].split(' ')
    #while '' in temp:
    #    temp.remove('')
    #return int(temp[3]) * 1024
    temp = os.popen(u'df /dev/%s -k' % device).read().decode(u'utf-8').split('\n')
    start_index = temp[0].find(u'Verfügbar')
    return int(temp[1][start_index:].strip().split(' ')[0].strip()) * 1024

def getVolumeTotalSpace(device):
    #temp = os.popen('df /dev/%s' % device).read().decode('utf-8').split('\n')[1].split(' ')
    #while '' in temp:
    #    temp.remove('')
    #return int(temp[1]) * 1024
    temp = os.popen(u'df /dev/%s -k' % device).read().decode(u'utf-8').split('\n')
    start_index = temp[0].find(u'1K-Blöcke')
    return int(temp[1][start_index:].strip().split(' ')[0].strip()) * 1024

def getVolumeName(device):
    #temp = os.popen('df /dev/%s' % device).read().decode('utf-8').split('\n')[1]
    #temp = temp.strip()
    #while '  ' in temp:
    #    temp = temp.replace('  ', ' ')
    #name = temp.split(' ')[6]
    #return name
    return __getDeviceMountpoint(device).split('/')[-1]

def __getDeviceFileSystemType(device):
    temp = os.popen(u'df /dev/%s -T -k' % device).read().decode(u'utf-8').split('\n')
    start_index = temp[0].find(u'Typ')
    end_index = temp[0].find(u'1K-Blöcke')
    fst = temp[1][start_index: end_index].strip()
    return fst

def __getDeviceInformations(device):
    temp = os.popen(u'udisks --show-info /dev/%s' % device).read().decode(u'utf-8').split('\n')
    infos = dict()
    first_key = ''
    for line in temp[2:]:
        nr_leading_spaces = len(line) - len(line.lstrip(u' '))
        if nr_leading_spaces == 2:
            splitted_line = line[2:].split(u':')
            first_key = splitted_line[0]
            value = u':'.join(splitted_line[1:]).strip()
            infos[first_key] = value
        elif nr_leading_spaces == 4:
            splitted_line = line[4:].split(':')
            key = first_key + u'_' + splitted_line[0]
            value = u':'.join(splitted_line[1:]).strip()
            infos[key] = value
    return infos

def __getDeviceMountpoint(device):
    temp = os.popen(u'df /dev/%s' % device).read().decode(u'utf-8').split('\n')
    index = temp[0].find(u'Eingehängt auf')
    mpt = temp[1][index:]
    if not os.path.isdir(mpt):
        raise ArithmeticError() # todo message/ error
    return mpt

#def __getVolumeSize(device):
#    temp = os.popen(u'df /dev/%s' % device).read().decode(u'utf-8').split('\n')

def getVolumeInfo(device):
    mountpoint = __getDeviceMountpoint(device)
    totalSpace = getVolumeTotalSpace(device)
    freeSpace = getVolumeFreeSpace(device)
    infos = __getDeviceInformations(device)
    name = infos.get(u'label', u'Unbenannter Datenträger')
    if name == u'':
        name = u'Unbenannter Datenträger'
    #name = infos.get('name', getVolumeName(device))
    file_system_type = infos.get(u'version', __getDeviceFileSystemType(device))
    return [mountpoint, name, totalSpace, freeSpace, file_system_type]

def __checkVolumeIsProperlyMounted(device):
    mp = __getDeviceMountpoint(device)
    if mp in (u'/dev', u'[SWAP]', u'/home', u'/'):
        return False # todo not start with /media/
    return True

def __checkVolumeIsRemovable(device):
    try:
        temp = os.popen(u'cat /sys/block/%s/removable' % device.rstrip(u'0123456789')).read().decode(u'utf-8')
        if temp.strip() == '0':
            return False
        return True
    except: #todo
        return True

def __checkDeviceType(device, types):
    temp = os.popen(u'lsblk -o NAME,TYPE /dev/%s' % device).read().decode(u'utf-8').split('\n')
    dev_type = temp[1].strip(u'├└─ ').split(' ')[-1]
    return dev_type in types

def getVolumesList(removableDrivesOnly, useExclusionList, excludedDrives):
    volume_list_str = os.popen(u'df').read().decode(u'utf-8').split('\n')
    blk_index = volume_list_str[0].index(u'1K-Blöcke')
    volume_list = []
    for line in volume_list_str[1:]:
        full_device = line[:blk_index - 1]
        if full_device.startswith(u'/dev/'):
            device = full_device[5:].rstrip()
            mountpoint = __getDeviceMountpoint(device)
            if mountpoint.startswith(u'/media/') and __checkDeviceType(device, ('disk', 'part')) and \
                    (not removableDrivesOnly or __checkVolumeIsRemovable(device)) and \
                    (not useExclusionList or (device.strip(u'0123456789') not in excludedDrives)): # todo testen
                    volume_list.append(device)
    return volume_list


'''
def getVolumesList(removableDrivesOnly, useExclusionList, excludedDrives):
    volume_list_str = os.popen(u'lsblk -o NAME,TYPE').read().decode(u'utf-8').split('\n')
    idx_type = volume_list_str[0].index(u'TYPE')
    volume_list = []
    for i in volume_list_str[2:]:
        print i, 'debug'
        if i[idx_type:] == u'part': #'part' in i and i[idx_type:idx_type+4] == 'part':  # todo linux removable drives only / use exclusion list
            #if 'part' in i and i[idx_type:idx_type+4] == 'part':  # todo linux removable drives only / use exclusion list
            device = i.split(' ')[0].lstrip(u'├└─')
            #if not device.startswith('sda') and __checkVolumeIsPropperlyMounted(device):
            if __checkVolumeIsProperlyMounted(device) and \
               (not removableDrivesOnly or __checkVolumeIsRemovable(device)) and \
               (not useExclusionList or (device.strip(u'0123456789') not in excludedDrives)): # todo testen
                volume_list.append(device)
    return volume_list
'''

def isHiddenPath(dir_name):
    if not os.path.exists(dir_name):
        return False
    temp = dir_name.split(os.sep)
    for i in temp:
        if i.startswith('.'):
            return True
    return False
    #return dir_name.startswith('.')

def ejectDrive(device, message_handler):
    temp = os.popen(u'udisks --unmount /dev/%s' % device).read()
    if temp == '':
        temp2 = os.popen(u'udisks --detach /dev/%s' % device.strip(u'0123456789')).read()
        if temp2 == '':
            return True
        else:
            message_handler.addMessage(u'Laufwerk wurde entfehrnt, aber es sind noch andere Laufwerke dieses '
                                       u'Datenträgers angeschlosssen', section=[0, 1],
                                       level=1) # todo Laufwerke ersetzen linux
            pass # todo message stick nicht entfehrnen  rückgabewert??  linux
            return True
    else:
        return False

    
def isValidDriveIdentifier(identifier):
    if identifier.startswith(u'/dev/'):
        return True
    return False
