# -*- coding: utf-8 -*-

__author__ = 'taken form the lib'


import posixpath
import urllib
import cgi
import shutil
import mimetypes
import thread
import json
try:
    from cStringIO import StringIO
except ImportError:
    from StringIO import StringIO

import sys
import time
from warnings import filterwarnings, catch_warnings
with catch_warnings():
    if sys.py3kwarning:
        filterwarnings("ignore", ".*mimetools has been removed",
                       DeprecationWarning)
    import mimetools

import socket
import select
import os
import errno
try:
    import threading
except ImportError:
    import dummy_threading as threading

import EncrConst
import MDConst

# Default error message template
DEFAULT_ERROR_MESSAGE = """\
<head>
<title>Error response</title>
</head>
<body>
<h1>Error response</h1>
<p>Error code %(code)d.
<p>Message: %(message)s.
<p>Error code explanation: %(code)s = %(explain)s.
</body>
"""

DEFAULT_ERROR_CONTENT_TYPE = "text/html"


class BaseRequestHandler:  # braucht man wirklicht

    """Base class for request handler classes.

    This class is instantiated for each request to be handled.  The
    constructor sets the instance variables request, client_address
    and server, and then calls the handle() method.  To implement a
    specific service, all you need to do is to derive a class which
    defines a handle() method.

    The handle() method can find the request as self.request, the
    client address as self.client_address, and the server (in case it
    needs access to per-server information) as self.server.  Since a
    separate instance is created for each request, the handle() method
    can define arbitrary other instance variariables.

    """

    def __init__(self, request, client_address, server, engine_interface):
        self.request = request
        self.client_address = client_address
        self.server = server
        self.setup()
        self.ei = engine_interface
        try:
            self.handle()
        finally:
            self.finish()

    def setup(self):
        pass

    def handle(self):
        pass

    def finish(self):
        pass


# The following two classes make it possible to use the same service
# class for stream or datagram servers.
# Each class sets up these instance variables:
# - rfile: a file object from which receives the request is read
# - wfile: a file object to which the reply is written
# When the handle() method returns, wfile is flushed properly


class StreamRequestHandler(BaseRequestHandler):  # braucht man wirklich

    """Define self.rfile and self.wfile for stream sockets."""

    # Default buffer sizes for rfile, wfile.
    # We default rfile to buffered because otherwise it could be
    # really slow for large data (a getc() call per byte); we make
    # wfile unbuffered because (a) often after a write() we want to
    # read and we need to flush the line; (b) big writes to unbuffered
    # files are typically optimized by stdio even when big reads
    # aren't.
    rbufsize = -1
    wbufsize = 0

    # A timeout to apply to the request socket, if not None.
    timeout = None

    # Disable nagle algorithm for this socket, if True.
    # Use only when wbufsize != 0, to avoid small packets.
    disable_nagle_algorithm = False

    def setup(self):
        self.connection = self.request
        if self.timeout is not None:
            self.connection.settimeout(self.timeout)
        if self.disable_nagle_algorithm:
            self.connection.setsockopt(socket.IPPROTO_TCP,
                                       socket.TCP_NODELAY, True)
        self.rfile = self.connection.makefile('rb', self.rbufsize)
        self.wfile = self.connection.makefile('wb', self.wbufsize)

    def finish(self):
        '''
        if not self.wfile.closed:
            try:
                self.wfile.flush()
            except socket.error:
                # An final socket error may have occurred here, such as
                # the local error ECONNABORTED.
                pass
        self.wfile.close() # todo hier gibt es fehler / try- catchblock #jeden erfolg und fehler im System eintragen
        self.rfile.close()
        '''

        self.ei.incrNumberOfRequests()
        try:
            if not self.wfile.closed:
                self.wfile.flush()
            self.wfile.close()
        except Exception as exc:
            self.ei.incrNumberOfFinishErrors()
            self.ei.master.l.logServer(u'Exception write wfile: %s Number: %i' % (exc, self.ei.numberOfFinishErrors),
                                       logLevel=MDConst.LOG_ERROR)
        self.rfile.close()


class BaseHTTPRequestHandler(StreamRequestHandler):  # braucht man wirklich

    """HTTP request handler base class.

    The following explanation of HTTP serves to guide you through the
    code as well as to expose any misunderstandings I may have about
    HTTP (so you don't need to read the code to figure out I'm wrong
    :-).

    HTTP (HyperText Transfer Protocol) is an extensible protocol on
    top of a reliable stream transport (e.g. TCP/IP).  The protocol
    recognizes three parts to a request:

    1. One line identifying the request type and path
    2. An optional set of RFC-822-style headers
    3. An optional data part

    The headers and data are separated by a blank line.

    The first line of the request has the form

    <command> <path> <version>

    where <command> is a (case-sensitive) keyword such as GET or POST,
    <path> is a string containing path information for the request,
    and <version> should be the string "HTTP/1.0" or "HTTP/1.1".
    <path> is encoded using the URL encoding scheme (using %xx to signify
    the ASCII character with hex code xx).

    The specification specifies that lines are separated by CRLF but
    for compatibility with the widest range of clients recommends
    servers also handle LF.  Similarly, whitespace in the request line
    is treated sensibly (allowing multiple spaces between components
    and allowing trailing whitespace).

    Similarly, for output, lines ought to be separated by CRLF pairs
    but most clients grok LF characters just fine.

    If the first line of the request has the form

    <command> <path>

    (i.e. <version> is left out) then this is assumed to be an HTTP
    0.9 request; this form has no optional headers and data part and
    the reply consists of just the data.

    The reply form of the HTTP 1.x protocol again has three parts:

    1. One line giving the response code
    2. An optional set of RFC-822-style headers
    3. The data

    Again, the headers and data are separated by a blank line.

    The response code line has the form

    <version> <responsecode> <responsestring>

    where <version> is the protocol version ("HTTP/1.0" or "HTTP/1.1"),
    <responsecode> is a 3-digit response code indicating success or
    failure of the request, and <responsestring> is an optional
    human-readable string explaining what the response code means.

    This server parses the request and the headers, and then calls a
    function specific to the request type (<command>).  Specifically,
    a request SPAM will be handled by a method do_SPAM().  If no
    such method exists the server sends an error response to the
    client.  If it exists, it is called with no arguments:

    do_SPAM()

    Note that the request name is case sensitive (i.e. SPAM and spam
    are different requests).

    The various request details are stored in instance variables:

    - client_address is the client IP address in the form (host,
    port);

    - command, path and version are the broken-down request line;

    - headers is an instance of mimetools.Message (or a derived
    class) containing the header information;

    - rfile is a file object open for reading positioned at the
    start of the optional input data part;

    - wfile is a file object open for writing.

    IT IS IMPORTANT TO ADHERE TO THE PROTOCOL FOR WRITING!

    The first thing to be written must be the response line.  Then
    follow 0 or more header lines, then a blank line, and then the
    actual data (if any).  The meaning of the header lines depends on
    the command executed by the server; in most cases, when data is
    returned, there should be at least one header line of the form

    Content-type: <type>/<subtype>

    where <type> and <subtype> should be registered MIME types,
    e.g. "text/html" or "text/plain".

    """

    # The Python system version, truncated to its first component.
    sys_version = "Python/" + sys.version.split()[0]

    # The server software version.  You may want to override this.
    # The format is multiple whitespace-separated strings,
    # where each string is of the form name[/version].
    server_version = "BaseHTTP/"

    # The default request version.  This only affects responses up until
    # the point where the request line is parsed, so it mainly decides what
    # the client gets back when sending a malformed request line.
    # Most web servers default to HTTP 0.9, i.e. don't send a status line.
    default_request_version = "HTTP/0.9"

    def __init__(self, arg1, arg2, arg3, engine_interface):
        self.ei = engine_interface
        StreamRequestHandler.__init__(self, arg1, arg2, arg3, self.ei)

    def parse_request(self):
        """Parse a request (internal).

        The request should be stored in self.raw_requestline; the results
        are in self.command, self.path, self.request_version and
        self.headers.

        Return True for success, False for failure; on failure, an
        error is sent back.

        """
        self.command = None  # set in case of error on the first line
        self.request_version = version = self.default_request_version
        self.close_connection = 1
        requestline = self.raw_requestline
        requestline = requestline.rstrip('\r\n')
        self.requestline = requestline
        words = requestline.split()
        if len(words) == 3:
            command, path, version = words
            if version[:5] != 'HTTP/':
                self.send_error(400, "Bad request version (%r)" % version)
                return False
            try:
                base_version_number = version.split('/', 1)[1]
                version_number = base_version_number.split(".")
                # RFC 2145 section 3.1 says there can be only one "." and
                #   - major and minor numbers MUST be treated as
                #      separate integers;
                #   - HTTP/2.4 is a lower version than HTTP/2.13, which in
                #      turn is lower than HTTP/12.3;
                #   - Leading zeros MUST be ignored by recipients.
                if len(version_number) != 2:
                    raise ValueError
                version_number = int(version_number[0]), int(version_number[1])
            except (ValueError, IndexError):
                self.send_error(400, "Bad request version (%r)" % version)
                return False
            if version_number >= (1, 1) and self.protocol_version >= "HTTP/1.1":
                self.close_connection = 0
            if version_number >= (2, 0):
                self.send_error(505,
                                "Invalid HTTP Version (%s)" % base_version_number)
                return False
        elif len(words) == 2:
            command, path = words
            self.close_connection = 1
            if command != 'GET':
                self.send_error(400,
                                "Bad HTTP/0.9 request type (%r)" % command)
                return False
        elif not words:
            return False
        else:
            self.send_error(400, "Bad request syntax (%r)" % requestline)
            return False
        self.command, self.path, self.request_version = command, path, version

        # Examine the headers and look for a Connection directive
        self.headers = self.MessageClass(self.rfile, 0)

        conntype = self.headers.get('Connection', "")
        if conntype.lower() == 'close':
            self.close_connection = 1
        elif (conntype.lower() == 'keep-alive' and
              self.protocol_version >= "HTTP/1.1"):
            self.close_connection = 0
        return True

    def getRawRequestline(self):
        try:
            self.raw_requestline = self.rfile.readline(65537)
        except Exception as exc:
            self.raw_requestline = ''
            self.ei.master.l.logServer(u'Exception in get requestline: %s; ip: %s' % (exc, self.client_address[0]),
                                       logLevel=MDConst.LOG_ERROR)
        self.raw_requestline_read = True

    def handle_one_request(self):
        """Handle a single HTTP request.

        You normally don't need to override this method; see the class
        __doc__ string for information on how to handle specific HTTP
        commands such as GET and POST.

        """
        self.raw_requestline = ''
        self.raw_requestline_read = False
        try:
            #self.raw_requestline = self.rfile.readline(65537)  #  hier bleibt er hängen
            thread.start_new_thread(self.getRawRequestline, (),) # das hier ist als laufzeiüberwachung gedacht
            x = 0
            timespan = 6000
            for x in range(timespan):
                if self.raw_requestline_read:
                    break
                time.sleep(0.001)
            if x == (timespan - 1): # if self.raw_requestline == '':
                self.ei.master.l.logServer(u'Error 408', logLevel=MDConst.LOG_ERROR)
                self.requestline = ''
                self.request_version = ''
                self.command = ''
                self.send_error(408)
                return
            if len(self.raw_requestline) > 65536:
                self.requestline = ''
                self.request_version = ''
                self.command = ''
                self.send_error(414)
                return
            if not self.raw_requestline:
                self.close_connection = 1
                return
            if not self.parse_request():
                # An error code has been sent, just exit
                return
            mname = 'do_' + self.command
            if not mname == 'do_GET':
                self.ei.master.l.logServer(u'Error 501', logLevel=MDConst.LOG_ERROR)
                self.send_error(501, "Unsupported method (%r)" % self.command) # für die Sicherheit
                return
            if not hasattr(self, mname):
                self.ei.master.l.logServer(u'Error 501', logLevel=MDConst.LOG_ERROR)
                self.send_error(501, "Unsupported method (%r)" % self.command)
                return
            method = getattr(self, mname)
            method()
            self.wfile.flush()  # actually send the response if not already done.
        except socket.timeout, e:
            #a read or a write timed out.  Discard this connection
            self.log_error("Request timed out: %r", e)
            self.close_connection = 1
            return

    def handle(self):
        """Handle multiple requests if necessary."""
        self.close_connection = 1
        self.handle_one_request()
        while not self.close_connection:
            self.handle_one_request()

    def send_error(self, code, message=None):
        """Send and log an error reply.

        Arguments are the error code, and a detailed message.
        The detailed message defaults to the short entry matching the
        response code.

        This sends an error response (so it must be called before any
        output has been generated), logs the error, and finally sends
        a piece of HTML explaining the error to the user.

        """

        try:
            short, long = self.responses[code]
        except KeyError:
            short, long = '???', '???'
        if message is None:
            message = short
        explain = long
        self.log_error("code %d, message %s", code, message)
        # using _quote_html to prevent Cross Site Scripting attacks (see bug #1100201)
        content = (self.error_message_format %
                   {'code': code, 'message': _quote_html(message), 'explain': explain})
        self.send_response(code, message)
        self.send_header("Content-Type", self.error_content_type)
        self.send_header('Connection', 'close')
        self.end_headers()
        if self.command != 'HEAD' and code >= 200 and code not in (204, 304):
            self.wfile.write(content)

    error_message_format = DEFAULT_ERROR_MESSAGE
    error_content_type = DEFAULT_ERROR_CONTENT_TYPE

    def send_response(self, code, message=None):
        """Send the response header and log the response code.

        Also send two standard headers with the server software
        version and the current date.

        """
        self.log_request(code)
        if message is None:
            if code in self.responses:
                message = self.responses[code][0]
            else:
                message = ''
        if self.request_version != 'HTTP/0.9':
            self.wfile.write("%s %d %s\r\n" %
                             (self.protocol_version, code, message))
            # print (self.protocol_version, code, message)
        self.send_header('Server', self.version_string())
        self.send_header('Date', self.date_time_string())

    def send_header(self, keyword, value):
        """Send a MIME header."""
        if self.request_version != 'HTTP/0.9':
            self.wfile.write("%s: %s\r\n" % (keyword, value))

        if keyword.lower() == 'connection':
            if value.lower() == 'close':
                self.close_connection = 1
            elif value.lower() == 'keep-alive':
                self.close_connection = 0

    def end_headers(self):
        """Send the blank line ending the MIME headers."""
        if self.request_version != 'HTTP/0.9':
            self.wfile.write("\r\n")

    def log_request(self, code='-', size='-'):
        """Log an accepted request.

        This is called by send_response().

        """

        self.log_message('"%s" %s %s',
                         self.requestline, str(code), str(size))

    def log_error(self, format, *args):
        """Log an error.

        This is called when a request cannot be fulfilled.  By
        default it passes the message on to log_message().

        Arguments are the same as for log_message().

        XXX This should go to the separate error log.

        """

        self.log_message(format, *args)

    def log_message(self, format, *args):
        """Log an arbitrary message.

        This is used by all other logging functions.  Override
        it if you have specific logging wishes.

        The first argument, FORMAT, is a format string for the
        message to be logged.  If the format string contains
        any % escapes requiring parameters, they should be
        specified as subsequent arguments (it's just like
        printf!).

        The client ip address and current date/time are prefixed to every
        message.

        """
        # hier sind die servermeldungen
        # sys.stderr.write("%s - - [%s] %s\n" % (self.client_address[0], self.log_date_time_string(), format % args))
        temp = format % args
        if '&password=' in temp:
            temp = temp[0:temp.find('&password=')]
        self.ei.master.l.logServer(u"%s - - [%s] %s" % (self.client_address[0], self.log_date_time_string(), temp))

    def version_string(self):
        """Return the server software version string."""
        return self.server_version + ' ' + self.sys_version

    def date_time_string(self, timestamp=None):
        """Return the current date and time formatted for a message header."""
        if timestamp is None:
            timestamp = time.time()
        year, month, day, hh, mm, ss, wd, y, z = time.gmtime(timestamp)
        s = "%s, %02d %3s %4d %02d:%02d:%02d GMT" % (self.weekdayname[wd], day, self.monthname[month], year, hh, mm, ss)
        return s

    def log_date_time_string(self):
        """Return the current time formatted for logging."""
        now = time.time()
        year, month, day, hh, mm, ss, x, y, z = time.localtime(now)
        s = "%02d/%3s/%04d %02d:%02d:%02d" % (
                day, self.monthname[month], year, hh, mm, ss)
        return s

    weekdayname = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']

    monthname = [None,
                 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
                 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

    def address_string(self):
        """Return the client address formatted for logging.

        This version looks up the full hostname using gethostbyaddr(),
        and tries to find a name that contains at least one dot.

        """

        host, port = self.client_address[:2]
        return host # nach einer stackoverfolw empfehlung geändert
        #return socket.getfqdn(host)

    # Essentially static class variables

    # The version of the HTTP protocol we support.
    # Set this to HTTP/1.1 to enable automatic keepalive
    protocol_version = "HTTP/1.0"

    # The Message-like class used to parse headers
    MessageClass = mimetools.Message

    # Table mapping response codes to messages; entries have the
    # form {code: (shortmessage, longmessage)}.
    # See RFC 2616.
    responses = {
        100: ('Continue', 'Request received, please continue'),
        101: ('Switching Protocols',
              'Switching to new protocol; obey Upgrade header'),

        200: ('OK', 'Request fulfilled, document follows'),
        201: ('Created', 'Document created, URL follows'),
        202: ('Accepted',
              'Request accepted, processing continues off-line'),
        203: ('Non-Authoritative Information', 'Request fulfilled from cache'),
        204: ('No Content', 'Request fulfilled, nothing follows'),
        205: ('Reset Content', 'Clear input form for further input.'),
        206: ('Partial Content', 'Partial content follows.'),

        300: ('Multiple Choices',
              'Object has several resources -- see URI list'),
        301: ('Moved Permanently', 'Object moved permanently -- see URI list'),
        302: ('Found', 'Object moved temporarily -- see URI list'),
        303: ('See Other', 'Object moved -- see Method and URL list'),
        304: ('Not Modified',
              'Document has not changed since given time'),
        305: ('Use Proxy',
              'You must use proxy specified in Location to access this '
              'resource.'),
        307: ('Temporary Redirect',
              'Object moved temporarily -- see URI list'),

        400: ('Bad Request',
              'Bad request syntax or unsupported method'),
        401: ('Unauthorized',
              'No permission -- see authorization schemes'),
        402: ('Payment Required',
              'No payment -- see charging schemes'),
        403: ('Forbidden',
              'Request forbidden -- authorization will not help'),
        404: ('Not Found', 'Nothing matches the given URI'),
        405: ('Method Not Allowed',
              'Specified method is invalid for this resource.'),
        406: ('Not Acceptable', 'URI not available in preferred format.'),
        407: ('Proxy Authentication Required', 'You must authenticate with '
              'this proxy before proceeding.'),
        408: ('Request Timeout', 'Request timed out; try again later.'),
        409: ('Conflict', 'Request conflict.'),
        410: ('Gone',
              'URI no longer exists and has been permanently removed.'),
        411: ('Length Required', 'Client must specify Content-Length.'),
        412: ('Precondition Failed', 'Precondition in headers is false.'),
        413: ('Request Entity Too Large', 'Entity is too large.'),
        414: ('Request-URI Too Long', 'URI is too long.'),
        415: ('Unsupported Media Type', 'Entity body in unsupported format.'),
        416: ('Requested Range Not Satisfiable',
              'Cannot satisfy request range.'),
        417: ('Expectation Failed',
              'Expect condition could not be satisfied.'),

        500: ('Internal Server Error', 'Server got itself in trouble'),
        501: ('Not Implemented',
              'Server does not support this operation'),
        502: ('Bad Gateway', 'Invalid responses from another server/proxy.'),
        503: ('Service Unavailable',
              'The server cannot process the request due to a high load'),
        504: ('Gateway Timeout',
              'The gateway server did not receive a timely response'),
        505: ('HTTP Version Not Supported', 'Cannot fulfill request.'),
        }


class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):  # braucht man wirklich

    """Simple HTTP request handler with GET and HEAD commands.

    This serves files from the current directory and any of its
    subdirectories.  The MIME type for files is determined by
    calling the .guess_type() method.

    The GET and HEAD requests are identical except that the HEAD
    request omits the actual contents of the file.

    """

    server_version = "SimpleHTTP/"

    def do_GET(self):
        """Serve a GET request."""
        f = self.send_head()
        if f:
            try:
                self.copyfile(f, self.wfile)
            finally:
                f.close()

    def do_HEAD(self):
        """Serve a HEAD request."""
        f = self.send_head()
        if f:
            f.close()

    def send_head(self):
        """Common code for GET and HEAD commands.

        This sends the response code and MIME headers.

        Return value is either a file object (which has to be copied
        to the outputfile by the caller unless the command was HEAD,
        and must be closed by the caller under all circumstances), or
        None, in which case the caller has nothing further to do.

        """
        path = self.translate_path(self.path)
        f = None  # this is used
        if os.path.isdir(path):
            if not self.path.endswith('/'):
                # redirect browser - doing basically what apache does
                self.send_response(301)
                self.send_header("Location", self.path + "/")
                self.end_headers()
                return None
            for index in "index.html", "index.htm":
                index = os.path.join(path, index)
                if os.path.exists(index):
                    path = index
                    break
            else:
                return self.list_directory(path)
        ctype = self.guess_type(path)
        try:
            # Always read in binary mode. Opening files in text mode may cause
            # newline translations, making the actual size of the content
            # transmitted *less* than the content-length!
            f = open(path, 'rb')
        except IOError:
            self.send_error(404, "File not found")
            return None
        try:
            self.send_response(200)
            self.send_header("Content-type", ctype)
            fs = os.fstat(f.fileno())
            self.send_header("Content-Length", str(fs[6]))
            self.send_header("Last-Modified", self.date_time_string(fs.st_mtime))
            self.end_headers()
            return f
        except:
            f.close()
            raise

    def list_directory(self, path):
        """Helper to produce a directory listing (absent index_alt.html).

        Return value is either a file object, or None (indicating an
        error).  In either case, the headers are sent, making the
        interface the same as for send_head().

        """
        try:
            list = os.listdir(path)
        except os.error:
            self.send_error(404, "No permission to list directory")
            return None
        list.sort(key=lambda a: a.lower())
        f = StringIO()
        displaypath = cgi.escape(urllib.unquote(self.path))
        f.write('<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">')
        f.write("<html>\n<title>Directory listing for %s</title>\n" % displaypath)
        f.write("<body>\n<h2>Directory listing for %s</h2>\n" % displaypath)
        f.write("<hr>\n<ul>\n")
        for name in list:
            fullname = os.path.join(path, name)
            displayname = linkname = name
            # Append / for directories or @ for symbolic links
            if os.path.isdir(fullname):
                displayname = name + "/"
                linkname = name + "/"
            if os.path.islink(fullname):
                displayname = name + "@"
                # Note: a link to a directory displays with @ and links with /
            f.write('<li><a href="%s">%s</a>\n'
                    % (urllib.quote(linkname), cgi.escape(displayname)))
        f.write("</ul>\n<hr>\n</body>\n</html>\n")
        length = f.tell()
        f.seek(0)
        self.send_response(200)
        encoding = sys.getfilesystemencoding()
        self.send_header("Content-type", "text/html; charset=%s" % encoding)
        self.send_header("Content-Length", str(length))
        self.end_headers()
        return f

    def translate_path(self, path):
        """Translate a /-separated PATH to the local filename syntax.

        Components that mean special things to the local file system
        (e.g. drive or directory names) are ignored.  (XXX They should
        probably be diagnosed.)

        """
        # abandon query parameters
        path = path.split('?',1)[0]
        path = path.split('#',1)[0]
        # Don't forget explicit trailing slash when normalizing. Issue17324
        trailing_slash = path.rstrip().endswith('/')
        path = posixpath.normpath(urllib.unquote(path))
        words = path.split('/')
        words = filter(None, words)
        path = os.getcwd()
        for word in words:
            drive, word = os.path.splitdrive(word)
            head, word = os.path.split(word)
            if word in (os.curdir, os.pardir): continue
            path = os.path.join(path, word)
        if trailing_slash:
            path += '/'
        return path

    def copyfile(self, source, outputfile):
        """Copy all data between two file objects.

        The SOURCE argument is a file object open for reading
        (or anything with a read() method) and the DESTINATION
        argument is a file object open for writing (or
        anything with a write() method).

        The only reason for overriding this would be to change
        the block size or perhaps to replace newlines by CRLF
        -- note however that this the default server uses this
        to copy binary data as well.

        """
        shutil.copyfileobj(source, outputfile)

    def guess_type(self, path):
        """Guess the type of a file.

        Argument is a PATH (a filename).

        Return value is a string of the form type/subtype,
        usable for a MIME Content-type header.

        The default implementation looks the file's extension
        up in the table self.extensions_map, using application/octet-stream
        as a default; however it would be permissible (if
        slow) to look inside the data to make a better guess.

        """

        base, ext = posixpath.splitext(path)
        if ext in self.extensions_map:
            return self.extensions_map[ext]
        ext = ext.lower()
        if ext in self.extensions_map:
            return self.extensions_map[ext]
        else:
            return self.extensions_map['']

    if not mimetypes.inited:
        mimetypes.init() # try to read system mime.types
    extensions_map = mimetypes.types_map.copy()
    extensions_map.update({
        '': 'application/octet-stream', # Default
        '.py': 'text/plain',
        '.c': 'text/plain',
        '.h': 'text/plain',
        })

# See also:
#
# HTTP Working Group                                        T. Berners-Lee
# INTERNET-DRAFT                                            R. T. Fielding
# <draft-ietf-http-v10-spec-00.txt>                     H. Frystyk Nielsen
# Expires September 8, 1995                                  March 8, 1995
#
# URL: http://www.ics.uci.edu/pub/ietf/http/draft-ietf-http-v10-spec-00.txt
#
# and
#
# Network Working Group                                      R. Fielding
# Request for Comments: 2616                                       et al
# Obsoletes: 2068                                              June 1999
# Category: Standards Track
#
# URL: http://www.faqs.org/rfcs/rfc2616.html

# Author of the BaseServer patch: Luke Kenneth Casson Leighton

# XXX Warning!
# There is a test suite for this module, but it cannot be run by the
# standard regression test.
# To run it manually, run Lib/test/test_socketserver.py.


def _eintr_retry(func, *args):  # braucht man wirklich
    """restart a system call interrupted by EINTR"""
    while True:
        try:
            return func(*args)
        except (OSError, select.error) as e:
            if e.args[0] != errno.EINTR:
                raise


class BaseServer:  # braucht man wirklich

    """Base class for server classes.

    Methods for the caller:

    - __init__(server_address, RequestHandlerClass)
    - serve_forever(poll_interval=0.5)
    - shutdown()
    - handle_request()  # if you do not use serve_forever()
    - fileno() -> int   # for select()

    Methods that may be overridden:

    - server_bind()
    - server_activate()
    - get_request() -> request, client_address
    - handle_timeout()
    - verify_request(request, client_address)
    - server_close()
    - process_request(request, client_address)
    - shutdown_request(request)
    - close_request(request)
    - handle_error()

    Methods for derived classes:

    - finish_request(request, client_address)

    Class variables that may be overridden by derived classes or
    instances:

    - timeout
    - address_family
    - socket_type
    - allow_reuse_address

    Instance variables:

    - RequestHandlerClass
    - socket

    """

    timeout = None

    def __init__(self, server_address, request_handler_class, engine_interface, logger):
        """Constructor.  May be extended, do not override."""
        self.server_address = server_address
        self.RequestHandlerClass = request_handler_class
        self.ei = engine_interface
        self.logger = logger
        self.__is_shut_down = threading.Event()
        self.__shutdown_request = False

    def server_activate(self):
        """Called by constructor to activate the server.

        May be overridden.

        """
        pass

    def serve_forever(self, poll_interval=0.5):
        """Handle one request at a time until shutdown.

        Polls for shutdown every poll_interval seconds. Ignores
        self.timeout. If you need to do periodic tasks, do them in
        another thread.
        """
        self.__is_shut_down.clear()
        try:
            while not self.__shutdown_request:
                # XXX: Consider using another file descriptor or
                # connecting to the socket to wake this up instead of
                # polling. Polling reduces our responsiveness to a
                # shutdown request and wastes cpu at all other times.
                r, w, e = _eintr_retry(select.select, [self], [], [],
                                       poll_interval)
                if self in r:
                    self._handle_request_noblock()
        finally:
            self.__shutdown_request = False
            self.__is_shut_down.set()

    def shutdown(self):
        """Stops the serve_forever loop.

        Blocks until the loop has finished. This must be called while
        serve_forever() is running in another thread, or it will
        deadlock.
        """
        self.__shutdown_request = True

        self.__is_shut_down.wait()

    # The distinction between handling, getting, processing and
    # finishing a request is fairly arbitrary.  Remember:
    #
    # - handle_request() is the top-level call.  It calls
    #   select, get_request(), verify_request() and process_request()
    # - get_request() is different for stream or datagram sockets
    # - process_request() is the place that may fork a new process
    #   or create a new thread to finish the request
    # - finish_request() instantiates the request handler class;
    #   this constructor will handle the request all by itself

    def handle_request(self):
        """Handle one request, possibly blocking.

        Respects self.timeout.
        """
        # Support people who used socket.settimeout() to escape
        # handle_request before self.timeout was available.
        timeout = self.socket.gettimeout()
        if timeout is None:
            timeout = self.timeout
        elif self.timeout is not None:
            timeout = min(timeout, self.timeout)
        fd_sets = _eintr_retry(select.select, [self], [], [], timeout)
        if not fd_sets[0]:
            self.handle_timeout()
            return
        self._handle_request_noblock()

    def _handle_request_noblock(self):
        """Handle one request, without blocking.

        I assume that select.select has returned that the socket is
        readable before this function was called, so there should be
        no risk of blocking in get_request().
        """
        try:
            request, client_address = self.get_request()
        except socket.error:
            return
        if self.verify_request(request, client_address):
            try:
                self.process_request(request, client_address)
            except:
                self.handle_error(request, client_address)
                self.shutdown_request(request)

    def handle_timeout(self):
        """Called if no new request arrives within self.timeout.

        Overridden by ForkingMixIn.
        """
        pass

    def verify_request(self, request, client_address):
        """Verify the request.  May be overridden.

        Return True if we should proceed with this request.

        """
        return True

    def process_request(self, request, client_address):
        """Call finish_request.

        Overridden by ForkingMixIn and ThreadingMixIn.

        """
        self.finish_request(request, client_address)
        self.shutdown_request(request)

    def server_close(self):
        """Called to clean-up the server.

        May be overridden.

        """
        pass

    def finish_request(self, request, client_address):
        """Finish one request by instantiating RequestHandlerClass."""
        self.RequestHandlerClass(request, client_address, self, self.ei)

    def shutdown_request(self, request):
        """Called to shutdown and close an individual request."""
        self.close_request(request)

    def close_request(self, request):
        """Called to clean up an individual request."""
        pass

    def handle_error(self, request, client_address):
        """Handle an error gracefully.  May be overridden.

        The default is to print a traceback and continue.

        """
        print '-'*40
        print 'Exception happened during processing of request from',
        print client_address
        import traceback
        traceback.print_exc()  # XXX But this goes to stderr!
        print '-'*40


class TCPServer(BaseServer): # braucht man wirklich

    """Base class for various socket-based server classes.

    Defaults to synchronous IP stream (i.e., TCP).

    Methods for the caller:

    - __init__(server_address, RequestHandlerClass, bind_and_activate=True)
    - serve_forever(poll_interval=0.5)
    - shutdown()
    - handle_request()  # if you don't use serve_forever()
    - fileno() -> int   # for select()

    Methods that may be overridden:

    - server_bind()
    - server_activate()
    - get_request() -> request, client_address
    - handle_timeout()
    - verify_request(request, client_address)
    - process_request(request, client_address)
    - shutdown_request(request)
    - close_request(request)
    - handle_error()

    Methods for derived classes:

    - finish_request(request, client_address)

    Class variables that may be overridden by derived classes or
    instances:

    - timeout
    - address_family
    - socket_type
    - request_queue_size (only for stream sockets)
    - allow_reuse_address

    Instance variables:

    - server_address
    - RequestHandlerClass
    - socket

    """

    address_family = socket.AF_INET

    socket_type = socket.SOCK_STREAM

    request_queue_size = 5

    allow_reuse_address = False

    def __init__(self, server_address, request_handler_class, engine_interface, logger,  bind_and_activate=True):
        """Constructor.  May be extended, do not override."""
        BaseServer.__init__(self, server_address, request_handler_class, engine_interface, logger)
        self.socket = socket.socket(self.address_family,
                                    self.socket_type)
        if bind_and_activate:
            self.server_bind()
            self.server_activate()

    def server_bind(self):
        """Called by constructor to bind the socket.

        May be overridden.

        """
        if self.allow_reuse_address:
            self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.socket.bind(self.server_address)   # todo hier gibt es einen fehler, wenn ein server auf diesem port schon läuft
        self.server_address = self.socket.getsockname()

    def server_activate(self):
        """Called by constructor to activate the server.

        May be overridden.

        """
        self.socket.listen(self.request_queue_size)

    def server_close(self):
        """Called to clean-up the server.

        May be overridden.

        """
        self.socket.close()

    def fileno(self):
        """Return socket file number.

        Interface required by select().

        """
        return self.socket.fileno()

    def get_request(self):
        """Get the request and client address from the socket.

        May be overridden.

        """
        return self.socket.accept()

    def shutdown_request(self, request):
        """Called to shutdown and close an individual request."""
        try:
            # explicitly shutdown.  socket.close() merely releases
            # the socket and waits for GC to perform the actual close.
            request.shutdown(socket.SHUT_WR)
        except socket.error:
            pass  # some platforms may raise ENOTCONN here
        self.close_request(request)

    def close_request(self, request):
        """Called to clean up an individual request."""
        request.close()


def _quote_html(html):
    return html.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;")

class HTTPServer(TCPServer): # braucht man wirklich

    allow_reuse_address = 1    # Seems to make sense in testing environment

    def server_bind(self):
        """Override server_bind to store the server name."""
        TCPServer.server_bind(self)
        host, port = self.socket.getsockname()[:2]
        self.server_name = socket.getfqdn(host)
        self.server_port = port

def encodeAjax(call_str):
    if not '&' in call_str and not '=' in call_str:
        return {'function': call_str}
    d = dict()
    for i in call_str.split('&'):
        data = i.split('=')
        d[data[0]] =  data[1]
    if not d.has_key('function'):
        d['function'] = None
    return d


class EngineRequestHandler(SimpleHTTPRequestHandler):
    """The test example handler."""

    def __init__(self, arg1, arg2, arg3, engine_interface):
        self.ei = engine_interface
        SimpleHTTPRequestHandler.__init__(self, arg1, arg2, arg3, engine_interface)

    def do_GET(self):
        """Serve a GET request."""

        '''
        if self.ei.getUa()[0] and not self.headers.values()[4] == self.ei.getUa()[1]:
            self.ei.master.l.logServer('Error 403')
            self.send_error(403, 'Kein Zugriff falscher Header-code')
            self.wfile.write('Wolltest du mich testen oder was??')
            return
        '''

        #print self.client_address

        if "?" in self.path:
            self.page, self.parameter = self.path.split("?")
            #self.parameters = urlparse.parse_qs(self.parameters)
        else:
            self.page, self.parameter = self.path.split("?")[0], ""
        if self.page == "/ajax":
            parameters = encodeAjax(self.parameter)
            json_answer = json.dumps({'state': 1})
            func = parameters['function']
            if func == 'getVolumes':
                json_answer = json.dumps({"status": 1, "info": self.ei.getDriveMessagesSidebar()})
            elif func == 'getMessages':
                json_answer = json.dumps({"status": 1, "info": self.ei.getMessageMessagesSidebar()})
            elif func == 'getModalMessages':
                info =  self.ei.getModalMessages(self.client_address[0])
                json_answer = json.dumps({"status": 1, 'info': info})
            elif func == 'getJobs':
                json_answer = json.dumps({"status": 1, "info": self.ei.getJobMessagesSidebar()})
            elif func == 'getDrives':
                json_answer = json.dumps({"status": 1, "info": self.ei.getDrives()})
            elif func == 'getDriveMessagesDrive':
                json_answer = json.dumps({"status": 1, "info": self.ei.getDriveMessagesDrives()})
            elif func == 'getExplorerDropdown':
                json_answer = json.dumps({"status": 1, "info": self.ei.getExplorerDropdown()})
            elif func == 'explorerGetDir':
                info, path_above, record_data = self.ei.explorerGetDir(parameters['path'])
                json_answer = json.dumps({"status": 1, "info": info, "prev_path": path_above,
                                          "record_data": record_data})
            elif func.startswith('eject_drive'):
                if self.client_address[0] == '127.0.0.1':
                    self.ei.ejectDrive(self.parameter.split('_')[2])
                json_answer = json.dumps({"status": 1, "info": ""})
            elif func == 'quit':
                if self.client_address[0] == '127.0.0.1':
                    self.ei.quit(self.client_address[0])
                json_answer = json.dumps({'status': 1})
            elif func == 'shutdown':
                self.ei.shutdown(self.client_address[0])
                #self.ei.master.master.start_shutdown_thread()
                json_answer = json.dumps({'status': 1})
            elif func == 'reload_filesettings':
                self.ei.master.fh.load_filesettings()
                json_answer = ''
            elif func == 'getCompoundData':
                json_answer = json.dumps({"status": 1, "info": self.ei.getCompoundData()})
            elif func == 'lock':
                self.ei.lock()
                json_answer = json.dumps({"status": 1})
            elif func == 'unlock':
                self.ei.unlock(self.client_address[0])
                json_answer = json.dumps({'status': 1})
            elif func == 'getRecord':
                json_answer = json.dumps({"status": 1, "info": self.ei.getRecord(parameters['date'])})
            elif func == 'getAllRecords':
                json_answer = json.dumps({"status": 1, "info": self.ei.getAllRecords()})
            elif func == 'startRecordJob':
                if self.client_address[0] == '127.0.0.1':
                    records_list_str = parameters['records'].strip('+').split('+')
                    if not records_list_str == ['']:
                        records_list = [int(i) for i in records_list_str]
                    else:
                        records_list = []
                    check_result = self.ei.checkRecordJob(int(parameters['drive_id']), records_list)
                    #if check_result[0]:
                    self.ei.startRecordJob(int(parameters['drive_id']), records_list)
                    json_answer = json.dumps({"status": 1, 'check': str(check_result[0]), 'message': check_result[1]})
                else:
                    json_answer = json.dumps({'status': 1, 'check': 'False', 'message': u'Aufträge können nur vom '
                                                                                        'lokalhost gestartet werden.'})
            elif func == 'getAllMessages':
                json_answer = json.dumps({"status": 1, "info": self.ei.getAllMessages()})
            elif func == 'cancelRecordJob':
                if self.client_address[0] == '127.0.0.1':
                    try:
                        drive_id_int = int(parameters['drive_id'])
                    except:
                        answer = False
                    else:
                        answer = self.ei.cancelRecordJob(drive_id_int)
                    json_answer = json.dumps({"status": 1, "info": answer})
                else:
                    json_answer = json.dumps({'status': 1, 'info': False})
            elif func == 'deleteDrive':
                try:
                    drive_id_int = int(parameters['drive_id'])
                except:
                    check_result = False, 'Fehler in der Laufwerks-ID'
                else:
                    check_result = self.ei.checkDelete(drive_id_int)
                    if check_result[0]:
                        self.ei.deleteDrive(drive_id_int)
                json_answer = json.dumps({"status": 1, 'check': check_result[0], 'message': check_result[1]})
            elif func == 'stopDeleteDrive':
                if self.client_address[0] == '127.0.0.1':
                    try:
                        drive_id_int = int(parameters['drive_id'])
                    except:
                        pass
                    else:
                        self.ei.stopDeleteDrive(drive_id_int)
                json_answer = json.dumps({"status": 1})
            elif func == 'reload_dh_settings':
                self.ei.reloadDhSettings()
                json_answer = json.dumps({"status": 1})
            elif func == 'reload_time_settings':
                self.ei.reloadTimeSettings()
                json_answer = json.dumps({"status": 1})
            elif func == 'isLocked':
                ans = False
                if self.client_address[0] == '127.0.0.1':
                    ans = self.ei.isLocked()
                json_answer = json.dumps({"status": 1, 'info': ans})
            elif func == 'reload_records':
                self.ei.reloadRecords()
                json_answer = json.dumps({"status": 1})
            elif func == 'getAllInfos':
                json_answer = json.dumps({"status": 1, 'info': self.ei.getAllInfos()})
            elif func == 'set_auto_shutdown':
                self.ei.set_auto_shutdown(self.client_address[0])
                json_answer = json.dumps({'status': 1})
            elif func == 'unset_auto_shutdown':
                self.ei.unset_auto_shutdown(self.client_address[0])
                json_answer = json.dumps({'status': 1})
            elif func == 'set_output_file':
                self.ei.set_logger_output(self.client_address[0], MDConst.LOGGERFILE)
                json_answer = json.dumps({'status': 1})
            elif func == 'set_output_stdout':
                self.ei.set_logger_output(self.client_address[0], MDConst.LOGGERSTDOUT)
                json_answer = json.dumps({'status': 1})
            elif func == 'set_output_file_stdout':
                self.ei.set_logger_output(self.client_address[0], MDConst.LOGGERFILESTDOUT)
                json_answer = json.dumps({'status': 1})
            elif func == 'logOn':
                result = self.ei.logOn(self.client_address[0], parameters['name'], parameters['password'])
                json_answer = json.dumps({'status': 1, 'result': result})
            elif func == 'logOff':
                self.ei.logOff(self.client_address[0])
                json_answer = json.dumps({'status': 1})
            elif func == 'checkSession':
                result = self.ei.checkSession(self.client_address[0])
                json_answer = json.dumps({'status': 1, 'info': result})
            elif func == 'onLocalhost':
                json_answer = json.dumps({'status': 1, 'info': self.client_address[0] == '127.0.0.1'})
            elif func == 'checkState':
                json_answer = json.dumps({'status': 1, 'html': self.ei.get_state_html()})
            elif func == 'get_fsm_state':
                json_answer = json.dumps({'status': 1, 'state': self.ei.get_fsm_state()})
            elif func == 'cancel_sd_start':
                self.ei.cancel_sd_start()
                json_answer = json.dumps({'status': 1})
            elif func == 'cancel_sd_timer':
                self.ei.cancel_sd_timer()
                json_answer = json.dumps({'status': 1})
            elif func == 'cancel_sd':
                self.ei.cancel_sd()
                json_answer = json.dumps({'status': 1})
            elif func == 'cancel_quit':
                self.ei.cancel_quit()
                json_answer = json.dumps({'status': 1})
            elif func == 'set_run':
                self.ei.set_run(self.client_address[0])
                json_answer = json.dumps({'status': 1})
            elif func == 'get_records_stat':
                info = self.ei.get_records_stat(self.client_address[0])
                json_answer = json.dumps({'status': 1, 'info': info})
            elif func == 'get_files_stat':
                info = self.ei.get_files_stat(self.client_address[0])
                json_answer = json.dumps({'status': 1, 'info': info})
            elif func == 'get_drives_stat':
                info = self.ei.get_drives_stat(self.client_address[0])
                json_answer = json.dumps({'status': 1, 'info': info})
            elif func == 'get_root_dir':
                if self.client_address[0] == '127.0.0.1':
                    self.ei.getRootDir() # todo just for local host
                json_answer = json.dumps({'status': 1})
            elif func == 'get_general_settings':
                json_answer = json.dumps({'status': 1})
            elif func == 'get_timer_settings':
                json_answer = json.dumps({'status': 1})
            elif func == 'get_files_settings':
                json_answer = json.dumps({'status': 1})
            elif func == 'check_general_settings':
                json_answer = json.dumps({'status': 1})
            elif func == 'check_timer_settings':
                json_answer = json.dumps({'status': 1})
            elif func == 'check_files_settings':
                json_answer = json.dumps({'status': 1})
            elif func == 'set_general_settings':
                json_answer = json.dumps({'status': 1})
            elif func == 'set_timer_settings':
                json_answer = json.dumps({'status': 1})
            elif func == 'set_files_settings':
                json_answer = json.dumps({'status': 1})
            else:
                self.ei.master.l.logServer(u'Server - Unbekannte Anfrage: ' + unicode(parameters),
                                           logLevel=MDConst.LOG_INFO)
                json_answer = json.dumps({"status": 1, "info": "Unbekannte Anfrage"})
            self.send_response(200)
            self.send_header("Content-type", "text/plain")
            self.end_headers()
            self.wfile.write(json_answer)
        else:
            allowedEndings = ('.html', '.css', '.js', '.png', '.gif', '.ttf', '.woff', '.svg', 'robots.txt')
            ending = False
            for i in allowedEndings:
                if self.page.endswith(i):
                    ending = True
                    break
            if not ending:
                self.ei.master.l.logServer(u'Error 403 unerlaubte Dateiendung: %s IP: %s' %
                                           (self.page, self.client_address[0]), logLevel=MDConst.LOG_ERROR)
                self.send_error(403, 'Kein Zugriff')
                self.wfile.write('Wolltest du mich testen oder was??')
            else:
                if self.page == '/media':
                    if self.parameter.endswith('.mp3'):
                        self.path = self.ei.getMedia(self.parameter)
                    else:
                        self.send_error(404, "File not found")
                f = self.send_head()
                if f:
                    try:
                        self.copyfile(f, self.wfile)
                    finally:
                        f.close()


class Server:
    def __init__(self, logger, engine_interface, port):
        self.logger = logger
        self.server = HTTPServer(("", port), EngineRequestHandler, engine_interface, logger)
        #import ssl
        #self.server.socket = ssl.wrap_socket (self.server.socket, certfile='static/server.pem', server_side=True)
        self.server_thread = threading.Thread(target=self.server.serve_forever, args=(), name='Server')
        self.server_thread.daemon = True

    def start(self):
        #thread.start_new_thread(self.server.serve_forever, ())
        self.server_thread.start()

    def stop(self):
        self.server.shutdown()

if __name__ == "__main__":
    print 'das geht jetzt nicht mehr'