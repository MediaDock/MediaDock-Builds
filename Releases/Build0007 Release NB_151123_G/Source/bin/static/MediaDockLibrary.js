/**
 * Created by Peter on 25.08.2015.
 */


var return_counter = 0;
var return_object = {};

var LOG_DEBUG = 0;
var LOG_INFO = 1;
var LOG_WARNING = 2;
var LOG_ERROR = 3;

function log(message, level){
    level = typeof level !== 'undefined' ? level : LOG_INFO;
    pserver.send('log', JSON.stringify({'message': message, 'level': level}))
}

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

function getJsonData(data, want_return){
    want_return = typeof want_return !== 'undefined' ? want_return : false;
    if (!want_return){
        data_str = JSON.stringify(data);
        pserver.send('ajax', data_str)
    }
    else{
        //return_counter++;
        return_id = 0;
        temp_counter = 0;
        while(return_id == 0){
            if (!return_object.hasOwnProperty(temp_counter)){ // todo prüfen ob das geht vermutung: es kann immer nur einen request zur selben zeit geben, da id immer 0 und bei langen wartezeiten sich auch nichts ändert
                return_object[temp_counter] = 0;
                return_id = temp_counter;
            }
            else{
                temp_counter++;
            }
        }
        return_id = return_counter;
        data['return_id'] = return_id;
        data_str = JSON.stringify(data);
        pserver.send('ajax', data_str);
        time_delta = 1;
        time = 10;
        time_increments = time/time_delta;
        i = 0;
        while(i < time_increments){
            if (return_object.hasOwnProperty(return_id)&&return_object[return_id]!=0){
                return_data = return_object[return_id];
                delete return_object[return_id];
                //log(JSON.stringify(return_data));
                return return_data;
            }
            else{
                sleep(time_delta * 1000);
            }
            i++;
        }
        log('Send timeout error', LOG_ERROR);
        throw Error;
    }
}

//log('GUI started', LOG_INFO);