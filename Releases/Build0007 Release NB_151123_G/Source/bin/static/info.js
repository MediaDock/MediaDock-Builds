function getAllInfos(){
    data = getJsonData({'function': 'getAllInfos'}, true);
    var elem = document.getElementById("info-list");
    var html = '';
    for(var key in data.info) {
         if(data.info.hasOwnProperty(key)) {
             html += '<tr><td>' + key + '</td><td>' + data.info[key] + '</td></tr>';
         }
    }
    elem.innerHTML = html;
}

getAllInfos();
setInterval(getAllInfos, 3000);