RemoveDrive is distributed under the following conditions:

Alle Programme auf dieser Seite funktionieren unter Windows XP und höher. Sie sind Freeware.

Erlaubt:
- Nutzung in jeder Umgebung, auch kommerziell
- Ausliefern zusammen mit anderen Software-Produkten, auch kommerziellen
- Veröffentlichen auf CD/DVD von Computer-Zeitschriften

Nicht erlaubt:
- Verändern von Dateien
- zum Download mittels einer "Downloader-Software" anbieten

http://www.uwe-sieber.de/drivetools.html
