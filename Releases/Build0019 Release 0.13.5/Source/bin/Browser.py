# -*- coding: utf-8 -*-

import  PySide.QtCore as QtCore
import  PySide.QtGui as QtGui
import  PySide.QtWebKit as QtWebKit
import os
#from PySide import QtGui
#from PySide import QtWebKit
import EncrConst
import MDConst

class PyQtBrowser(QtGui.QMainWindow):
    jsSignal = QtCore.Signal(list)

    def __init__(self, master):
        QtGui.QMainWindow.__init__(self)

        self.master = master

        self.setWindowTitle('MediaDock')
        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        self.setWindowIcon(QtGui.QIcon(os.path.abspath(u'../files/music-folder.png')))

        self.centralwidget = QtGui.QWidget(self)

        self.mainLayout = QtGui.QHBoxLayout(self.centralwidget)
        self.mainLayout.setSpacing(0)
        self.mainLayout.setContentsMargins(0,0,0,0)

        self.frame = QtGui.QFrame(self.centralwidget)

        self.gridLayout = QtGui.QVBoxLayout(self.frame)
        self.gridLayout.setContentsMargins(0,0,0,0)
        self.gridLayout.setSpacing(0)

        self.button_back = QtGui.QPushButton(u'< Zurück', self.frame)
        self.button_back.setFixedWidth(75)

        self.button_forward = QtGui.QPushButton(u'Vor >', self.frame)
        self.button_forward.setFixedWidth(75)

        self.button_reload = QtGui.QPushButton(u'Neu Laden', self.frame)
        self.button_reload.setFixedWidth(75)

        self.button_home = QtGui.QPushButton(u'Startseite', self.frame)
        self.button_home.setFixedWidth(75)

        self.button_show_normal = QtGui.QPushButton(u'Normal', self.frame)
        self.button_show_normal.setFixedWidth(75)

        self.button_show_fullscreen = QtGui.QPushButton(u'Voll', self.frame)
        self.button_show_fullscreen.setFixedWidth(75)

        self.label_blank = QtGui.QLabel(self)
        self.label_blank.setMaximumHeight(18)

        self.horizontalLayout = QtGui.QHBoxLayout()

        self.horizontalLayout.addWidget(self.button_back)
        self.horizontalLayout.addWidget(self.button_forward)
        self.horizontalLayout.addWidget(self.button_reload)
        self.horizontalLayout.addWidget(self.button_home)
        self.horizontalLayout.addWidget(self.label_blank)
        self.horizontalLayout.addWidget(self.button_show_normal)
        self.horizontalLayout.addWidget(self.button_show_fullscreen)
        #self.gridLayout.addLayout(self.horizontalLayout)

        self.html_viewer = QtWebKit.QWebView()
        self.html_viewer.setAttribute(QtCore.Qt.WA_AcceptTouchEvents, False)
        #self.html_viewer.setPage(SavePage(master))

        #websettings = self.html_viewer.settings()
        #websettings.setAttribute(QtWebKit.QWebSettings.PluginsEnabled, True)
        #websettings.setAttribute(QtWebKit.QWebSettings.LocalContentCanAccessRemoteUrls, True)
        #websettings.setAttribute(QtWebKit.QWebSettings.LocalContentCanAccessFileUrls, True)
        #QtWebKit.QWebSettings.setAttribute(self.html_viewer.settings(), QtWebKit.QWebSettings.PluginsEnabled, True)
        # this is for javascript
        #QtWebKit.QWebSettings.setAttribute(self.html_viewer.settings(),
        #                                   QtWebKit.QWebSettings.LocalContentCanAccessRemoteUrls, True)
        #QtWebKit.QWebSettings.setAttribute(self.html_viewer.settings(),
        #                                   QtWebKit.QWebSettings.LocalContentCanAccessFileUrls, True)

        #QWebSettings::globalSettings()->setAttribute(QWebSettings::LocalContentCanAccessRemoteUrls, true);
        self.gridLayout.addWidget(self.html_viewer)
        self.mainLayout.addWidget(self.frame)
        self.setCentralWidget(self.centralwidget)

        #self.connect(self.html, QtCore.SIGNAL("loadStarted()"), self.start_wait)
        #self.connect(self.html, QtCore.SIGNAL("loadFinished()"), self.stop_wait)
        self.connect(self.button_back, QtCore.SIGNAL("clicked()"), self.html_viewer.back)
        self.connect(self.button_forward, QtCore.SIGNAL("clicked()"), self.html_viewer.forward)
        #self.connect(self.button_reload, QtCore.SIGNAL("clicked()"), self.reload)
        self.connect(self.button_home, QtCore.SIGNAL("clicked()"), self.load_home)
        self.connect(self.button_show_normal, QtCore.SIGNAL("clicked()"), self.showNormalButton)
        self.connect(self.button_show_fullscreen, QtCore.SIGNAL("clicked()"), self.showFullScreen)

        self.jsSignal.connect(self.executeJs)

    def load_home(self):
        self.master.gi.pah.loadStartPage()

    #def reload(self): todo
    #    self.html_viewer.load(self.html_viewer.url())
    #    self.html_viewer.show()

    def showNormalButton(self):
        temp = self.master.e.sh.checkRight('127.0.0.1', EncrConst.QUIT)
        if temp[0]:
            self.showNormal()
        else:
            self.master.e.mh.addMessage(temp[1], level=MDConst.MESSAGE_INFO, requested=False)

    @QtCore.Slot(list)
    def executeJs(self, var_list):
        js, return_id, return_dict = var_list
        if return_id is None:
            self.html_viewer.page().mainFrame().evaluateJavaScript(js)
        else:
            return_dict[return_id] = self.html_viewer.page().mainFrame().evaluateJavaScript(js)

class Browser:
    def __init__(self, master):
        self.master = master
        self.program = QtGui.QApplication([])
        #self.b = PyQtBrowser(master)
        self.b = None
        self.splash_pix = QtGui.QPixmap(os.path.abspath(u'../files/splash_screen.png'))
       # self.splash = QtGui.QSplashScreen(self.splash_pix, QtCore.Qt.WindowStaysOnTopHint)
        self.splash = QtGui.QSplashScreen(self.splash_pix) # todo
        self.splash.setMask(self.splash_pix.mask())

    def execJS(self, js, return_id=None, return_dict=None):
        self.b.jsSignal.emit([js, return_id, return_dict])

    def setContent(self, content):
        self.b.html_viewer.setHtml(content)

    def startSplashScreen(self):
        self.splash.show()
        self.program.processEvents()
        self.splash.showMessage(' ')
        self.program.processEvents()

    def stopSplashScreen(self):
        self.splash.close()

    def messageBox(self, header, message):
        QtGui.QMessageBox.about(self.b, header, message)

    def openDir(self, defaultDir=None):
        dialog = QtGui.QFileDialog()
        #print QtGui.QFileDialog.getExistingDirectory(self.program)
        print dialog.getExistingDirectory()
        print 'hello hello' # todo

    def exec_(self):
        self.b = PyQtBrowser(self.master)
        self.b.showFullScreen()
        self.stopSplashScreen()
        #self.b.html_viewer.setZoomFactor(0.5)
        #self.b.html_viewer.page().mainFrame().addToJavaScript('')
        #webView.page().mainFrame().addToJavaScriptWindowObject("pyObj", myObj
        self.master.gi.pah.loadStartPage()
        self.program.exec_()

    def stop(self):
        self.program.quit()