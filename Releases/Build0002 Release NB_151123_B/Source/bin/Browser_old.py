# -*- coding: utf-8 -*-

from PySide import QtCore, QtGui, QtWebKit
import EncrConst
import MDConst


class SavePage(QtWebKit.QWebPage):
    def __init__(self, master):
        QtWebKit.QWebPage.__init__(self)
        self.master = master

    def userAgentForUrl(self, url):
        return self.master.save_ua


class PyQtBrowser(QtGui.QMainWindow):
    def __init__(self, master, port):
        QtGui.QMainWindow.__init__(self)

        self.master = master

        self.setWindowTitle('MediaDock')
        self.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        self.setWindowIcon(QtGui.QIcon('../files/music-folder.png'))

        self.centralwidget = QtGui.QWidget(self)

        self.mainLayout = QtGui.QHBoxLayout(self.centralwidget)
        self.mainLayout.setSpacing(0)
        self.mainLayout.setContentsMargins(0,0,0,0)

        self.frame = QtGui.QFrame(self.centralwidget)

        self.gridLayout = QtGui.QVBoxLayout(self.frame)
        self.gridLayout.setContentsMargins(0,0,0,0)
        self.gridLayout.setSpacing(0)

        self.img_wait = QtGui.QMovie('../files/wait.gif')

        self.label_wait = QtGui.QLabel(self)
        self.label_wait.setFixedWidth(18)
        self.label_wait.setFixedHeight(18)
        self.label_wait.setMaximumHeight(18)

        self.button_back = QtGui.QPushButton(u'< Zurück', self.frame)
        self.button_back.setFixedWidth(75)

        self.button_forward = QtGui.QPushButton('Vor >', self.frame)
        self.button_forward.setFixedWidth(75)

        self.button_reload = QtGui.QPushButton('Neu Laden', self.frame)
        self.button_reload.setFixedWidth(75)

        self.button_home = QtGui.QPushButton('Startseite', self.frame)
        self.button_home.setFixedWidth(75)

        self.button_show_normal = QtGui.QPushButton('Normal', self.frame)
        self.button_show_normal.setFixedWidth(75)

        self.button_show_fullscreen = QtGui.QPushButton('Voll', self.frame)
        self.button_show_fullscreen.setFixedWidth(75)

        self.label_blank = QtGui.QLabel(self)
        self.label_blank.setMaximumHeight(18)

        self.horizontalLayout = QtGui.QHBoxLayout()

        self.horizontalLayout.addWidget(self.label_wait)
        self.horizontalLayout.addWidget(self.button_back)
        self.horizontalLayout.addWidget(self.button_forward)
        self.horizontalLayout.addWidget(self.button_reload)
        self.horizontalLayout.addWidget(self.button_home)
        self.horizontalLayout.addWidget(self.label_blank)
        self.horizontalLayout.addWidget(self.button_show_normal)
        self.horizontalLayout.addWidget(self.button_show_fullscreen)
        self.gridLayout.addLayout(self.horizontalLayout)

        self.html_viewer = QtWebKit.QWebView()
        #self.html_viewer.setPage(SavePage(master))

        #websettings = self.html_viewer.settings()
        #websettings.setAttribute(QtWebKit.QWebSettings.PluginsEnabled, True)
        #websettings.setAttribute(QtWebKit.QWebSettings.LocalContentCanAccessRemoteUrls, True)
        #websettings.setAttribute(QtWebKit.QWebSettings.LocalContentCanAccessFileUrls, True)
        QtWebKit.QWebSettings.setAttribute(self.html_viewer.settings(), QtWebKit.QWebSettings.PluginsEnabled, True)
        # this is for javascript
        QtWebKit.QWebSettings.setAttribute(self.html_viewer.settings(),
                                           QtWebKit.QWebSettings.LocalContentCanAccessRemoteUrls, True)
        QtWebKit.QWebSettings.setAttribute(self.html_viewer.settings(),
                                           QtWebKit.QWebSettings.LocalContentCanAccessFileUrls, True)

        #QWebSettings::globalSettings()->setAttribute(QWebSettings::LocalContentCanAccessRemoteUrls, true);
        self.gridLayout.addWidget(self.html_viewer)
        self.mainLayout.addWidget(self.frame)
        self.setCentralWidget(self.centralwidget)

        #self.connect(self.html, QtCore.SIGNAL("loadStarted()"), self.start_wait)
        #self.connect(self.html, QtCore.SIGNAL("loadFinished()"), self.stop_wait)
        self.html_viewer.loadFinished.connect(self.stop_wait)
        self.html_viewer.loadStarted.connect(self.start_wait)
        self.connect(self.button_back, QtCore.SIGNAL("clicked()"), self.html_viewer.back)
        self.connect(self.button_forward, QtCore.SIGNAL("clicked()"), self.html_viewer.forward)
        self.connect(self.button_reload, QtCore.SIGNAL("clicked()"), self.reload)
        self.connect(self.button_home, QtCore.SIGNAL("clicked()"), self.load_home)
        self.connect(self.button_show_normal, QtCore.SIGNAL("clicked()"), self.showNormalButton)
        self.connect(self.button_show_fullscreen, QtCore.SIGNAL("clicked()"), self.showFullScreen)

        self.default_url = 'http://127.0.0.1:' + str(port) + '/index.html'

        self.wait = False

        self.load_home()

    def load_home(self):
        self.html_viewer.load(QtCore.QUrl(self.default_url))
        self.html_viewer.show()

    def reload(self):
        self.html_viewer.load(self.html_viewer.url())
        self.html_viewer.show()

    def stop_wait(self):
        self.wait = False
        self.img_wait.stop()
        self.label_wait.setMovie(QtGui.QMovie())

    def start_wait(self):
        if not self.wait:
            self.wait = True
            self.label_wait.setMovie(self.img_wait)
            self.img_wait.start()

    def showNormalButton(self):
        temp = self.master.e.sh.checkRight('127.0.0.1', EncrConst.QUIT)
        if temp[0]:
            self.showNormal()
        else:
            self.master.e.mh.addMessage(temp[1], level=MDConst.MESSAGE_INFO, requested=False)

class Browser:
    def __init__(self, master):
        self.master = master
        self.program = QtGui.QApplication([])
        #self.b = PyQtBrowser(master)
        self.b = None
        self.splash_pix = QtGui.QPixmap('../files/splash_screen.png')
        self.splash = QtGui.QSplashScreen(self.splash_pix, QtCore.Qt.WindowStaysOnTopHint)
        self.splash.setMask(self.splash_pix.mask())

    def startSplashScreen(self):
        self.splash.show()
        self.program.processEvents()

    def stopSplashScreen(self):
        self.splash.close()

    def messageBox(self, header, message):
        QtGui.QMessageBox.about(self.b, header, message)

    def openDir(self, defaultDir=None):
        dialog = QtGui.QFileDialog()
        #print QtGui.QFileDialog.getExistingDirectory(self.program)
        print dialog.getExistingDirectory()
        print 'hello hello' # todo

    def exec_(self, port):
        self.b = PyQtBrowser(self.master, port)
        self.b.showFullScreen()
        self.stopSplashScreen()
        #self.b.html_viewer.page().mainFrame().addToJavaScript('')
        #webView.page().mainFrame().addToJavaScriptWindowObject("pyObj", myObj
        self.program.exec_()

    def stop(self):
        self.program.quit()