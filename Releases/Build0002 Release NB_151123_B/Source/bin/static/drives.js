function ajax_request(parurl, pardata, callback) {
    data = getJsonData(pardata, true);
    callback(data);
}

function get_volume_messages() {
    ajax_request("ajax", {'function': "getDriveMessagesDrive"}, handle_volume_messages);
}

function get_drives() {
    ajax_request("ajax", {'function': "getDrives"}, handle_drives);
}

function ask_eject_drive(letter, name){
    var text = '<div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><h4 class="modal-title" id="myModalLabel">Laufwerk entfehrnen</h4></div><div class="modal-body">Soll das Laufwerk ' +
    name + ' wirklich entfehrnt werden?</div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal" onclick="eject_drive(' +
    letter + ')">Laufwerk entfehrnen</button><button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button></div></div></div></div>';
    $(text).modal();
}

function eject_drive(id) {
    getJsonData({'function': "eject_drive_" + id});
}

function handle_drives(data) {
    var elem = document.getElementById('drive_info');
    elem.innerHTML = data.info;
}

function handle_volume_messages(data) {
    var elem = document.getElementById('drive_messages');
    elem.innerHTML = data.info;
}

get_drives();
get_volume_messages();
setInterval(get_drives, 3000);
setInterval(get_volume_messages, 1000);