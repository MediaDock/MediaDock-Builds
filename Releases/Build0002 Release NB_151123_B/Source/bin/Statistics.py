# coding=utf-8

__author__ = 'Peter'

from datetime import datetime
import os
import codecs
import json
import re

import MDConst

EPOCH_TIME = datetime.utcfromtimestamp(0)

def getLatestValue(time_dict):
    if not time_dict:
        return None
    latest = datetime(year=1900, day=1, month=1)
    for i in time_dict.keys():
        if i > latest:
            latest = i
    return time_dict[latest], latest

def datetime_to_str(time):
    time_str = str(time)
    if len(time_str) <= 19:
        time_str += '.000000'
    return time_str

def str_to_datetime(str):
    return datetime.strptime(str[:26], '%Y-%m-%d %H:%M:%S.%f')


class FreqAnalysis():
    def __init__(self):
        self.freq = dict()

    def addItem(self, item):
        if self.freq.has_key(item):
            self.freq[item] += 1
        else:
            self.freq[item] = 1

    def getSorted(self):
        return sorted(self.freq.items(), key=lambda x:x[1], reverse=True)

class Statistics():
    def __init__(self, master):
        self.master = master
        self.l = master.l
        self.mh = master.mh
        self.db_version = 0
        self.not_saved_copy_files = []
        self.not_saved_copy_records = []
        self.not_saved_copy_collectios = []

    def copyFile(self, file_path):
        file_path = file_path.replace('\\', '/')
        root_dir = self.master.fh.rootDir.replace('\\', '/')
        if file_path.lower().startswith(root_dir.lower()):
            file_path = u'Media' + file_path[len(root_dir):]
        now = datetime.now()
        raw_data = {'time': now, 'path': file_path}
        try:
            self.writeCopyFile(raw_data)
        except:
            self.not_saved_copy_files.append(raw_data)

    def writeCopyFile(self, data):
        file_name = 'files%i_%02i.db' % (data['time'].year, data['time'].month)
        file_path = os.path.join(MDConst.STATPATH, file_name)
        if not os.path.isfile(file_path):
            content = {'version': self.db_version, 'data':{datetime_to_str(data['time']): data['path']}}
        else:
            db_file = codecs.open(file_path, 'r', encoding='utf-8')
            content = json.load(db_file, encoding='utf-8')
            db_file.close()
            key = datetime_to_str(data['time'])
            key_2 = key
            i = 0
            while content['data'].has_key(key_2):
                key_2 = '%s+%i' % (key, i)
                i += 1
            content['data'][key] = data['path']
        db_file = codecs.open(file_path, 'w', encoding='utf-8')
        json.dump(content, db_file)
        db_file.close()

    def copyRecord(self, file_path):
        file_path = file_path.replace('\\', '/')
        root_dir = self.master.fh.rootDir.replace('\\', '/')
        if file_path.lower().startswith(root_dir.lower()):
            file_path = u'Media' + file_path[len(root_dir):]
        now = datetime.now()
        raw_data = {'time': now, 'path': file_path}
        try:
            self.writeCopyRecord(raw_data)
        except:
            self.not_saved_copy_files.append(raw_data)

    def writeCopyRecord(self, data):
        file_name = 'records%i_%02i.db' % (data['time'].year, data['time'].month)
        file_path = os.path.join(MDConst.STATPATH, file_name)
        if not os.path.isfile(file_path):
            content = {'version': self.db_version, 'data':{datetime_to_str(data['time']): data['path']}}
        else:
            db_file = codecs.open(file_path, 'r', encoding='utf-8')
            content = json.load(db_file, encoding='utf-8')
            db_file.close()
            key = datetime_to_str(data['time'])
            key_2 = key
            i = 0
            while content['data'].has_key(key_2):
                key_2 = '%s+%i' % (key, i)
                i += 1
            content['data'][key] = data['path']
        db_file = codecs.open(file_path, 'w', encoding='utf-8')
        json.dump(content, db_file)
        db_file.close()

    def getCopyedFiles(self):
        return_dict = {}
        for file_name in os.listdir(MDConst.STATPATH):
            if re.match(r'files\d\d\d\d_\d\d.db', file_name):
                file_path = os.path.join(MDConst.STATPATH, file_name)
                try:
                    db_file = codecs.open(file_path, 'r', encoding='utf-8')
                    content = json.load(db_file, encoding='utf-8')
                    db_file.close()
                except:
                    self.mh.addMessage(u'Auf die Datenbank %s konnte nicht zugegriffen werden.' % file_name,
                                       level=MDConst.MESSAGE_WARNING)
                else:
                    for key, value in content['data'].iteritems():
                        return_dict[key] = value
        return return_dict

    def getCopyedRecords(self):
        return_dict = {}
        for file_name in os.listdir(MDConst.STATPATH):
            if re.match(r'records\d\d\d\d_\d\d.db', file_name):
                file_path = os.path.join(MDConst.STATPATH, file_name)
                try:
                    db_file = codecs.open(file_path, 'r', encoding='utf-8')
                    content = json.load(db_file, encoding='utf-8')
                    db_file.close()
                except:
                    self.mh.addMessage(u'Auf die Datenbank %s konnte nicht zugegriffen werden.' % file_name,
                                       level=MDConst.MESSAGE_WARNING)
                else:
                    for key, value in content['data'].iteritems():
                        return_dict[key] = value
        return return_dict

    def getStatCopyRecords(self):
        records = self.getCopyedRecords()
        f = FreqAnalysis()
        for i in records.values():
            f.addItem(i)
        return f.getSorted()

    def getStatCopyRecordsByDay(self):
        copied_records = self.getCopyedRecords()
        return_dict = dict()
        for key, value in copied_records.iteritems():
            temp_date = str_to_datetime(key)
            year = temp_date.year
            month = temp_date.month
            day = temp_date.day
            date = datetime(year, month, day, hour=12)
            delta = date - EPOCH_TIME
            new_time = delta.total_seconds()
            if return_dict.has_key(new_time):
                if return_dict[new_time].has_key(value):
                    return_dict[new_time][value] = return_dict[new_time][value] + 1
                else:
                    return_dict[new_time][value] = 1
            else:
                return_dict[new_time] = {value: 1}
        return return_dict

    def getStatCopyFiles(self):
        files = self.getCopyedFiles()
        f = FreqAnalysis()
        for i in files.values():
            f.addItem(i)
        return f.getSorted()

    def flush(self):
        temp_files = tuple(self.not_saved_copy_files)
        self.not_saved_copy_files = []
        temp_records = tuple(self.not_saved_copy_records)
        self.not_saved_copy_records = []
        temp_collections = tuple(self.not_saved_copy_collectios)
        self.not_saved_copy_collectios = []
        for data in temp_files:
            try:
                self.writeCopyFile(data)
            except:
                self.not_saved_copy_files.append(data)
        for data in temp_records:
            try:
                self.writeCopyRecord(data)
            except:
                self.not_saved_copy_records.append(data)

    def deleteStat(self):
        errors_list = []
        for file_name in os.listdir(MDConst.STATPATH):
            if re.match(r'(files|records|collections)\d\d\d\d_\d\d.db', file_name):
                try:
                    os.remove(os.path.join(MDConst.STATPATH, file_name))
                except:
                    errors_list.append(file_name)
        if errors_list:
            self.mh.addMessage('Statistikdatei(en) konnte(n) nicht gelöscht werden: %s' ', '.join(errors_list),
                               level=MDConst.MESSAGE_ERROR, requested=False, ip='127.0.0.1')