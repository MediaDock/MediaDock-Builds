# coding=utf-8

__author__ = 'Peter'

from ZODB import FileStorage, DB
import transaction
from datetime import datetime
import os
from persistent import Persistent
from persistent.mapping import PersistentMapping

import MDConst

ACTION_INIT = -1
ACTION_DRIVE_CONNECT = 0
ACTION_DRIVE_REMOVE = 1
ACTION_DRIVE_REMOVE_UNEXPECTED = 2

EPOCH_TIME = datetime.utcfromtimestamp(0)

def getLatestValue(time_dict):
    if not time_dict:
        return None
    latest = datetime(year=1900, day=1, month=1)
    for i in time_dict.keys():
        if i > latest:
            latest = i
    return time_dict[latest], latest

class FreqAnalysis():
    def __init__(self):
        self.freq = dict()

    def addItem(self, item):
        if self.freq.has_key(item):
            self.freq[item] += 1
        else:
            self.freq[item] = 1

    def getSorted(self):
        return sorted(self.freq.items(), key=lambda x:x[1], reverse=True)

class Statistics():
    def __init__(self, master):
        self.master = master
        db_path = os.path.join(MDConst.LOCALAPPDATAPATH, u'/stat/stat.db')
        db_dir = os.path.dirname(db_path)
        if not os.path.isdir(db_dir):
            try:
                os.makedirs(db_dir)
            except:
                pass
        self.db  = StatisticsDB(self, db_path)

    def driveAction(self, drive, action):
        self.db.driveAction(drive, action)

    def copyFile(self, file_path, drive):
        file_path = file_path.replace('\\', '/')
        root_dir = self.master.fh.rootDir.replace('\\', '/')
        if file_path.lower().startswith(root_dir.lower()):
            file_path = u'Media' + file_path[len(root_dir):]
        self.db.copyFile(file_path, drive)

    def copyRecord(self, record_path, drive):
        record_path = record_path.replace('\\', '/')
        root_dir = self.master.fh.rootDir.replace('\\', '/')
        if record_path.lower().startswith(root_dir.lower()):
            record_path = u'Media' + record_path[len(root_dir):]
        self.db.copyRecord(record_path, drive)

    def getStatCopyRecords(self):
        records = self.db.getCopyRecords()
        f = FreqAnalysis()
        for i in records.values():
            f.addItem(i)
        return f.getSorted()

    def getStatCopyRecordsByDay(self):
        copied_records = self.db.getCopyRecords()
        return_dict = dict()
        for key, value in copied_records.iteritems():
            year = key.year
            month = key.month
            day = key.day
            date = datetime(year, month, day, hour=12)
            delta = date - EPOCH_TIME
            new_time = delta.total_seconds()
            if return_dict.has_key(new_time):
                if return_dict[new_time].has_key(value):
                    return_dict[new_time][value] = return_dict[new_time][value] + 1
                else:
                    return_dict[new_time][value] = 1
            else:
                return_dict[new_time] = {value: 1}
        return return_dict

    def getStatCopyFiles(self):
        files = self.db.getCopyFiles()
        f = FreqAnalysis()
        for i in files.values():
            f.addItem(i)
        return f.getSorted()

    def getStatDrives(self):
        drives = self.db.getDrives()
        drives_list = list()
        for i in drives:
            temp_drive = dict()
            temp_drive['serial_number'] = i.serial_number
            temp_drive['first_date'] = i.first_date
            temp_drive['name'] = getLatestValue(i.name)[0]
            temp_drive['file_system'] = getLatestValue(i.file_system)[0]
            temp_drive['total_space'] = getLatestValue(i.total_space)[0]

            remo = 0
            remo_dict = dict()
            remo_un = 0
            remo_un_dict = dict()
            conn = 0
            conn_dict = dict()
            for k, l in i.action.items():
                if l == ACTION_DRIVE_CONNECT:
                    conn += 1
                    conn_dict[k] = l
                elif l == ACTION_DRIVE_REMOVE:
                    remo += 1
                    remo_dict[k] = l
                elif l == ACTION_DRIVE_REMOVE_UNEXPECTED:
                    remo_un += 1
                    remo_un_dict[k] = l
            temp_drive['removes'] = remo
            temp_drive['unexpected_removes'] = remo_un
            temp_drive['connections'] = conn
            if remo > 0:
                temp_drive['latest_remove'] = getLatestValue(remo_dict)[1]
            else:
                temp_drive['latest_remove'] = None
            if remo_un > 0:
                temp_drive['latest_unexpected_remove'] = getLatestValue(remo_un_dict)[1]
            else:
                temp_drive['latest_unexpected_remove'] = None
            if conn > 0:
                temp_drive['latest_connection'] = getLatestValue(conn_dict)[1]
            else:
                temp_drive['latest_connection'] = None
            temp_drive['number_copied_records'] = len(i.copy_records)
            if len(i.copy_records):
                temp = getLatestValue(i.copy_records)
                temp_drive['latest_copied_record'] = temp[0]
                temp_drive['date_latest_copied_record'] = temp[1]
            else:
                temp_drive['latest_copied_record'] = u''
                temp_drive['date_latest_copied_record'] = None
            temp_drive['number_copied_files'] = len(i.copy_files)
            if len(i.copy_files):
                temp = getLatestValue(i.copy_files)
                temp_drive['latest_copied_file'] = temp[0]
                temp_drive['date_latest_copied_file'] = temp[1]
            else:
                temp_drive['latest_copied_file'] = u''
                temp_drive['date_latest_copied_file'] = None

            drives_list.append(temp_drive)
        return drives_list

class StatisticsDB:
    def __init__(self, master, file_path):
        self.master = master
        self.file_path = file_path
        if not os.path.isfile(file_path):
            self.createDB()
        self.pack()

    def pack(self):
        storage = FileStorage.FileStorage(self.file_path)
        db = DB(storage)
        db.pack()
        transaction.commit()
        db.close()
        storage.close()

    def createDB(self):
        storage = FileStorage.FileStorage(self.file_path)
        db = DB(storage)
        connection = db.open()
        root = connection.root()

        now = datetime.now()
        root['firstDate'] = now
        root['latestDate'] = now
        root['drives'] = PersistentMapping()
        root['copy'] = PersistentMapping()
        root['copy']['records'] = PersistentMapping()
        root['copy']['files'] = PersistentMapping()

        transaction.commit()
        connection.close()
        db.close()
        storage.close()

    def driveAction(self, drive, action): # todo überarbeiten
        storage = FileStorage.FileStorage(self.file_path)
        db = DB(storage)
        connection = db.open()
        root = connection.root()

        now = datetime.now()
        root['latestDate'] = now
        drives = root['drives']
        if drives.has_key(drive.serial_number):
            drives[drive.serial_number].update(drive, action)
        else:
            drives[drive.serial_number] = StatDrive(drive, action)

        transaction.commit()
        connection.close()
        db.close()
        storage.close()

    def copyFile(self, file_path, drive):
        storage = FileStorage.FileStorage(self.file_path)
        db = DB(storage)
        connection = db.open()
        root = connection.root()

        now = datetime.now()
        root['latestDate'] = now
        drives = root['drives']
        if drives.has_key(drive.serial_number):
            drives[drive.serial_number].update(drive)
        else:
            drives[drive.serial_number] = StatDrive(drive, ACTION_INIT)

        drives[drive.serial_number].copy_files[now] = file_path
        root['copy']['files'][now] = file_path

        transaction.commit()
        connection.close()
        db.close()
        storage.close()

    def copyRecord(self, record_path, drive):
        storage = FileStorage.FileStorage(self.file_path)
        db = DB(storage)
        connection = db.open()
        root = connection.root()

        now = datetime.now()
        root['latestDate'] = now
        drives = root['drives']
        if drives.has_key(drive.serial_number):
            drives[drive.serial_number].update(drive)
        else:
            drives[drive.serial_number] = StatDrive(drive, ACTION_INIT)

        drives[drive.serial_number].copy_records[now] = record_path
        root['copy']['records'][now] = record_path

        transaction.commit()
        connection.close()
        db.close()
        storage.close()


    def getCopyRecords(self):
        storage = FileStorage.FileStorage(self.file_path)
        db = DB(storage)
        connection = db.open()
        root = connection.root()
        value = dict(root['copy']['records'])
        connection.close()
        db.close()
        storage.close()
        return value

    def getCopyFiles(self):
        storage = FileStorage.FileStorage(self.file_path)
        db = DB(storage)
        connection = db.open()
        root = connection.root()
        value = dict(root['copy']['files'])
        connection.close()
        db.close()
        storage.close()
        return value

    def getDrives(self):
        storage = FileStorage.FileStorage(self.file_path)
        db = DB(storage)
        connection = db.open()
        root = connection.root()
        value = list()
        for i in root['drives'].values():
            value.append(StatDriveNp(i))
        connection.close()
        db.close()
        storage.close()
        return value


class StatDrive(Persistent):
    def __init__(self, drive, action):
        now = datetime.now()
        self.serial_number = drive.serial_number
        self.first_date = now
        self.name = PersistentMapping({now: drive.name})
        self.file_system = PersistentMapping({now: drive.file_system})
        self.total_space = PersistentMapping({now: drive.totalSpace})
        self.copy_records = PersistentMapping()
        self.copy_files = PersistentMapping()
        self.action = PersistentMapping({now: action})

    def update(self, drive, action=None):
        now = datetime.now()
        latest_name = getLatestValue(self.name)
        if not latest_name == drive.name:
            self.name[now] = drive.name
        latest_file_system = getLatestValue(self.file_system)
        if not latest_file_system == drive.file_system:
            self.file_system[now] = drive.file_system
        latest_total_space = getLatestValue(self.total_space)
        if not latest_total_space == drive.totalSpace:
            self.total_space[now] = drive.totalSpace
        if not action is None:
            self.action[now] = action

    def copyFile(self, file_path):
        self.copy_files[datetime.now()] = file_path

    def copyRecord(self, record_path):
        self.copy_records[datetime.now()] = record_path

class StatDriveNp():
    def __init__(self, drive):
        self.serial_number = drive.serial_number
        self.first_date = drive.first_date
        self.name = dict(drive.name)
        self.file_system = dict(drive.file_system)
        self.total_space = dict(drive.total_space)
        self.copy_records = dict(drive.copy_records)
        self.copy_files = dict(drive.copy_files)
        self.action = dict(drive.action)


