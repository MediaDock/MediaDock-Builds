var d = new Date();

var monthTable = {  0: 'Januar',
                    1: 'Februar',
                    2: 'März',
                    3: 'April',
                    4: 'Mai',
                    5: 'Juni',
                    6: 'Juli',
                    7: 'August',
                    8: 'September',
                    9: 'Oktober',
                    10: 'November',
                    11: 'Dezember'};

function Calendar(date){
    this.date = date;
    this.month = date.getMonth();
    this.year = date.getFullYear();
    this.elem_month = document.getElementById('label-month'); // todo das geht hier nicht (
    this.elem_year = document.getElementById('label-year')
}

Calendar.prototype.yearPlus = function(){
    this.year++;
    if (this.year > this.date.getFullYear()){
        this.year = this.date.getFullYear();
    }
    this.show();
};

Calendar.prototype.yearMinus = function(){
    this.year--;
    if (this.year < 2000){
        this.year = 2000;
    }
    this.show();
};

Calendar.prototype.show = function(){
    document.getElementById('label-month').innerHTML = monthTable[this.month];
    document.getElementById('label-year').innerHTML = this.year;
    this.showDays(this.month, this.year);
};

Calendar.prototype.monthPlus = function(){
    if (this.month >= this.date.getMonth() && this.year >= this.date.getFullYear()){
        return;
    }
    this.month++;
    if (this.month >= 12){
        this.month = 0;
        this.yearPlus();
    }
    this.show();
};

Calendar.prototype.monthMinus = function(){
    this.month--;
    if (this.month <= -1){
        this.month = 11;
        this.yearMinus();
    }
    this.show();
};

Calendar.prototype.setToday = function(){
    this.month = this.date.getMonth();
    this.year = this.date.getFullYear();
    this.show();
};

Calendar.prototype.calcMonthLength = function(month, year){
    var length;
    switch (month){
        case 0:
        case 2:
        case 4:
        case 6:
        case 7:
        case 9:
        case 11:
            length = 31;
            break;
        case 1:
            if (year % 4 != 0)
                length = 28;
            else if ((year % 100 == 0)&& (year % 400 != 0))
                length = 28;
            else
                length = 29;
            break;
        case 3:
        case 5:
        case 8:
        case 10:
            length = 30;
            break;
    }
    return length;
};

Calendar.prototype.showDays = function(month, year){
    var prev_year = year;
    var prev_month = month - 1;
    if (prev_month < 0){
        prev_month = 11;
        prev_year--;
    }
    else if (prev_month > 11){
        prev_month = 0;
    }

    var next_year = year;
    var next_month = month + 1;
    if (next_month < 0){
        next_month = 11;
    }
    else if (next_month > 11){
        next_month = 0;
        next_year++ ;
    }

    var len_prev_month = this.calcMonthLength(prev_month, prev_year);
    var len_this_month = this.calcMonthLength(month, year);
    var len_next_month = this.calcMonthLength(next_month, next_year);

    var firstDay = new Date();
    firstDay.setDate(1);
    firstDay.setMonth(month);
    firstDay.setFullYear(year);

    var day_transl = {0:-5, 1:-6, 2:0, 3:-1, 4:-2, 5:-3, 6:-4};

    var day_counter = day_transl[firstDay.getDay()];

    var day_elem_id = "day_0_0";
    var day_elem = document.getElementById(day_elem_id);
    var container_elem_id = "record-container-0-0";
    var container_elem = document.getElementById(container_elem_id);
    var date = '1.01.2000';
    var month_str = '01';

    for (var i = 0; i < 6; i++){
        for (var j = 0; j < 7; j++){
            day_elem_id = 'day_' + i + '_' + j;
            day_elem = document.getElementById(day_elem_id);
            container_elem_id = "record-container-" + i + '-' + j;
            container_elem = document.getElementById(container_elem_id);

            if (day_counter <= 0){
                day_elem.innerHTML = day_counter + len_prev_month;
                month_str = (prev_month + 1).toString();
                month_str = prev_month < 9 ? '0' + month_str : month_str;
                date = day_counter + len_prev_month + '.' + month_str + '.' + prev_year;
                container_elem.innerHTML = '<div id="records-day-' + date + '"></div>';
                if (day_counter + len_prev_month == cal.date.getDate() && prev_month == cal.date.getMonth() && prev_year == cal.date.getFullYear()){
                    day_elem.className  = 'day-number-today-other';
                    container_elem.parentNode.className = 'calendar-container-today-other';

                }
                else{
                    day_elem.className = 'day-number-other';
                    container_elem.parentNode.className = 'calendar-container-other';
                }
                //rh.getRecord(date);
                rh.insertRecordFromAllRecords(date);
            }
            else if(day_counter > len_this_month){
                day_elem.innerHTML = day_counter - len_this_month;
                month_str = (next_month + 1).toString();
                month_str = next_month < 9 ? '0' + month_str : month_str;
                date = day_counter - len_this_month + '.' + month_str + '.' + next_year;
                container_elem.innerHTML = '<div id="records-day-' + date + '"></div>';
                //alert(next_month)
                //alert(next_year)
                //alert(cal.date.getMonth())
                if (day_counter - len_this_month == cal.date.getDate() && next_month == cal.date.getMonth() && next_year == cal.date.getFullYear()){
                    day_elem.className  = 'day-number-today-other';
                    container_elem.parentNode.className = 'calendar-container-today-other';

                }
                else{
                    day_elem.className = 'day-number-other';
                    container_elem.parentNode.className = 'calendar-container-other';
                }
                //rh.getRecord(date);
                rh.insertRecordFromAllRecords(date);
            }
            else{
                day_elem.innerHTML = day_counter;
                month_str = (month + 1).toString();
                month_str = month < 9 ? '0' + month_str : month_str;
                date = day_counter + '.' + month_str + '.' + year;
                container_elem.innerHTML = '<div id="records-day-' + date + '"></div>';
                //rh.getRecord(date);
                if (day_counter == cal.date.getDate() && month == cal.date.getMonth() && year == cal.date.getFullYear()){
                    day_elem.className = 'day-number-today';
                    container_elem.parentNode.className = 'calendar-container-today';

                }
                else{
                    day_elem.className = 'day-number';
                    container_elem.parentNode.className = 'calendar-container';

                }

                rh.insertRecordFromAllRecords(date);
            }

            day_counter++ ;
        }
    }
    srl.showList();
};

function RecordHandler (){
    this.records = {};
    this.churches = []
}

RecordHandler.prototype.getRecord = function(date){
    if (! (date in rh.records)){
        rh.requestServer(date);
    }
    rh.insertRecord(date);
};

RecordHandler.prototype.insertRecordFromAllRecords = function(date){
    try{
        var elem = document.getElementById('records-day-' + date);
        var html = '';
        var church = document.getElementById('church_select').value;
        if (! (date in rh.all_records)){
            return;
        }
        else{
            for (var i = 0; i < rh.all_records[date].length; i++){
                if(church == 'undefined') {
                    html += '<div class="record-label" id="record-id-' + rh.all_records[date][i]['id'] +
                        '" onclick="srl.addRecord(' + rh.all_records[date][i]['id'] + ')">' +
                        rh.all_records[date][i]['name'] + '</div>';
                }
                else if(rh.all_records[date][i]['church'] == church){
                    html += '<div class="record-label" id="record-id-' + rh.all_records[date][i]['id'] +
                        '" onclick="srl.addRecord(' + rh.all_records[date][i]['id'] + ')">' +
                        rh.all_records[date][i]['name'] + '</div>';
                }
            }
        }

        elem.innerHTML = html;
    }
    catch (e){
    }
};

RecordHandler.prototype.insertRecord = function(date){
    try{
        var elem = document.getElementById('records-day-' + date);
        var html = '';
        if (! (date in rh.records)){
            return;
        }
        else if (rh.records[date].length == 0){
            html = '<br>';
        }
        else {
            for (var i = 0; i < rh.records[date].length; i++){
                html += '<div class="record-label" id="record-id-' + rh.records[date][i]['id'] +
                        '" onclick="srl.addRecord(' + rh.records[date][i]['id'] + ')">' +
                        rh.records[date][i]['name'] + '</div>';
            }
        }
        elem.innerHTML = html;
    }
    catch (e){
    }
};

RecordHandler.prototype.insertWait = function(date){
    try{
        var elem = document.getElementById('records-day-' + date);
        elem.innerHTML = '<img src="images/wait-small.gif" alt="Bitte warten..."/>';
        //alert('wait');
    }
    catch (e){
    }
};

RecordHandler.prototype.requestServer = function(date){
    //rh.records[date] = [];
    rh.insertWait(date);
    data = getJsonData({'function': 'getRecord', 'date': date}, true);
    rh.records[date] = data.info;
    rh.insertRecord(date);
};

function SelectedRecordsList (){
    this.list_elem = document.getElementById('records-in-list');
    this.records = [];
}

RecordHandler.prototype.requestAllRecords = function(){
    data = getJsonData({'function': 'getAllRecords'}, true);
    rh.all_records = data.info;
    data = getJsonData({'function': 'getChurches'}, true);
    rh.churches = data.info;
};

RecordHandler.prototype.showChurches = function(){
    elem = document.getElementById('church_select');
    church = document.getElementById('church_select').value;
    html = '<option value="undefined">Alle</option>';
    for (var i in this.churches){
        name = '';
        if(this.churches[i] == ''){
            name = '<i>Keine Gemeinde</i>'
        }
        else{
            name = this.churches[i]
        }
        html += '<option value="' + this.churches[i] + '">' + name + '</option>'
    }
    elem.innerHTML = html;
    document.getElementById('church_select').value = church
};

SelectedRecordsList.prototype.showList = function(){
    var html = '';
    html = '<table  class="table table-condensed"><thead><tr><th>Datum</th><th>Veranstaltung</th></tr></thead><tbody>';
    var space = 0;
    for (var i in rh.all_records){
        for (var j in rh.all_records[i]){
            var index = this.records.indexOf(rh.all_records[i][j].id);
            if (! (index == -1)){
                //html += '<div class="record-in-list" onclick="srl.addRecord(' + rh.records[i][j].id + ')">' + i + ' ' +
                //        rh.records[i][j].name + '</div>'
                //html += '<tr onclick="srl.addRecord(' + rh.all_records[i][j].id + ')"><td>' + i +'</td><td>' + rh.all_records[i][j].name + '</td></tr>'
                html += '<tr><td>' + i +'</td><td>' + rh.all_records[i][j].name + '</td></tr>';
                space += rh.all_records[i][j].size;
                var elem = '';
                try{
                    elem = document.getElementById('record-id-' + rh.all_records[i][j].id);
                    elem.className = 'record-label-selected';
                }
                catch(e){
                }
            }
            else{
                try{
                    elem = document.getElementById('record-id-' + rh.all_records[i][j].id);
                    elem.className = 'record-label';
                }
                catch(e){
                }
            }
        }
    }
    html += '</tbody></table>';
    if (html == ''){
        html = '<div class="record-in-list">Keine Aufnahme ausgewählt</div>';
    }
    this.list_elem.innerHTML = html;
    document.getElementById('record-list-space').innerHTML = 'Gesamtgröße: ' +  getSpaceStr(space);
};

SelectedRecordsList.prototype.addRecord = function(id){
    var index = this.records.indexOf(id);
    if (index == -1){
        this.records.push(id);
    }
    else{
        this.records.splice(index, 1);
    }
    this.showList();
};

SelectedRecordsList.prototype.deleteList = function(){
    this.records = [];
    this.showList();
};

function getSpaceStr(space) {
    var spaceDict = {0: 'B', 1: 'kB', 2: 'MB', 3: 'GB', 4: 'TB'};
    var i = 0;
    while (space > 1024){
        space /= 1024.0;
        if (i == 4)
            break;
        i++;
    }
    return space.toFixed(2).toString() + spaceDict[i]
}

function getCompounds(){
    data = getJsonData({'function': 'getCompoundData'}, true);
    insertCompounds(data.info);
}

function insertCompounds(data){
    var compoundElem = document.getElementById('compound-elements');
    var html = '';
    for (var i in data){
        html += '<div class="compound-element">' + data[i].name +
                '<br><div class="progress"><div class="progress-bar" role="progressbar" style="width:' + data[i].ussage
                + ';"><div style="color: black;">Speicherplatz</div></div></div>' + data[i].freeSpace + ' frei von ' + data[i].totalSpace + '<br>';
        switch(data[i].state){
            case -1:
                html += 'Schwerwiegender Fehler';
                break;
            case 0:
                html += '<div class="btn-group-vertical" style="">  <button type="button" class="btn btn-default" onclick="eject_drive('
                        + data[i].id + ')">  <span class="glyphicon glyphicon-eject">  </span>  Auswerfen  </button>  <button type="button" class="btn btn-default" onclick="deleteDriveModal('
                        + data[i].id + ', ' + "'" + data[i].name + "'" + ')">  <span class="glyphicon glyphicon-trash">  </span>  Löschen  </button>  <button type="button" class="btn btn-default" onclick="startRecordJob('
                        + data[i].id  + ')">  <span class="glyphicon glyphicon-download">  </span>  Kopieren starten  </button>  </div>';
                break;
            case 1:
                html += '<div class="progress"> <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" style="width: '
                        + data[i].progess_percent + '%;min-width: 2em;"> '
                        + data[i].progess_percent + '% </div>  </div>  Datei '
                        + (parseInt(data[i].progress) + 1) + ' von '
                        + data[i].totalFiles + ' <br>  Vorraussichtliche Restdauer: '
                        + data[i].left_time + ' <br> <button type="button" class="btn btn-default" onclick="cancelRecordJob(' +
                        + data[i].id + ')">  <span class="glyphicon glyphicon-remove"></span>  Abbrechen  </button>';
                break;
            case 2:
                break;
            case 3:
                html += 'Laufwerk wird gelöscht <br>  <button type="button" class="btn btn-default" onclick="stopDeleteDrive(' +
                        + data[i].id + ')">  <span class="glyphicon glyphicon-remove"></span>  Abbrechen  </button>';
                break;
        }
        html += '</div>';
    }
    compoundElem.innerHTML = html;
}

function eject_drive(id) {
    getJsonData({'function': "eject_drive_" + id});
}

function startRecordJob(drive_id){
    var records = '';
    for (var i in srl.records){
        records += srl.records[i] + ' ';
    }
    data = getJsonData( {'function': 'startRecordJob', 'drive_id': drive_id, 'records': srl.records}, true);
    if (data.check != 'False'){
        srl.deleteList();
    }
}

function cancelRecordJob(drive_id){
    data = getJsonData({'function': 'cancelRecordJob', 'drive_id': drive_id});
}

function deleteDriveModal(drive_id, drive_name){
    var text = '<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><h4 class="modal-title" id="myModalLabel">Warnung</h4></div><div class="modal-body">Möchten sie das Laufwerk '
               + drive_name + ' wirklich löschen? Die Daten können unwiederbringlich verloren gehen. </div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal" onclick="deleteDrive('
               + drive_id + ')">Löschen</button><button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button></div></div></div></div>';
    $(text).modal();
}

function deleteDrive(drive_id){
    data = getJsonData( {'function': 'deleteDrive', 'drive_id': drive_id}, true);
    if (! data.check){
        var text = '<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button><h4 class="modal-title" id="myModalLabel">Warnung</h4></div><div class="modal-body">' +
        data.message + '</div><div class="modal-footer"><button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button></div></div></div></div>';
        $(text).modal();
    }
}

function stopDeleteDrive(drive_id){
    data = getJsonData({'function': 'stopDeleteDrive', 'drive_id': drive_id});
}

function updateCalendar(){
    cal.date = new Date();
    rh.requestAllRecords();
    rh.showChurches();
    cal.show();
    srl.showList();
}

var srl = new SelectedRecordsList();
var rh = new RecordHandler();
var cal = new Calendar(d);

getCompounds();
updateCalendar();
setInterval(getCompounds, 1000);
setInterval(updateCalendar, 3000);
