function insertAllCDOrders(){
    insertCDOrders(0);
    insertCDOrders(1);
    insertCDOrders(2);
    insertCDOrders(3);
}

function insertCDOrders(state){
    var data = getJsonData({'function': 'getCDOrders', 'state': state}, true).data;
    var html = '';
    for (var i in data){
        html += '<tr>';
        html += '   <th scope="row">' + data[i].id + '</th>';
        // html += '   <td>' + data[i].record_date + '</td>';
        // html += '   <td>' + data[i].record_name + '</td>';
        html += '   <td>' + data[i].name + '</td>';
        // html += '   <td>' + data[i].church + '</td>';
        // html += '   <td>' + data[i].comment + '</td>';
        // html += '   <td>' + data[i].format + '</td>';
        // html += '   <td>' + data[i].order_datetime + '</td>';
        html += '</tr>';
    }
    $('#tbody_cd_order_state_' + state).html(html);
}
