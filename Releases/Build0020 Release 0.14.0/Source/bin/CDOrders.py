# -*- coding: utf-8 -*-

import MySQLdb
import PySide.QtCore as QtCore
from datetime import datetime

import MDConst

class CDOrderHandler(object):

    def __init__(self, master):
        self.master = master
        self.db_settings = None
        self.settings = None
        self.load_settings()
        MDConst.events.general_load_settings.connect(self.load_settings)
        MDConst.events.db_load_settings.connect(self.load_settings)

    @QtCore.Slot()
    def load_settings(self):
        general_settings = self.master.master.sth.get_general_settings()
        self.settings = {u'activated': general_settings[u'order_cd_activated']}
        self.db_settings = self.master.master.sth.get_db_settings()

    def get_activated(self):
        if not self.settings.get(u'activated', False):
            return False
        return CDOrderHandler.check_db_connection(self.db_settings)

    @classmethod
    def check_db_connection(cls, connection_settings):
        connect_dict = connection_settings
        connect_dict[u'use_unicode'] = True
        try:
            connection = MySQLdb.connect(**connect_dict)
            cursor = connection.cursor()
        except:
            return False
        else:
            cursor.close()
            connection.close()
        return True

    def order(self, record_date, record_name, name, church, cd_format=None, comment=None, count=None):
        connect_dict = self.db_settings
        connect_dict[u'use_unicode'] = True
        connection = MySQLdb.connect(**connect_dict)
        cursor = connection.cursor()
        now = datetime.now()
        query = u"INSERT INTO mediadock.cd_orders (record_date, record_name, name, church, order_datetime, " \
                u"state, format, comment, count) VALUES (%s, %s, %s, %s, %s, 0, %s, %s, %s);"
        cursor.execute(query, [record_date, record_name, name, church, now, cd_format, comment, count])
        cursor.execute(u"SELECT last_insert_id();")
        row_id = cursor.fetchall()[0][0]
        cursor.close()
        connection.commit()
        connection.close()
        return row_id

    def get_orders(self, state):
        connect_dict = self.db_settings
        connect_dict[u'use_unicode'] = True
        data = {}
        formats = self.get_formats()
        connection = MySQLdb.connect(**connect_dict)
        cursor = connection.cursor()
        query = u'SELECT id, record_date, record_name, name, church, format, order_datetime, comment ' \
                u'FROM mediadock.cd_orders WHERE state=%s'
        cursor.execute(query, [state])
        for i in cursor.fetchall():
            _id = i[0]
            _record_date = i[1]
            _record_name = i[2]
            _name = i[3]
            _church = i[4]
            _format = i[5]
            _order_datetime = i[6]
            _comment = i[7]
            data[_id] = {'id': _id, 'record_date': _record_date.strftime('%d.%m.%Y'), 'record_name': _record_name,
                         'name': _name, 'church': _church, 'format': formats.get(_format, 'Unbekannt'),
                         'order_datetime': _order_datetime.strftime('%d.%m.%Y %H:%M'), 'comment': _comment}
        cursor.close()
        connection.close()
        return data

    def get_formats(self):
        connect_dict = self.db_settings
        connect_dict[u'use_unicode'] = True
        formats = {}
        connection = MySQLdb.connect(**connect_dict)
        cursor = connection.cursor()
        query = u'SELECT * FROM mediadock.format'
        cursor.execute(query)
        for i in cursor.fetchall():
            formats[i[0]] = i[1]
        cursor.close()
        connection.close()
        return formats
