# -*- coding: utf-8 -*-

__author__ = 'Peter'

general = {u'zoom': 1.0,
           u'autostart': True,
           u'statistics': True,
           u'default_church': u'Alle',
           u'drives_removable_drives_only': True,
           u'drives_use_exclusion_list': False,
           u'drives_excluded_drives': [],  # example: [u'C:\\', u'sda']
           u'log_everything': False,
           u'order_cd_activated': True
           }

timer = {u'nr_days': [],
         u'sd_time_weekly': {u'0': [u'23:59'], u'1': [u'23:59'], u'2': [u'23:59'], u'3': [u'23:59'],
                             u'4': [u'23:59'], u'5': [u'23:59'], u'6': [u'23:59']},
         u'sd_time_yearly': {},
         u'lc_time_weekly': {},
         u'lc_time_yearly': {},
         u'ul_time_weekly': {},
         u'ul_time_yearly': {},
         u'time_transition_4_7': 5.0,  # time in minutes
         u'time_transition_5_2': 10.0,  # time in minutes
         u'time_transition_6_7': 5.0  # time in minutes
         }

files = {u'rootdir': None,
         u'copy_file_extensions': [u'mp3',
                                   u'wav',
                                   u'jpg',
                                   u'jpeg',
                                   u'png']
         }

db = {u'host': u'localhost',
      u'port': 3306,
      u'user': u'md_user',
      u'passwd': u'immerMediaDock',
      u'charset': u'utf8'
      }
