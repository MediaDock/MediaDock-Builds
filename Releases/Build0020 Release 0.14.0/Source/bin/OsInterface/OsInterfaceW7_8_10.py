# -*- coding: utf-8 -*-

import os
import win32api
import win32file
import win32con
import string

WINDOWS = True
LINUX = False
PYWIN32_ENCODING = 'latin-1' # 'ISO-8859-15'
_GENERIC_READ = 0x80000000
_GENERIC_WRITE = 0x40000000
_FILE_SHARE_READ = 1
_FILE_SHARE_WRITE = 2
_FSCTL_LOCK_VOLUME = 0x90018
_FSCTL_DISMOUNT_VOLUME = 0x90020
_IOCTL_STORAGE_MEDIA_REMOVAL = 0x2D4804
_IOCTL_STORAGE_EJECT_MEDIA = 0x2D4808


def getLocalAppDataPath():
    return os.path.abspath(os.path.join(os.environ[u'APPDATA'], u'MediaSuite', u'MediaDock'))

def getVolumeFreeSpace(device):
    info_space = win32api.GetDiskFreeSpace(device)
    return info_space[0] * info_space[1] * info_space[2]

def getVolumeTotalSpace(letter):
    info_space = win32api.GetDiskFreeSpace(letter)
    return info_space[0] * info_space[1] * info_space[3]

def getVolumeName(device):
    info_volume = win32api.GetVolumeInformation(device)
    if info_volume[0] == '':
        return u'Unbenannter Datenträger'
    return info_volume[0].decode(PYWIN32_ENCODING, errors='replace')

def getVolumeInfo(device):
    info_space = win32api.GetDiskFreeSpace(device)
    totalSpace = info_space[0] * info_space[1] * info_space[3]
    freeSpace = info_space[0] * info_space[1] * info_space[2]
    info_volume = win32api.GetVolumeInformation(device)
    return [device, getVolumeName(device), totalSpace, freeSpace, info_volume[4]]

def getVolumesList(removableDrivesOnly, useExclusionList, excludedDrives): # todo testen
    drives_list1 = win32api.GetLogicalDriveStrings().strip('\x00').split('\x00')
    drives_list2 = drives_list1[:]
    #if self.master.dh.removableDrivesOnly:
    if removableDrivesOnly:
        for i in drives_list1:
            drive_type = win32file.GetDriveType(i)
            if drive_type != win32file.DRIVE_REMOVABLE:
                drives_list2.remove(i)
    for i in drives_list1:
        drive_type = win32file.GetDriveType(i)
        if drive_type == win32file.DRIVE_CDROM and i in drives_list2:
            drives_list2.remove(i)
    #if self.master.dh.useExclusionList:
    if useExclusionList:
        for i in drives_list1:
            #if i in self.master.dh.excludedDrives:
            if i in excludedDrives and i in drives_list2:
                drives_list2.remove(i)
    return drives_list2

def isHiddenPath(dir_name):
    if not os.path.exists(dir_name):
        return False
    temp = win32api.GetFileAttributes(dir_name)
    return bool(temp & (win32con.FILE_ATTRIBUTE_HIDDEN | win32con.FILE_ATTRIBUTE_SYSTEM))

def ejectDrive(device, message_handler=None):
    # """
    result = os.popen(u'RemoveDrive %s' % device, 'r').read()
    if 'success' in result:
        return  True
    return False
    """
    success = True
    path = '\\\\.\\' + device[0] +  ":"
    handle = win32file.CreateFile(path, _GENERIC_READ | _GENERIC_WRITE, _FILE_SHARE_READ | _FILE_SHARE_WRITE, None, 3,
                                  0, None)
    try:
        win32file.DeviceIoControl(handle, _FSCTL_LOCK_VOLUME, "", 0, None)
        win32file.DeviceIoControl(handle, _FSCTL_DISMOUNT_VOLUME, "", 0, None)
        win32file.DeviceIoControl(handle, _IOCTL_STORAGE_MEDIA_REMOVAL, '\x00', 0, None)
        win32file.DeviceIoControl(handle, _IOCTL_STORAGE_EJECT_MEDIA, "", 0, None)
    except Exception as e:
        success = False
    finally:
        win32file.CloseHandle(handle)
    return success
    # """


def isValidDriveIdentifier(identifier):
    if not len(identifier) == 3:
        return False
    if not identifier.endswith(':\\'):
        return False
    if identifier[0] not in string.ascii_uppercase:
        return False
    return True
