# coding=utf-8

import sys
import time
import platform
import os
from datetime import datetime, timedelta
import threading
import copy
import json
import codecs
import subprocess

import Logger
import MDConst
from Settings import SettingsHandler
from Timer import TimeHandler
import OsInterface as Osi

__author__ = u'Peter Klassen'
__copyright__ = u'Copyright +++ replace year +++, Peter Klassen'
__license__ = u'Apache 2'
__date__ = u'+++ replace full date +++'


def about():
    return u'MediaDock Author: %s Copyright: %s License: %s Date: %s' % (__author__, __copyright__, __license__,
                                                                        __date__)

class Watchdog(object):
    def __init__(self, master):
        self.master = master
        self.__run = True
        self.thread = threading.Thread(target=self.update, name=u'WatchdogUpdater')
        self.thread.daemon = True

    def start(self):
        self.thread.start()

    def write_crash_dump(self):
        now = datetime.now()
        file_name = u'engine_crash_dump_%s.cd' % str(now)[:10]
        dump_file = open(MDConst.LOGPATH + '/' + file_name, 'a')
        e = self.master.e
        dump_file.write(unicode(now) + u' -- ' + repr(unicode(e.dh)) + u' -- ' + repr(unicode(e.jh)) + '\n')
        dump_file.close()

    def update(self):
        while self.__run:
            time.sleep(0.1)
            if not self.master.e.getThreadRunning():
                self.master.e.mh.addMessage(u'Watchdog: Restart Engine Thread', level=MDConst.MESSAGE_ERROR)
                try:
                    self.master.i.register_engine_restart()
                    self.write_crash_dump()
                    self.master.e.restart()
                except Exception as exc:
                    self.master.e.mh.addMessage(u'Watchdog: Restart Engine failed ' + unicode(exc),
                                                level=MDConst.MESSAGE_ERROR)

        pass # todo job überwachung -> zukünftiges release
        # todo drivehanler update überwachung (update_drive_pause nach einer bestimmten zeit auf 0 setzen)

class MDSystem:
    pass

    def __init__(self, master):
        self.master = master
        self.__info = {u'System Start': datetime.now(), u'Engine Restart': 0}
        # get system information
        # python version
        # os version (kernel version ...)
        # pid
        # abort if mediadock is already running

    def write_info(self, key, info):
        self.__info[key] = info

    def get_info(self, key):
        if self.__info.has_key(key):
            return copy.deepcopy(self.__info[key])
        return None

    def get_all_infos(self):
        temp = copy.deepcopy(self.__info)
        if temp.has_key(u'System Start'):
            temp[u'Uptime'] = unicode(datetime.now() - temp[u'System Start'])
        temp[u'System Start'] = unicode(temp[u'System Start'])
        '''
        if temp.has_key('NumberOfServerErrors') and temp.has_key('NumberOfServerRequests') \
                and temp['NumberOfServerRequests'] != 0:
            temp['ServerErrorRate'] = '%f%%' % (float(100* temp['NumberOfServerErrors']) /
                                                temp['NumberOfServerRequests'])
        '''
        if temp.has_key(u'NumberOfServerErrors'):
            temp[u'NumberOfServerErrors'] = unicode(temp[u'NumberOfServerErrors'])
        if temp.has_key(u'NumberOfServerRequests'):
            temp[u'NumberOfServerRequests'] = unicode(temp[u'NumberOfServerRequests'])
        return temp

    def write_to_file(self):
        file_name = u'quit-%s-' % unicode(datetime.now())[:10]
        i = 0
        while file_name + unicode(i) + u'.json' in os.listdir(MDConst.LOGPATH):
            i += 1
        file_name += unicode(i) + u'.json'
        try: # todo checken ob das immer geht (sonderzeichen)
            report_file = codecs.open(os.path.join(MDConst.LOGPATH, file_name), 'a', 'utf-8')
            json.dump(self.get_all_infos(), report_file, encoding='utf-8')
            report_file.close()
        except:
            pass

    def register_engine_restart(self):
        self.__info[u'Engine Restart'] += 1

    def startup(self):
        pass
        #
        # check all lib's
        # check firstrun

    def register_start_update(self, element):
        '''
        :param element: element witch starts update
        :return:
        '''
        pass

    def register_stop_update(self, element):
        pass # mintes

class MediaDock(object):
    def __init__(self):
        self.run = True
        self.shutdown = False
        self.th = None
        self.e = None
        self.s = None
        self.w = None

    def checkQt(self):
        try:
            #import PyQt4
            import PySide  # todo test before release for linux
            #import  PySide.QtCore as QtCore
            #import  PySide.QtGui as QtGui
            #import  PySide.QtWebKit as QtWebKit
        except:
            self.l.logMediaDock(u'Could not load library: PySide', logLevel=MDConst.LOG_ERROR)
            import tkMessageBox
            tkMessageBox.showerror(u'Schwerwiegender Feher', 'Die Bibliothek PySide konnte nicht geladen werden.')
            sys.exit(-1)

    def exec_(self):

        if MDConst.FROZEN:
            os.chdir(os.path.abspath(os.path.realpath(os.path.dirname(sys.executable))))

        run = True

        if '-autostart' in sys.argv:
            settings_file_path = os.path.join(MDConst.SETTINGSPATH, 'general.json')
            if os.path.isfile(settings_file_path):
                try:
                    settings_file = codecs.open(settings_file_path, 'r', encoding='utf-8')
                    settings = json.load(settings_file, encoding='utf-8')
                    settings_file.close()
                    if settings.has_key('autostart'):
                        if not settings['autostart']:
                            run = False
                    else:
                        run = False
                except:
                    run = False
            else:
                run = False

        if not run:
            return

        try:
            self.checkDirs()
        except:
            self.start_up_error(u'Fehler', u'Kein Zugriff auf benötigte Ordner.',
                                u'No access to some folders.')

        if MDConst.FROZEN:
            #sys.stdout = codecs.open(MDConst.LOGPATH + '/stdout-%s.log' % str(datetime.now())[:10], 'a', 'utf-8')
            sys.stderr = codecs.open(MDConst.LOGPATH + '/stderr-%s.log' % str(datetime.now())[:10], 'a', 'utf-8')

        self.l = Logger.Logger(self, MDConst.LOGPATH)
        self.mh = MessageHandler(self, self.l)

        self.l.logMediaDock(u'Start MediaDock Version: +++ replace release +++', logLevel=MDConst.LOG_INFO)

        # check PyQt
        self.checkQt()

        import Browser

        # start splash
        self.b = Browser.Browser(self)
        self.b.startSplashScreen()

        self.i = MDSystem(self)
        self.i.write_info(u'Release', u'+++ replace release +++')
        self.i.write_info(u'Build', u'+++ replace build +++')

        #time.sleep(2)

        # check single instance
        self.checkSingleInstance()

        # check environment
        self.checkEnvironment() # todo linux

        self.sth = SettingsHandler(self)
        self.l.load_settings()
        self.l.cleanup()

        # check modules

        # check first run


        import Engine
        #import Server

        self.th = TimeHandler(self)
        self.e = Engine.Engine(self, self.l, self.mh)

        #server_port = self.sth.get_server_port()
        #self.s = Server.Server(self.l, self.e.si, server_port)

        self.w = Watchdog(self)

        self.th.start()
        self.e.start()
        #self.s.start()
        self.w.start()

        import GuiInterface
        self.gi = GuiInterface.GuiInterface(self)

        self.b.exec_()

        self.e.stat.flush()

        self.i.write_to_file()
        self.l.logMediaDock(u'Stop', logLevel=MDConst.LOG_INFO)

        if self.shutdown:
            self.l.logMediaDock(u'Shutdown', logLevel=MDConst.LOG_INFO)
            system = platform.system()
            if Osi.WINDOWS:
                subprocess.call(['shutdown', '/s', '/t', '1'])
            elif Osi.LINUX:
                pass # todo a bash file to shutdown linux

    def checkEnvironment(self):
        # check os
        #if not platform.system() == 'Windows':  # todo testen
        #    self.start_up_error(u'Fehler', u'Fasches Betriebssystem. Verwenden sie Windows 7 oder 8 (bzw. 10).',
        #                        u'Wrong OS %s' % platform.system())

        # check os (windows 7/8)
        if Osi.WINDOWS and not platform.release() in ['7', '8', '8.1', '10']:
            self.start_up_error(u'Fehler', u'Fasches Betriebssystem. Verwenden sie Windows 7, 8, 8.1 oder 10.',
                                u'Wrong OS %s' % platform.version())
        elif Osi.LINUX:
            pass # todo check utils

        # check interpreter
        if not sys.subversion[0] == 'CPython':
            self.start_up_error(u'Fehler', u'Falscher Interpreter. Er sollte CPython sein.',
                                u'Wrong interpreter %s' % sys.subversion[0])

        # check python interpreter 2.7.9
        if not sys.version.startswith('2.7.'): # getestet beim umstellen auf 2.7.10
            self.start_up_error(u'Fehler', u'Falsche Interpeterversion. Sie sollte 2.7 sein',
                                u'Wrong Python version: %s' % sys.version)

        # check 32 bit environment
        #if not ctypes.sizeof(ctypes.c_voidp) == 4:
        #    self.start_up_error(u'Fehler', u'Falsche Interpreterversion. Sie sollte 32-Bit sein.',
        #                        u'Wrong Python version: Not 32-Bit.')

    def start_up_error(self, header, show_message, log_message):
        self.l.logMediaDock(log_message, logLevel=MDConst.LOG_ERROR)
        self.b.stopSplashScreen()
        self.b.messageBox(header, show_message)
        sys.exit(-1)

    def checkDirs(self):
        if not os.path.isdir(MDConst.SETTINGSPATH):
            os.makedirs(MDConst.SETTINGSPATH)
        if not os.path.isdir(MDConst.LOGPATH):
            os.makedirs(MDConst.LOGPATH)
        if not os.path.isdir(MDConst.STATPATH):
            os.makedirs(MDConst.STATPATH)
        #if not os.path.isdir(u'../Media'):
        #    os.makedirs(u'../Media')

    def checkSingleInstance(self):
        lock_file_name = os.path.join(MDConst.LOCALAPPDATAPATH, u'.lock')
        try:
            if Osi.WINDOWS and os.path.isfile(lock_file_name):
                os.remove(lock_file_name)
            self.lock_file = open(lock_file_name, 'w+')
            if Osi.LINUX:
                Osi.fcntl.flock(self.lock_file, Osi.fcntl.LOCK_EX | Osi.fcntl.LOCK_NB)
        except EnvironmentError as e:
            #if e.errno == 13:
            self.start_up_error(u'Fehler', u'Das Programm läuft bereits', u'Instance is already running')
            #else:
            #    print e


class Message():
    def __init__(self, message, id, level, lifespan, section, requested=True, ip=None):
        self.message = message
        self.id = id
        self.level = level
        self.lifespan = lifespan
        self.section = section
        self.requested = requested
        if ip is None:
            self.ip = '127.0.0.1'
        else:
            self.ip = ip
        self.startTime = datetime.now()

    def __unicode__(self):
        return self.message


class MessageHandler():
    maxId = 0
    levelDict = {0: u'Debug', 1: u'Info', 2: u'Warnung', 3: u'Fehler'}

    def __init__(self, master, logger):
        self.master = master
        self.logger = logger
        self.messages = list()
        self.allMessages = list()
        self.default_live_span = 10000

    def remove(self, message_id):
        for i in self.messages:
            if i.id == message_id:
                self.messages.remove(i)
                break

    '''
    sections:
        0 all
        1 volumes
        2 new job
    '''
    def addMessage(self, message, level=0, lifespan=None, section=(0,), requested=True, ip=None):
        if lifespan is None:
            lifespan = self.default_live_span
        self.messages.append(Message(message, self.maxId, level, lifespan, section, requested, ip))
        self.allMessages.append({u'id': self.maxId, u'message': message, u'level': MessageHandler.levelDict[level],
                                 u'level_int': level, u'section': unicode(section).strip('[](),'),
                                 u'time': str(datetime.now()), u'requested': requested})
        requested_str = ''
        if not requested:
            requested_str = u'Modal'
        self.logger.logMessage(u'ID: %i / %sMessage: %s / Section: %s' % (self.maxId, requested_str, message,
                                                                          str(section).strip('[]')),
                               logLevel=level)
        MessageHandler.maxId += 1

    def getSortedMessages(self, section=0, level=0):
        sorted_messages = []
        if level >= 1:
            for i in self.messages[::-1]:
                if section in i.section and i.level == level:
                    sorted_messages.append(i)
        return sorted_messages

    def getAllMessages(self):
        messages = list()
        for i in self.allMessages[::-1]:
            if i[u'level_int'] >= 0: #self.level:
                messages.append(i)
        return messages

    def update(self):
        self.master.i.write_info(u'Total Messages', str(len(self.allMessages)))
        self.master.i.write_info(u'Current Messages', str(len(self.messages)))
        for i in self.messages:
            d = datetime.now() - i.startTime
            if d.total_seconds() * 1000 > i.lifespan and i.requested:
                self.messages.remove(i)

    def getModalMessages(self, ip):
        data = []
        for i in self.messages:
            if i.requested == False and i.ip == ip:
                data.append({u'message': i.message, u'levelName': MessageHandler.levelDict[i.level], u'level': i.level})
                i.requested = True
        return data


if __name__ == '__main__':
    md = MediaDock()
    md.exec_()
    sys.exit()