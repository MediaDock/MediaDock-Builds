function getAllMessages(){
    data = getJsonData({'function': 'getAllMessages'}, true);

    var elem = document.getElementById("messages-list");
    var html = '';
    var elementDebug = document.getElementById('inlineCheckboxDebug');
    var includeDebug = elementDebug.checked;
    var elementInfos = document.getElementById('inlineCheckboxInfos');
    var includeInfos = elementInfos.checked;
    var elementWarnings = document.getElementById('inlineCheckboxWarnings');
    var includeWarnings = elementWarnings.checked;
    var elementErrors = document.getElementById('inlineCheckboxErrors');
    var includeErrors = elementErrors.checked;
    for (var i in data.info){
        switch (data.info[i].level_int){
            case 0:
                if (includeDebug) {
                    html += '<tr><td>' +
                        data.info[i].id + '</td><td>' +
                        data.info[i].message + '</td><td>' +
                        data.info[i].time + '</td><td>' +
                        data.info[i].level + '</td><td>' +
                        data.info[i].section + '</td></tr>';
                }
                break;
            case 1:
                if (includeInfos) {
                    html += '<tr><td>' +
                        data.info[i].id + '</td><td>' +
                        data.info[i].message + '</td><td>' +
                        data.info[i].time + '</td><td>' +
                        data.info[i].level + '</td><td>' +
                        data.info[i].section + '</td></tr>';
                }
                break;
            case 2:
                if (includeWarnings) {
                    html += '<tr><td>' +
                        data.info[i].id + '</td><td>' +
                        data.info[i].message + '</td><td>' +
                        data.info[i].time + '</td><td>' +
                        data.info[i].level + '</td><td>' +
                        data.info[i].section + '</td></tr>';
                }
                break;
            case 3:
                if (includeErrors) {
                    html += '<tr><td>' +
                        data.info[i].id + '</td><td>' +
                        data.info[i].message + '</td><td>' +
                        data.info[i].time + '</td><td>' +
                        data.info[i].level + '</td><td>' +
                        data.info[i].section + '</td></tr>';
                }
                break;
            default:
                html += '<tr><td>' +
                 data.info[i].id + '</td><td>' +
                 data.info[i].message + '</td><td>' +
                 data.info[i].time + '</td><td>' +
                 data.info[i].level + '</td><td>' +
                 data.info[i].section + '</td></tr>';
                break;
        }

    }
    elem.innerHTML = html;
}

getAllMessages();
setInterval(getAllMessages, 3000);