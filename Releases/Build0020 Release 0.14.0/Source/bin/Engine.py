# -*- coding: utf-8 -*-

import calendar
import copy
import datetime
import os
import pickle
import re
import scrypt
import shutil
import threading
import time
import traceback
import urllib
import xml.etree.ElementTree as et
import PySide.QtCore as QtCore

import DefaultSettings
import EncrConst
import MDConst
from Statistics import Statistics
import OsInterface as Osi
from CDOrders import CDOrderHandler


class Engine(object):
    def __init__(self, master, logger, message_handler):
        self.master = master
        self.run = True
        self.l = logger
        self.mh = message_handler
        self.fh = FileHandler(logger, self)
        self.dh = DriveHandler(self)
        self.jh = JobHandler(self)
        self.si = ServerInterface(self)
        self.rh = RecordHandler(self)
        self.sh = SessionHandler(self)
        self.stat = Statistics(self)
        self.coh = CDOrderHandler(self)
        self.update_drive_pause = 0
        self.counter_update_dh = 0
        self.counter_update_jh = 0
        self.counter_update_mh = 0
        self.counter_update_rh = 0
        self.counter_update_sh = 0
        self.counter_update_stat = 0
        self.update_error_dh = False
        self.update_error_mh = False
        self.update_error = False
        self.thread = threading.Thread(target=self.update, args=(), name=u'EngineUpdater')
        self.thread.daemon = True

    def getThreadRunning(self):
        if not self.thread.is_alive() and self.run:
            return False
        return True

    def restart(self):
        if not self.thread.is_alive():
            self.thread = threading.Thread(target=self.update, args=(), name=u'EngineUpdater')
            self.thread.daemon = True
            self.thread.start()

    def update(self):
        self.l.logEngine(u'Start Engine', logLevel=MDConst.LOG_DEBUG)
        i = 0
        while self.run:
            if not self.update_drive_pause and not self.counter_update_dh:
                try:
                    self.dh.update()
                except Exception as e:
                    if not MDConst.FROZEN:
                        print e
                        print traceback.format_exc()
                    self.l.logEngine(u'Update DriveHandler failed: %s' % e, logLevel=MDConst.LOG_ERROR)
                    self.mh.addMessage(u'Update DriveHandler failed %s' % e, level=MDConst.MESSAGE_ERROR)
            if not self.counter_update_jh:
                try:
                    self.jh.update()
                except Exception as e:
                    self.l.logEngine(u'Update JobHandler failed: %s' % e, logLevel=MDConst.LOG_ERROR)
                    self.mh.addMessage(u'Update JobHandler failed: %s' % e, level=MDConst.MESSAGE_ERROR)
            if not self.counter_update_mh:
                try:
                    self.mh.update()
                except Exception as e:
                    self.l.logEngine(u'Update MessageHandler failed: %s' % e, logLevel=MDConst.LOG_ERROR)
                    self.mh.addMessage(u'Update MessageHandler failed: %s' % e, level=MDConst.MESSAGE_ERROR)
            '''
            if not self.counter_update_rh:
                #self.l.logEngine('Update RecordHandler')
                try:
                    self.rh.update()
                except Exception as e:
                    self.l.logEngine(u'Update RecordHandler failed: %s' % e, logLevel=MDConst.LOG_ERROR)
                    self.mh.addMessage(u'Update RecordHandler failed: %s' % e, level=MDConst.MESSAGE_ERROR)
            '''
            if not self.counter_update_sh:
                try:
                    self.sh.update()
                except Exception as e:
                    self.l.logEngine(u'Update SessionHandler failed: %s' % e, logLevel=MDConst.LOG_ERROR)
                    self.mh.addMessage(u'Update SessionHandler failed: %s' % e, level=MDConst.MESSAGE_ERROR)
            if not self.counter_update_stat:
                try:
                    self.stat.flush()
                except Exception as e:
                    self.l.logEngine(u'Flush Statisitcs failed: %s' % e, logLevel=MDConst.LOG_ERROR)
                    self.mh.addMessage(u'Flush Statisitcs failed: %s' % e, level=MDConst.MESSAGE_ERROR)

            self.counter_update_dh += 1
            self.counter_update_dh %= MDConst.UPDATE_TIME_DH
            self.counter_update_jh += 1
            self.counter_update_jh %= MDConst.UPDATE_TIME_JH
            self.counter_update_mh += 1
            self.counter_update_mh %= MDConst.UPDATE_TIME_MH
            self.counter_update_rh += 1
            self.counter_update_rh %= MDConst.UPDATE_TIME_RH
            self.counter_update_sh += 1
            self.counter_update_sh %= MDConst.UPDATE_TIME_SH
            self.counter_update_stat += 1
            self.counter_update_stat %= MDConst.UPDATE_TIME_STAT

            i += 1
            i %= 200

            time.sleep(0.1)

    '''
    def stop(self):
        if self.jh.numberOfRunningJobs():
            self.jh.deleteAllJobs()
        if self.jh.numberOfJobs():
            return False  # todo
        else:
            self.run = False  # todo muss solange updaten, bis jobhandler keine Jobs mehr hat
            return True
    '''

    def start(self):
        # update all
        self.rh.start()
        try:
            self.dh.update()
        except Exception as e:
            self.l.logEngine(u'Update DriveHandler failed: %s' % e, logLevel=MDConst.LOG_ERROR)
            self.mh.addMessage(u'Update DriveHandler failed %s' % e, level=MDConst.MESSAGE_ERROR)
        try:
            self.jh.update()
        except Exception as e:
            self.l.logEngine(u'Update JobHandler failed: %s' % e, logLevel=MDConst.LOG_ERROR)
            self.mh.addMessage(u'Update JobHandler failed: %s' % e, level=MDConst.MESSAGE_ERROR)
        try:
            self.mh.update()
        except Exception as e:
            self.l.logEngine(u'Update MessageHandler failed: %s' % e, logLevel=MDConst.LOG_ERROR)
            self.mh.addMessage(u'Update MessageHandler failed: %s' % e, level=MDConst.MESSAGE_ERROR)
        try:
            self.sh.update()
        except Exception as e:
            self.l.logEngine(u'Update SessionHandler failed: %s' % e, logLevel=MDConst.LOG_ERROR)
            self.mh.addMessage(u'Update SessionHandler failed: %s' % e, level=MDConst.MESSAGE_ERROR)

        self.thread.start()

    def is_stop_ready(self):
        if self.jh.numberOfJobs() > 0:
            return False
        if self.dh.has_delete_tasks():
            return False
        return True


class Drive(object):

    def __init__(self, master, device):
        self.master = master
        self.device = device
        self.id = DriveHandler.maxId
        self.__state = MDConst.DRIVE.IDLE
        self.deleting = False
        self.deleteThread = None
        DriveHandler.maxId += 1
        volume_info = Osi.getVolumeInfo(device)
        self.mountpoint = volume_info[0]
        self.name = volume_info[1]
        self.totalSpace = volume_info[2]
        self.freeSpace = volume_info[3]
        self.leftSpace = volume_info[3]
        self.file_system = volume_info[4]

    def __unicode__(self):
        return self.name

    def __str__(self):
        return self.name.encode('utf-8')

    def get_info_str(self):
        return 'Drive ' \
               'id: %s, ' \
               'mountpoint: %s, ' \
               'device: %s, ' \
               'name: %s, ' \
               'state: %i, ' \
               'total_space: %i, ' \
               'free_space: %i, ' \
               'left_space: %i, ' \
               'file_system: %s' % (
               self.id,
               self.mountpoint,
               self.device,
               str(self),
               self.__state,
               self.totalSpace,
               self.freeSpace,
               self.leftSpace,
               self.file_system
                )

    def get_info_unicode(self):
        return 'Drive ' \
               'id: %s, ' \
               'mountpoint: %s, ' \
               'device: %s, ' \
               'name: %s, ' \
               'state: %i, ' \
               'total_space: %i, ' \
               'free_space: %i, ' \
               'left_space: %i, ' \
               'file_system: %s' % (
               self.id,
               self.mountpoint,
               self.device,
               unicode(self),
               self.__state,
               self.totalSpace,
               self.freeSpace,
               self.leftSpace,
               self.file_system
                )

    @property
    def state(self):
        return self.__state

    def requestChangeState(self, target_state, force=False):
        if force:
            self.__state = target_state
            return True
        elif self.__state == MDConst.DRIVE.IDLE and target_state in (MDConst.DRIVE.IDLE,
                                                                 MDConst.DRIVE.RECORD_JOB_RUNS,
                                                                 MDConst.DRIVE.FILE_JOB_RUNS,
                                                                 MDConst.DRIVE.DELETES,
                                                                 MDConst.DRIVE.EJECTING):
            self.__state = target_state
            return True
        elif self.__state == MDConst.DRIVE.RECORD_JOB_RUNS and target_state in (MDConst.DRIVE.IDLE,
                                                                                MDConst.DRIVE.ERROR):
            self.__state = target_state
            return True
        elif self.__state == MDConst.DRIVE.FILE_JOB_RUNS and target_state in (MDConst.DRIVE.IDLE,
                                                                              MDConst.DRIVE.ERROR):
            self.__state = target_state
            return True
        elif self.__state == MDConst.DRIVE.DELETES and target_state in (MDConst.DRIVE.IDLE,
                                                                        MDConst.DRIVE.ERROR):
            self.__state = target_state
            return True
        elif self.__state == MDConst.DRIVE.EJECTING and target_state in (MDConst.DRIVE.IDLE,):
            self.__state = target_state
            return True
        elif self.__state == MDConst.DRIVE.ERROR and target_state in (MDConst.DRIVE.EJECTING,):
            self.__state = target_state
            return True
        return False

    def eject(self):
        success = Osi.ejectDrive(self.device, self.master.master.mh)
        if success:
            self.master.drives.remove(self)
        return success

    def getUsage(self):
        if self.totalSpace == 0:
            return '0%'
        return '%i%%' % (((self.totalSpace - self.freeSpace) * 100) / self.totalSpace)

    def delete_dir(self, directory, errors):
        try:
            dir_list = os.listdir(directory)
        except:
            self.master.master.mh.addMessage(u'Fehler beim Löschen: Pfad konnte nicht gefunden werden: ' + directory,
                                             level=MDConst.MESSAGE_ERROR) # todo testen
            return 1
        for i in dir_list:
            if not self.deleting:
                break
            path = os.path.join(directory, i)
            if os.path.isdir(path):
                if not Osi.isHiddenPath(path):
                    errors += self.delete_dir(path, errors)
                    try:
                        os.rmdir(path)
                    except Exception as e:
                        # print e
                        # print traceback.format_exc()
                        errors += 1
                        self.master.master.mh.addMessage(u'Fehler beim Löschen des Ordners: ' + path,
                                                         level=MDConst.MESSAGE_ERROR)# todo testen
            if os.path.isfile(path):
                #print 'file'
                try:
                    os.remove(path)
                except:
                    #print traceback.format_exc()
                    errors += 1
                    self.master.master.mh.addMessage(u'Fehler beim Löschen der Datei: ' + path,
                                                     level=3) # todo testen
        return errors

    def delete(self):
        result = self.master.checkDelete(self.id)
        if not result[0]:
            self.deleteThread = None
            return
        if self.requestChangeState(MDConst.DRIVE.DELETES):
            self.deleting = True
            self.master.master.mh.addMessage(u'Löschen gestartet für Laufwerk: ' + unicode(self),
                                             level=MDConst.MESSAGE_INFO)
            errors = self.delete_dir(unicode(self.mountpoint), 0)
            if self.deleting:
                if errors:
                    if errors > 1:
                        self.master.master.mh.addMessage(u'Löschen mit %i Fehlern abgeschlossen.' % errors,
                                                         level=MDConst.MESSAGE_ERROR, requested=False)
                    else:
                        self.master.master.mh.addMessage(u'Löschen mit einem Fehler abgeschlossen.',
                                                         level=MDConst.MESSAGE_ERROR, requested=False)
                else:
                    self.master.master.mh.addMessage(u'Löschen erfogreich abgeschlossen.', level=MDConst.MESSAGE_INFO,
                                                     requested=False)
            else:
                if errors:
                    if errors > 1:
                        self.master.master.mh.addMessage(u'Löschen mit % Fehlern abgebrochen.' % errors,
                                                         level=MDConst.MESSAGE_ERROR, requested=False)
                    else:
                        self.master.master.mh.addMessage(u'Löschen mit einem Fehler abgebrochen.',
                                                         level=MDConst.MESSAGE_ERROR, requested=False)
                else:
                    self.master.master.mh.addMessage(u'Löschen abgebrochen.', level=MDConst.MESSAGE_INFO,
                                                     requested=False)
            if self.requestChangeState(MDConst.DRIVE.IDLE):
                try:
                    self.leftSpace = Osi.getVolumeFreeSpace(self.device)
                    self.freeSpace = Osi.getVolumeFreeSpace(self.device)
                except:
                    self.leftSpace = 0
                    self.freeSpace = 0
                    self.master.master.mh.addMessage(u'Laufwerksinformationen konnten nicht ausgelesen werden für: ' +
                                                     unicode(self), level=MDConst.MESSAGE_WARNING)
            else:
                self.requestChangeState(MDConst.DRIVE.ERROR, True)
                self.master.l.logEngine(u'Error: Laufwerk %s  hat den Status während des Löschens geändert' %
                                        unicode(self), logLevel=MDConst.LOG_ERROR)
                self.master.mh.addMessage(u'Error: Laufwerk %s  hat den Status während des Löschens geändert' %
                                          unicode(self), level=MDConst.MESSAGE_ERROR, requested=False)
            self.deleting = False
        self.deleteThread = None

        """
        if self.state == 0:
            self.state = 3

            # deleting

            if self.state == 3:
                try:
                    self.leftSpace = Osi.getVolumeFreeSpace(self.device)
                    self.freeSpace = Osi.getVolumeFreeSpace(self.device)
                except:
                    self.leftSpace = 0
                    self.freeSpace = 0
                    self.master.master.mh.addMessage(u'Laufwerksinformationen konnten nicht ausgelesen werden für: ' +
                                                     unicode(self), level=MDConst.MESSAGE_WARNING)
                self.state = 0
            else:
                self.state = -1
                self.master.l.logEngine(u'Error: Laufwerk %s  hat den Status während des Löschens geändert' %
                                        unicode(self), logLevel=MDConst.LOG_ERROR)
                self.master.mh.addMessage(u'Error: Laufwerk %s  hat den Status während des Löschens geändert' %
                                          unicode(self), level=MDConst.MESSAGE_ERROR, requested=False)
            self.deleting = False
        self.deleteThread = None
        """

    def start_delete_thread(self):
        self.deleteThread = threading.Thread(target=self.delete, args=(), name=u'DeleteThreadDrive' + unicode(self.id))
        self.deleteThread.start()

    def stop_deleting(self):
        self.deleting = False

    def updateSpace(self):
        if self.master.master.jh.hasJobsForDrive(self.id):
           return
        volume_info = Osi.getVolumeInfo(self.device)
        self.totalSpace = volume_info[2]
        self.freeSpace = volume_info[3]
        self.leftSpace = volume_info[3]


class DriveHandler(object):
    maxId = 0

    def __init__(self, master):
        self.master = master
        self.drives = []
        self.excludedDrives = []
        self.useExclusionList = True
        self.removableDrivesOnly = True
        self.load_settings()
        MDConst.events.general_load_settings.connect(self.load_settings)

    def __str__(self):
        return unicode(self).encode('utf-8')

    def __unicode__(self):
        return ', '.join(unicode(x) + u' ' + MDConst.DRIVE.STATE_DICT[x.state] + u' ' + x.file_system
                         for x in self.drives)

    @QtCore.Slot()
    def load_settings(self):
        settings = self.master.master.sth.get_general_settings()
        self.excludedDrives = settings[u'drives_excluded_drives']
        self.useExclusionList = settings[u'drives_use_exclusion_list']
        self.removableDrivesOnly = settings[u'drives_removable_drives_only']

    def update(self):
        drives = Osi.getVolumesList(self.removableDrivesOnly, self.useExclusionList, self.excludedDrives)

        # check if there are new drives
        temp_drives = drives[:]
        temp_drives2 = drives[:]
        for i in temp_drives:
            for j in self.drives:
                if i == j.device:
                    temp_drives2.remove(i)
        for i in temp_drives2:
            try:
                Osi.getVolumeName(i)
            except Exception as e:
                self.master.l.logEngine(u'Fehler beim Auslesen des Laufwerks %s' %i, logLevel=MDConst.LOG_WARNING)
                if not MDConst.FROZEN:
                    print e
                    print traceback.format_exc()
            else:
                new_drive = Drive(self, i)
                self.drives.append(new_drive)
                self.master.mh.addMessage(u'Neues Laufwerk erkannt: %s' % new_drive, level=MDConst.MESSAGE_INFO,
                                          section=[0,1])

        # check if there are missing drives
        temp_drives = self.drives[:]
        temp_drives2 = self.drives[:]
        drives = Osi.getVolumesList(self.removableDrivesOnly, self.useExclusionList, self.excludedDrives)
        for i in temp_drives:
            for j in drives:
                if i.device == j:
                    temp_drives2.remove(i)
        for i in temp_drives2:
            self.master.mh.addMessage(u'Laufwerk %s wurde unzulässig entfehrnt' % i.name, level=MDConst.MESSAGE_ERROR,
                                      section=[0, 1], requested=False)
            self.master.jh.forceDeleteJobsForDrive(i.id)
            self.drives.remove(i)

        # calculate free space
        for i in self.drives:
            try:
                i.freeSpace = Osi.getVolumeFreeSpace(i.device)
            except:
                self.master.mh.addMessage(u'Informationen über Laufwerk %s konnten nicht ausgelesen werden' %i,
                                          level=MDConst.MESSAGE_WARNING, section=[0,1], lifespan=2000)

        # check
        # for i in self.drives:
        #     if not self.master.jh.hasJobsForDrive(i.id) and i.state == 1:
        #         i.state = 0
        #         self.master.mh.addMessage(u'Zustand von %s auf idle gesetzt' % i, level=MDConst.MESSAGE_INFO)
        #         self.master.mh.addMessage(u'Unbekannter Fehler im Laufwerk %s' % i, level=MDConst.MESSAGE_WARNING,
        #                                  requested=False)

        #self.master.master.i.write_info('Drives', ', '.join(str(x) + x.state for x in self.drives))
        self.master.master.i.write_info(u'Drives', unicode(self))

    def hasDrive(self, device):
        for i in self.drives:
            if i.device == device:
                return True
        return False

    def ejectDrive(self, drive_id):
        for i in self.drives:
            if str(i.id) == drive_id:
                if i.requestChangeState(MDConst.DRIVE.EJECTING):
                    name = i.name
                    self.master.update_drive_pause += 1
                    try:
                        temp = i.eject()
                    except:
                        self.master.mh.addMessage(u'Fehler beim Entfernen es Laufwerk %s' % name,
                                                  level=MDConst.MESSAGE_ERROR, section=[0,1], requested=False)
                        i.requestChangeState(MDConst.DRIVE.ERROR, True)
                    else:
                        if temp:
                            self.master.mh.addMessage(u'Laufwerk %s erfogreich entfernt' % name,
                                                      level=MDConst.MESSAGE_INFO,section=[0,1], requested=False)
                        else:
                            self.master.mh.addMessage(u'Laufwerk %s konnte nicht entfernt werden' % name,
                                                      level=MDConst.MESSAGE_WARNING, section=[0,1], requested=False)
                            i.requestChangeState(MDConst.DRIVE.IDLE)
                        break
                    finally:
                        self.master.update_drive_pause -= 1
                else:
                    self.master.mh.addMessage(u'Laufwerk %s konnte nicht entfernt werden, es ist anderweitig beschäfitgt.'
                                              % i.name, level=MDConst.MESSAGE_INFO, section=[0,1], requested=False)

    def getMontpoints(self):
        mountpoint_list = []
        for i in self.drives:
            mountpoint_list.append(i.mountpoint)
        return mountpoint_list

    def getMountpointById(self, id):
        for i in self.drives:
            if i.id == id:
                return i.mountpoint
        return ''

    def getDriveById(self, drive_id):
        for i in self.drives:
            if i.id == drive_id:
                return i
        return None

    def checkDelete(self, drive_id):
        drive = self.getDriveById(drive_id)
        if not isinstance(drive, Drive):
            return False, u'Fehler in der Laufwerks-ID'
        if not drive.state == MDConst.DRIVE.IDLE:
            return False, u'Laufwerk ist anderweitig beschäftigt'
        result = self.master.master.th.check_job_ready()
        if not result[0]:
            return result
        return True, u'Laufwerk bereit zum Löschen' # todo testen

    def has_delete_tasks(self):
        for i in self.drives:
            if not i.deleteThread is None:
                return True
        return False


class FileHandler(object):
    def __init__(self, logger, master):
        self.logger = logger
        self.master = master
        self.rootDir = DefaultSettings.files[u'rootdir']
        self.load_settings()
        MDConst.events.files_load_settings.connect(self.load_settings)

    @QtCore.Slot()
    def load_settings(self):
        settings = self.master.master.sth.get_files_settings()
        self.rootDir = settings[u'rootdir']
        self.legalCopyFileExtensions = settings[u'copy_file_extensions']
        if self.rootDir is None:
            self.master.mh.addMessage(u'Aufnahmenverzeichnis wurde nicht gefunden.', level=MDConst.MESSAGE_ERROR,
                                      requested=False)

    def getExplorerDirContent(self, path):
        if not os.path.isdir(path):
            return []
        try:
            files = os.listdir(path)
        except Exception as e:
            self.master.mh.addMessage(u'Fehler bei Auslesen eines Ordners %s' % e, level=MDConst.MESSAGE_WARNING)
            return []
        content = []
        for i in files:
            if os.path.isdir(os.path.join(path, i)) or self.isLegalExplorerFile(os.path.join(path, i)):
                content.append(i)
        return content

    def isLegalExplorerFile(self, file):
        if Osi.WINDOWS:
            file = file.lower()
        if Osi.isHiddenPath(file):
            return False
        if not os.path.isfile(file):
            return False
        return True

    def isLegalCopyFile(self, file):
        if not os.path.isfile(file):
            return False
        for i in self.legalCopyFileExtensions:
            if file.endswith(i):
                return True
        return False

    def isLegalDir(self, path):
        if not os.path.isdir(path):
            return False
        if Osi.isHiddenPath(path):
            return False
        if not self.rootDir is None and path.startswith(self.rootDir):
            return True
        for i in self.master.dh.excludedDrives: # todo testen ob exclueded dives richtig ist
            if path.startswith(i):
                return False
        return True

    def getSpaceStr(self, space):
        spaceDict = {0: u'B', 1: u'kB', 2: u'MB', 3: u'GB', 4: u'TB'}
        i = 0
        while space > 1024:
            space /= 1024.0
            i += 1
            if i == 5:
                break
        return u"%.1f %s" % (space, spaceDict[i])

    @staticmethod
    def decodeURL(text):
        temp = urllib.unquote(text.replace('+', ' '))
        return temp.decode('utf-8')

    @staticmethod
    def correctDirName(dir_name):
        return re.sub(r'[\n\t\a\f\\\r\b\./:\*<>|\?"]', '', dir_name)


class RecordJob(object):
    def __init__(self, master, record_job_id, driveId, recordsIdList):
        self.master = master
        self.id = record_job_id
        self.drive = master.master.dh.getDriveById(driveId)
        self.progress = 0
        self.buf_size = 16*1024
        self.nr_of_loops = 0
        self.start_time = datetime.datetime.now()
        self.left_time = u'wird berechnet'
        self.last_times = list()
        self.calc_time_cycle = 10
        self.recordList = list()
        for i in recordsIdList:
            j = master.master.rh.getRecordById(i)
            if j:
                self.recordList.append(copy.deepcopy(j))
            else:
                self.master.master.mh.addMessage(u'Fehler im Aufsführen eines Aufnahmeauftrages: Aufnahme fehlt',
                                                 level=MDConst.MESSAGE_WARNING, requested=False)
        self.number_of_files = 0
        for i in self.recordList:
            self.number_of_files += len(i.files)
        if self.number_of_files == 0:
            master.master.mh.addMessage(u'Auftrag hat keine Dateien', level=MDConst.MESSAGE_WARNING, requested=False)
            return
        space = 0
        for i in self.recordList:
            space += i.size
        self.size = space
        self.expected_loops = int(self.size / self.buf_size)
        self.drive.leftSpace = self.drive.leftSpace - space
        if self.drive.leftSpace < 0:
            self.drive.leftSpace = 0
        if self.drive.requestChangeState(MDConst.DRIVE.RECORD_JOB_RUNS):
            self.run = True
            thread_name = u'RecordJob%i' % self.id
            self.thread = threading.Thread(target=self.copyFiles, args=(), name=thread_name)
            self.thread.start()
        else:
            self.master.master.mh.addMessage(u'Fehler im Aufsführen eines Aufnahmeauftrages: Laufwerk %s anderweitig '
                                             u'beschäftigt' % unicode(self.drive), level=MDConst.MESSAGE_WARNING,
                                             requested=False)

    def __str__(self):
        return 'RecordJob ID: %i, Target: %s, Progress: %i of %i' % (self.id, str(self.drive), self.progress,
                                                                     self.number_of_files)

    def __unicode__(self):
        return u'RecordJob ID: %i, Target: %s, Progress: %i of %i' % (self.id, unicode(self.drive), self.progress,
                                                                      self.number_of_files)

    def getProgress(self):
        return self.progress, self.number_of_files

    def getProgressSizePercent(self):
        if self.size > 0:
            progress = (self.nr_of_loops * self.buf_size * 100) / self.size
            if progress >= 99:
                return 99
            else:
                return progress
        return 0

    def calculateLeftTime(self):
        '''
        self.last_times.append(datetime.datetime.now())
        temp = 50
        if len(self.last_times) > temp:
            self.last_times = self.last_times[-1*temp:-1]
        if self.last_times:
            delta_time = self.last_times[-1] - self.last_times[0]
            time_per_loop = delta_time.total_seconds() / (len(self.last_times) * self.calc_time_cycle)
            left_loops = self.expected_loops - self.nr_of_loops
            if left_loops <= 0:
                self.left_time = 'wird berechnet'
                return
            left_time = left_loops * time_per_loop
            self.left_time = str(int(left_time))
        else:
            self.left_time = 'wird berechnet'
        '''
        if self.nr_of_loops < 1000:
            self.left_time = u'wird berechnet'
            return

        run_time = datetime.datetime.now() - self.start_time
        time_per_loop = run_time.total_seconds() / self.nr_of_loops
        #print 'time_per_loop', time_per_loop, self.nr_of_loops, run_time.total_seconds()
        left_loops = self.expected_loops - self.nr_of_loops
        if left_loops <= 0:
            self.left_time = u'wird berechnet'
            return
        left_time = left_loops * time_per_loop

        self.left_time = u'wird berechnet'

        if left_time >= 3600: # > 1h
            temp = divmod(left_time, 3600)
            self.left_time = u'>%i h' % int(temp[0])
        elif left_time >= 600: # 1h - 15min / 15min
            temp = divmod(left_time, 900)
            self.left_time = u'%i min' % int((temp[0] + 1) * 15)
        elif left_time >= 60: # 15min - 1min / 1min
            temp = divmod(left_time, 60)
            self.left_time = u'%i min' % int(temp[0] + 1)
        elif left_time >= 15: # 1min - 15s / 15s
            temp = divmod(left_time, 15)
            self.left_time = u'%i s' % int((temp[0] + 1) * 15)
        elif left_time >= 1: # 15s - 1s / 1s
            self.left_time = u'%i s' % int(left_time + 1)
        else: # < 1s
            self.left_time =  u'gleich fertig'



        '''
        if left_time >= 3600: # > 1h
            temp = divmod(left_time, 3600)
            self.left_time = u'>%i h' % temp[0]
        elif left_time >= 900: # 1h - 15min / 15min
            temp = divmod(left_time, 600)
            self.left_time = u'%i min' % (temp[0] * 15)
        elif left_time >= 60: # 15min - 1min / 1min
            temp = divmod(left_time, 60)
            self.left_time = u'%i min' % temp[0]
        elif left_time >= 15: # 1min - 15s / 15s
            temp = divmod(left_time, 15)
            self.left_time = u'%i s' % (temp[0] * 15)
        elif left_time >= 1: # 15s - 1s / 1s
            self.left_time = u'%i s' % int(left_time)
            pass
        else: # < 1s
            self.left_time = u'gleich fertig'
        '''
        #print divmod(left_time, 60)

        ''''
        if left_time > 5400:
            self.left_time = u'sehr lange'
        elif left_time > 600.0:
            temp = divmod(left_time, 600)
            temp2 = divmod(left_time, 60)
            if temp[1] > 0.5:
                self.left_time = u'%imin ' % (10 * (temp[0] + 1))
            else:
                self.left_time = u'%imin ' % (10 * (temp[0] + 0.5))
        elif left_time > 900.0: # 15 min
            temp = divmod(left_time, 60)
            if temp[1] >= 45.0:
                self.left_time = u'%imin ' % (temp[0] + 1)
            elif temp[1] >=15.0:
                self.left_time = u'%imin 30s ' % temp[0]
            else:
                self.left_time  = u'%imin ' % temp[0]
        elif left_time >= 15.0:
            temp = divmod(left_time, 60)
            if temp[1] > 45:
                self.left_time = '1 min'
            elif temp[1] > 30:
                self.left_time = '45 s'
            elif temp[1] > 15:
                self.left_time = '30 s'
            else:
                self.left_time = '15 s'
        elif left_time >= 1.0:
            self.left_time = u'%i s' % int(left_time + 1)
        else:
            self.left_time = u'gleich fertig'
        '''

        #self.left_time = str(left_time)
        #'''

    def getLeftTime(self):
        return self.left_time

    def cancel(self):
        self.run = False

    def copyFiles(self):
        number_of_errors = 0
        self.master.master.mh.addMessage(u'Auftrag gestartet für Laufwerk ' + unicode(self.drive),
                                         level=MDConst.MESSAGE_INFO)
        for i in self.recordList:
            self.master.master.stat.copyRecord(i.dir)
            if not self.run:
                break
            dirName = self.master.makeRecordDirName(self.drive.mountpoint, i)
            if os.path.isdir(dirName):
                self.master.master.mh.addMessage(u'Fehler im ausführen eines Kopierauftrages: Ordner mit dem Namen %s '
                                                 u'ist bereits vorhanden' % dirName, level=MDConst.MESSAGE_ERROR,
                                                 requested=False)
                self.number_of_files -= len(i.files)
                number_of_errors += len(i.files)
                continue
            os.mkdir(dirName)
            self.master.master.l.logCopy(u'Start copy record -> Event: %s Date: %s, %d %d; RecordID: %d; RecordJobID '
                                         u':%d; Drive: (%s)' %
                                         (i.event, calendar.month_name[i.month], i.day, i.year, i.id, self.id,
                                          self.drive.get_info_unicode()), logLevel=MDConst.LOG_INFO)
            for j in i.files:
                if not self.run:
                    break
                source_file = os.path.join(i.dir, j)
                try:
                    self.master.master.l.logCopy(u'Start copy file: %s, RecordID: %d, RecordJobID: %d' %
                                                 (source_file, i.id, self.id), logLevel=MDConst.LOG_INFO)

                    target_file_name = os.path.join(dirName, os.path.basename(source_file))
                    src_file = open(source_file, 'rb')
                    target_file = open(target_file_name, 'wb')
                    not_fat_file_system = not self.drive.file_system.lower().startswith('fat')
                    while self.run:
                        buf = src_file.read(self.buf_size)
                        if not buf:
                            break
                        target_file.write(buf)
                        target_file.flush()
                        if not_fat_file_system:
                            os.fsync(target_file.fileno())
                        self.nr_of_loops += 1
                        if not self.nr_of_loops % self.calc_time_cycle:
                            self.calculateLeftTime()
                    src_file.close()
                    target_file.close()
                    if not self.run:
                        os.remove(target_file_name)
                    else:
                        shutil.copystat(source_file, target_file_name)
                        self.master.master.stat.copyFile(source_file)
                    #'''

                except Exception as e:
                    #print traceback.format_exc()
                    self.master.master.mh.addMessage(u'Fehler beim kopieren einer Datei nach %s Fehler: %s' %
                                                     (dirName, unicode(e)), level=MDConst.MESSAGE_ERROR)
                    self.master.master.l.logCopy(u'Error in copy file: %s, RecordID: %d, RecordJobID: %d' %
                                                 (source_file, i.id, self.id), logLevel=MDConst.LOG_ERROR,)
                    self.number_of_files -= 1
                    number_of_errors += 1
                else:
                    self.progress += 1
                    self.master.master.l.logCopy(u'Finished copy file: %s, RecordID: %d, RecordJobID: %d' %
                                                 (source_file, i.id, self.id), logLevel=MDConst.LOG_INFO)
            self.master.master.l.logCopy(u'Finished copy record ->  RecordID: %d, RecordJobID: %d' %
                                         (i.id, self.id), logLevel=MDConst.LOG_INFO)
        if self.drive.requestChangeState(MDConst.DRIVE.IDLE):
            if self.run:
                if number_of_errors == 0:
                    self.master.master.mh.addMessage(u'Auftrag erfolgreich abgeschlossen für Laufwerk ' +
                                                     unicode(self.drive), requested=False, level=MDConst.MESSAGE_INFO)
                else:
                    self.master.master.mh.addMessage(u'Auftrag abgeschlossen für Laufwerk %s mit %i Fehlern' %
                                                     (unicode(self.drive), number_of_errors), requested=False,
                                                     level=MDConst.MESSAGE_ERROR)
            else:
                if number_of_errors == 0:
                    self.master.master.mh.addMessage(u'Auftrag für Laufwerk ' + unicode(self.drive) + ' abgebrochen.',
                                                     requested=False, level=MDConst.MESSAGE_INFO)
                else:
                    self.master.master.mh.addMessage(u'Auftrag für Laufwerk %s abgebrochen mit %i Fehlern' %
                                                     (unicode(self.drive), number_of_errors), requested=False,
                                                     level=MDConst.MESSAGE_ERROR)
        else:
            self.drive.requestChangeState(MDConst.DRIVE.ERROR, True)
            self.master.master.mh.addMessage(u'Fehler im Status des Laufwerks ' + unicode(self.drive), requested=False,
                                             level=MDConst.MESSAGE_WARNING)
        self.master.jobs.remove(self)
        try: # todo alle messages testen
            self.drive.updateSpace()
        except:
            self.master.master.mh.addMessage(u'Fehler beim Auslesen der Datenträgergröße ' + unicode(self.drive),
                                             level=MDConst.MESSAGE_WARNING)
        #print 'needed time', datetime.datetime.now() - self.start_time
        #print self.nr_of_loops


class JobHandler(object):
    maxId = 0

    def __init__(self, master):
        self.master = master
        self.maxJobsOnTime = 10
        self.jobs = []

    def __str__(self):
        return '; '.join(str(x) for x in self.jobs)

    def __unicode__(self):
        return '; '.join(unicode(x) for x in self.jobs)

    def makeRecordDirName(self, mountpoint, record):
        dirName = u'%i_%02i_%02i_%s_%s' % (record.year, record.month, record.day,
                                                     self.master.fh.correctDirName(record.event),
                                                     self.master.fh.correctDirName(record.church))
        return os.path.join(mountpoint, dirName)

    def addRecordJob(self, driveId, recordsIdList):
        check_result = self.checkRecordJob(driveId, recordsIdList)
        if check_result[0]:
            self.jobs.append(RecordJob(self, JobHandler.maxId, driveId, recordsIdList))
            JobHandler.maxId += 1
        else:
            self.master.mh.addMessage(u'Auftrag konnte nicht gestartet werden: %s' % check_result[1],
                                      level=MDConst.MESSAGE_WARNING, requested=False)
            # todo testen

    def checkRecordJob(self, driveId, recordsIdList):
        result = self.master.master.th.check_job_ready()
        if not result[0]:
            return result
        if self.numberOfJobs() >= self.maxJobsOnTime:
            return False, u"Maximale Anzahl von Aufträgen erreicht"
        record = self.getRecordJobByDriveId(driveId)
        if record:
            return False, u'Es läuft schon ein Auftrag für dieses Laufwerk'
        drive = self.master.dh.getDriveById(driveId)
        if not isinstance(drive, Drive):
            return False, u'Laufwerk nicht vorhanden'
        if not drive.state == MDConst.DRIVE.IDLE:
            return False, u'Laufwerk anderweitig beschäfigt'
        recordsList = [self.master.rh.getRecordById(i)for i in recordsIdList]
        if len(recordsList) == 0:
            return False, u'Es wurden keine Aufnahmen angegeben'
        if None in recordsList:
            return False, u'Ausgewählte Aufnahme nicht vorhanden'
        totalSpace = 0
        dirs = list()
        for i in recordsList:
            totalSpace += i.size
            dir_name = self.makeRecordDirName('', i)
            dirs.append(dir_name)
        dirs_on_drive = os.listdir(unicode(drive.mountpoint))
        problem_dirs = list()
        for i in dirs_on_drive:
            if i in dirs:
                problem_dirs.append(i)
        if problem_dirs:
            return False, u"Es gibt schon Ordner mit dem/n Namen auf dem Laufwerk: " + u', '.join(problem_dirs)
        double_dirs = list()
        for i in dirs:
            if dirs.count(i) > 1 and i not in double_dirs:
                double_dirs.append(i)
        if double_dirs:
            return False, u"Folgende Aufnahmen wurden doppelt ausgewählt: " + u", ".join(double_dirs)
        if totalSpace > drive.leftSpace:
            return False, u'Es ist nicht genug Speicherplatz auf dem Speichermedium vorhanden. Es fehlen %s' \
                   % self.master.fh.getSpaceStr(totalSpace - drive.leftSpace)
        return True, u'Job ist in Ordnung'

    def addJob(self):
        pass

    def checkJob(self):
        pass

    def getRecordJobById(self):
        for i in self.jobs:
            if isinstance(i, RecordJob) and i.id == id:
                return i
        return None

    def getRecordJobByDriveId(self, id):
        for i in self.jobs:
            if isinstance(i, RecordJob) and i.drive.id == id:
                return i
        return None

    def numberOfJobs(self):
        return len(self.jobs)

    def numberOfRunningJobs(self):
        number = 0
        for i in self.jobs:
            if i.state in (MDConst.DRIVE.RECORD_JOB_RUNS, MDConst.DRIVE.FILE_JOB_RUNS):
                number += 1
        return number

    def hasJobsForDrive(self, id):
        drive = self.master.dh.getDriveById(id)
        for i in self.jobs:
            if i.drive == drive:
                return True
        return False

    def deleteJob(self, job):
        job.stop()
        while job.isRunning():
            time.sleep(0.01)
        self.jobs.remove(job)

    def deleteAllJobs(self):
        for i in self.jobs:
            self.deleteJob(i)

    def getJobsForDrive(self, drive_id):
        jobs = list()
        for i in self.jobs:
            if i.drive.id == drive_id:
                jobs.append(i)
        return jobs

    def forceDeleteJobsForDrive(self, drive_id):
        for i in self.jobs:
            if i.drive.id == drive_id:
                i.cancel()
                self.jobs.remove(i)

    def update(self):
        self.master.master.i.write_info(u'Jobs', unicode(self))
        for i in self.jobs:
            if not i.thread.is_alive():
                i.cancel()
                self.jobs.remove(i)


class RecordHandler(object):
    def __init__(self, master):
        self.master = master
        self.records = []
        self.files = []
        self.collections = []
        self.default_church = DefaultSettings.general[u'default_church']
        self.churches = []
        self.maxId = 0 # for records
        self.maxFilesId = 0
        self.maxCollectionsId = 0
        self.is_scanning = False
        self.request_scan = False
        self.run = True
        self.thread = threading.Thread(target=self.update, args=(), name=u'RecordHandlerUpdater')
        self.thread.daemon = True
        self.load_settings()
        MDConst.events.general_load_settings.connect(self.load_settings)
        #self.updateRecords()

    @QtCore.Slot()
    def load_settings(self):
        settings = self.master.master.sth.get_general_settings()
        self.default_church = settings[u'default_church']
        if not self.default_church in self.churches:
            self.churches.append(self.default_church)

    def get_default_church(self):
        return self.default_church

    def read_record_info(self, path):
        record_info = dict()
        tree = et.parse(path + '/' + 'record_info.xml')
        xml_root = tree.getroot()
        # version
        record_info['version'] =  int(xml_root.find('version').text)
        assert record_info['version'] >= 0
        # day
        record_info['day'] = int(xml_root.find('day').text)
        assert record_info['day'] in range(1, 32)
        # month
        record_info['month'] = int(xml_root.find('month').text)
        # year
        record_info['year'] =  int(xml_root.find('year').text)
        assert record_info['year'] >= 2000
        # church
        church = xml_root.find('church').text
        if church is None:
            record_info['church'] = u''
        else:
            record_info['church'] = unicode(church)
        # event
        event = xml_root.find('event').text
        if event is None:
            record_info['event'] = u'Aufnahme'
        else:
            record_info['event'] = self.master.fh.correctDirName(unicode(xml_root.find('event').text))
        # info
        info = xml_root.find('info').text
        if info is None:
            record_info['info'] = u''
        else:
            record_info['info'] = unicode(info)
        # order
        if record_info['version'] > 0:
            record_info['order'] = int(xml_root.find('order').text)
        else:
            record_info['order'] = 0
        return record_info

    def read_collection_info(self, path):
        # todo check if file paths exists
        pass
        return dict()

    def updateRecords(self): #todo make this more efficent
        if self.is_scanning:
            return
        self.is_scanning = True
        self.master.l.logEngine(u'scan started')
        self.master.mh.addMessage(u'Aufnahmen-Scan begonnen')
        changes = False
        new_records = self.scanRecords()
        old_records = self.records[:]

        # compare function add new to old
        for i in old_records[:]:
            is_in = False
            for j in new_records:
                if i == j:
                    is_in = True
                    break
            if not is_in:
                changes = True
                old_records.remove(i)

        for i in new_records:
            is_in = False
            for j in old_records:
                if i == j:
                    is_in = True
                    break
            if not is_in:
                changes = True
                # add id's
                temp = i
                self.maxId += 1
                temp.id = self.maxId
                old_records.append(temp)

        # scan collections
        old_collections = self.collections[:]
        new_collections = self.scanCollections()

        # todo calculate files
        old_files = self.files[:]
        if changes:
            new_files = list()
            for i in old_records:
                for j in i.files:
                    path = os.path.join(i.dir, j)
                    is_in = False
                    for k in new_files:
                        if k.path == path:
                            is_in = True
                            break
                    if not is_in:
                        new_files.append(File(None, i.day, i.month, i.year, i.church, i.event, i.info, path))
        else:
            new_files = old_files
        # add new to old files

        ''''
        # calculate size
        for i in old_records:
            size = 0
            for j in i.files:
                file = os.path.join(i.dir, j)#.decode('utf-8')
                size += os.path.getsize(file)
            i.size = size
        '''

        churches = list()
        if changes:
            for i in old_records:
                if i.church not in churches:
                    churches.append(i.church)
        else:
            churches = self.churches

        if not self.default_church in churches:
            churches.append(self.default_church)

        if changes:
            total_record_size = sum([i.size for i in old_records])
            total_collections_size = sum([i.size for i in self.collections])
            total_data_obj_size = sum([i.size for i in new_files])
            self.master.master.i.write_info('Size All Records', self.master.fh.getSpaceStr(total_record_size))
            self.master.master.i.write_info('Size All Files', self.master.fh.getSpaceStr(total_data_obj_size))

        # write results
        self.records = old_records
        self.churches = churches
        self.files = new_files

        self.master.l.logEngine(u'scan finished')
        self.master.mh.addMessage(u'Aufnahmen-Scan beendet')
        self.is_scanning = False
        self.request_scan = False

    def update(self):
        while self.run:
            try:
                self.updateRecords()
            except Exception as e:
                #traceback.print_exc()
                self.master.l.logEngine(u'Fehler beim Nachladen der Aufnahmen', logLevel=MDConst.LOG_WARNING) # todo anderer text
                self.cleanup()
            start_time = datetime.datetime.now()
            while self.run \
                    and (datetime.datetime.now() - start_time).total_seconds() < (MDConst.UPDATE_TIME_RH / 10) \
                    and not self.request_scan:
                time.sleep(1)

    def start(self):
        self.thread.start()

    def cleanup(self):
        self.is_scanning = False
        self.request_scan = False

    def scanRecords(self):
        records = list()
        if self.master.fh.rootDir is None:
            return records
        for root, dirs, files in os.walk(self.master.fh.rootDir):
            if u'record_info.xml' in files:
                try:
                    record_info = self.read_record_info(root)
                except:
                    self.master.mh.addMessage(u'Aufnahme konnte nicht ausgelesen werden: record_info.xml in %s fehlerha'
                                              u'ft.' % root, level=MDConst.MESSAGE_WARNING) # todo testen
                    continue
                else:
                    legalFiles = list()
                    for i in files:
                        if self.master.fh.isLegalCopyFile(root + '/' + i):
                            legalFiles.append(i)
                    if not len(legalFiles) == 0:
                        records.append(Record(None, root, legalFiles, record_info['day'], record_info['month'],
                                              record_info['year'], record_info['church'], record_info['event'],
                                              record_info['info'], record_info['order']))
                    #else:
                        #print files
        return records

    def scanCollections(self):
        collections = list()
        if self.master.fh.rootDir is None:
            return collections
        colls_dir = os.path.join(self.master.fh.rootDir, MDConst.COLLECTIONSDIR)
        if os.path.isdir(colls_dir):
            for i in os.listdir(colls_dir):
                if i.lower().endswith('.xml'):
                    try:
                        coll_path = os.path.join(colls_dir, i)
                        temp_info = self.read_collection_info(coll_path)
                    except:
                        pass # todo make sensefull error message
                    else:
                        pass # todo
                        #temp_coll = Collection(0, temp_info['fDay'])
                        #collections.append(temp_coll)
        return collections

    def getRecordsByDate(self, date):
        record_list = []
        for i in self.records:
            if date == u'%i.%02i.%04i' %(i.day, i.month, i.year):
                record_list.append(i)
        return record_list

    def getRecordById(self, id):
        for i in self.records:
            if i.id == id:
                return i
        return None

    def getRecordsDict(self):
        records_dict = dict()
        for i in self.records:
            date = u'%d.%02d.%04d' % (i.day, i.month, i.year)
            records_dict[date] = {'id': i.id, 'event': i.event, 'size': i.size}
        return records_dict

    def getAllRecords(self):
        data = dict()
        '''
        for i in self.records:
            date = '%i.%02i.%i' % (i.day, i.month, i.year)
            if not data.has_key(date):
                data[date] = []
            data[date].append({'id': i.id, 'name': i.event, 'size': i.size})
        return data
        '''
        for i in self.records:
            date = u'%i.%02i.%i' % (i.day, i.month, i.year)
            if not data.has_key(date):
                data[date] = []
            data[date].append(i)
        data_2 = dict()
        for i, j in data.iteritems():
            temp = sorted(j, key=lambda k: k.order, reverse=True)
            data_2[i] = list()
            for l in temp:
                data_2[i].append({u'id': l.id, u'name': l.event, u'size': l.size,
                                  u'church': l.church, u'date': u'%i-%02i-%02i' % (l.year, l.month, l.day)})
        return data_2
        #'''

    def getChurches(self):
        return self.churches


class DataObject(object):
    def __init__(self, data_id, day, month, year, church, event, info):
        self.id = data_id
        self.day = day
        self.month = month
        self.year = year
        self.church = church
        self.event = event
        self.info = info
        self.size = 0

    def __unicode__(self):
        return u'DataObject'


class Record(DataObject):
    def __init__(self, data_id, record_dir, files, day, month, year, church, event, info, order):
        super(self.__class__, self).__init__(data_id, day, month, year, church, event, info)
        self.dir = record_dir
        self.files = files
        self.order = order
        self.info = info
        size = 0  # todo checken ob der parameter size passt
        for i in self.files:
            file_path = os.path.join(self.dir, i)
            size += os.path.getsize(file_path)
        self.size = size

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        if not self.dir == other.dir:
            return False
        if not self.files == other.files:
            return False
        if not self.day == other.day:
            return False
        if not self.month == other.month:
            return False
        if not self.year == other.year:
            return False
        if not self.church == other.church:
            return False
        if not self.event == other.event:
            return False
        if not self.info == other.info:
            return False
        return True

    def __unicode__(self):
        return unicode(self.id) + ' ' + self.dir + ' ' + u':'.join(self.files) + \
               u' %i.%02i.%04i' % (self.day, self.month, self.year) + ' ' + self.event


class File(DataObject):
    def __init__(self, data_id, day, month, year, church, event, info, path):
        super(self.__class__, self).__init__(data_id, day, month, year, church, event, info)
        self.path = path
        self.size = os.path.getsize(self.path)
        #selfself.master = master
        #self.size = getsize...
        #self.fDay, self.fMonth, self.fYear = checkDate(fDay, fMonth, fYear)
        #self.lastDate = chechDate(lastDate)

    def __eq__(self, other):
        return self.path == other.path


class Collection(DataObject):
    def __init__(self, data_id, day, month, year, church, event, info, files_list, lDay, lMonth, lYear):
        super(self.__class__, self).__init__(data_id, day, month, year, church, event, info)
        # todo some more parameters

    def __eq__(self, other):
        raise # todo not implemented jet


class CopyObject(object):
    def __init__(self, data):
        self.files_dict = dict()
        if isinstance(data, Record):
            pass
        elif isinstance(data, list):
            for i in data:
                if not isinstance(i, File):
                    raise # wrong type error
        elif isinstance(data, Collection):
            pass
        else:
            raise # wrong data type
        # calc size

    def __len__(self):
        return len(self.files_dict)


class Job(object):
    def __init__(self, drive, object_list):
        # sort elements in object list by type
        # create CopyObjects from sorted elements
        # calc size form CopyObjects (for progress calculations)
        # start job
        pass


class ServerInterface(object):
    def __init__(self, master):
        self.master = master
        self.numberOfFinishErrors = 0
        self.numberOfRequests = 0

    def incrNumberOfFinishErrors(self):
        self.numberOfFinishErrors += 1
        self.master.master.i.write_info(u'NumberOfServerErrors', self.numberOfFinishErrors)

    def incrNumberOfRequests(self):
        self.numberOfRequests += 1
        self.master.master.i.write_info(u'NumberOfServerRequests', self.numberOfRequests)

    def getDriveMessagesSidebar(self):
        html = ''
        for i in self.master.dh.drives:
            html += u'''<div class="volume">
                    Laufwerk %s<br>

                    <div class="progress">
                        <div class="progress-bar" role="progressbar" aria-valuenow="%i" aria-valuemin="0"
                        aria-valuemax="%i" style="width: %i%%;">
                        </div>
                    </div>
                    %s frei von %s
                    </div>''' %(i, i.totalSpace - i.freeSpace, i.totalSpace,
            ((i.totalSpace - i.freeSpace) *100) / i.totalSpace, self.master.fh.getSpaceStr(i.freeSpace),
            self.master.fh.getSpaceStr(i.totalSpace))
        if html == u'':
            html = u'<div class="volume">Kein Laufwerk angeschlossen</div>'
        return html

    def getMessageMessagesSidebar(self):
        html = ''
        for i in (3, 2, 1, 0):
            for j in self.master.mh.getSortedMessages(1, i):
                html += u'<div class="message%i" id="message%i">%s</div>' % (i, i, j.message)
        if html == u'':
            return u'<div class="message_null" id="message_null">Keine Meldung</div>'
        return html

    def getJobMessagesSidebar(self):
        html = ''
        if len(self.master.jh.jobs) == 0:
            html = u'<div class="no_job" id="no_job">Kein Auftrag</div>'
        else:
            for i in self.master.jh.jobs:
                html += u'<div class="job" id="job">'
                html += u'Ziel: %s' % i.drive.name
                html += u'<ul>'
                for j in i.recordList:
                    html += u'<li>%s %i.%02i.%04i</li>' % (j.event, j.day, j.month, j.year)
                html += u'</ul>'
                progress = i.getProgress()
                left_time = i.getLeftTime()
                '''
                if progress[1] > 0:
                    progress_percent = ((progress[0] + 1) * 100)/progress[1]
                else:
                    progress_percent = 0
                '''
                progress_percent = i.getProgressSizePercent()
                html += r'<div class="progress"><div class="progress-bar progress-bar-success progress-bar-striped" ' \
                        r'style="width:%i%%;min-width: 2em;" role="progressbar"> %i%% </div></div>' % \
                        (progress_percent, progress_percent)
                html += u'Datei %i von %i' % ((progress[0] + 1), progress[1])
                html += u'<br>Vorraussichtliche Dauer: %s' % left_time
                html += u'</div>'
        return html

    def getModalMessages(self, ip):
        return self.master.mh.getModalMessages(ip)

    def getDrives(self):
        html = ''
        for i in self.master.dh.drives:
            html += u'''<div class="drive" id="drive">
            Laufwerk %s <br>
            <div class="progress">
                <div class="progress-bar" role="progressbar" aria-valuenow="%i" aria-valuemin="0" aria-valuemax="%i"
                style="width: %i%%;">
                </div>
            </div>
            %s frei von %s<br>
            <div class="btn-group">
            <button type="button" class="btn btn-default" onclick="ask_eject_drive('%i', '%s')">Laufwerk auswerfen</button>
            </div>
            </div><br>''' %(i, i.totalSpace - i.freeSpace, i.totalSpace,((i.totalSpace - i.freeSpace) *100) / i.totalSpace,
                        self.master.fh.getSpaceStr(i.freeSpace), self.master.fh.getSpaceStr(i.totalSpace), i.id, unicode(i))
        if html == u'':
            html = u'<div class="nodrive" id="nodrive">Kein Laufwerk angeschlossen</div>'
        return html

    def ejectDrive(self, id):
        eject_thread = threading.Thread(target=self.master.dh.ejectDrive, args=(id,),
                                        name=u'EjectThreadDrive' + unicode(id))
        eject_thread.start()

    def getDriveMessagesDrives(self):
        html = ''
        for i in (3, 2, 1, 0):
                html += u'<div class="message%i" id="message%i">%s</div>' %(i, i, j.message)
        if html == '':
            return html
        else:
            return html + u'<br>'

    def getMedia(self, path):
        return os.path.join(self.master.fh.rootDir, path)

    def explorerGetDir(self, par_path):
        #path = self.master.fh.decodeURL(par_path)
        path = par_path
        if (path.startswith(u'Media') or path.startswith(u'/Media')) and self.master.fh.rootDir is None:
            return u'Kein Aufnahmenverzeichnis angegeben.', u'Media', ''

        legal_paths = list()
        if not self.master.fh.rootDir is None:
            legal_paths.append(self.master.fh.rootDir.replace('\\', '/'))
        for i in self.master.dh.getMontpoints():
            legal_paths.append(i.replace('\\', '/'))

        if path.startswith(u'/Media'):
            path = self.master.fh.rootDir + path[6:]
        elif path.startswith(u'Media'):
            path = self.master.fh.rootDir + path[5:]
        if Osi.LINUX and (self.master.fh.rootDir is None or
                not path.startswith(self.master.fh.rootDir)) and not path.startswith('/media'): # todo doesnt work with rootDir is None
            path = '/media/' + os.popen('echo "$USER"').read().decode('utf-8').strip('\n') + '/' + path.lstrip('/')
        elif Osi.WINDOWS and len(path) == 2:
            path += '/'
        path = path.replace('//', '/')

        if Osi.WINDOWS:
            if path.startswith('/'):
                path = '/' + path  # das ist für Server-Ordner unter Windows

        is_in = False
        for i in legal_paths:
            if path.startswith(i):
                is_in = True
                break

        if not is_in:
            return u'Der Pfad existiert nicht (mehr)', 'Media', ''

        # make path above list
        if not path == self.master.fh.rootDir: #todo wozu wird dass hier eigentlich gebraucht
            path_above = path
        else:
            path_above = self.master.fh.rootDir
        if not self.master.fh.rootDir is None:
            if len(path_above) < len(self.master.fh.rootDir) and self.master.fh.rootDir.startswith(path_above):
                path_above = self.master.fh.rootDir
                path = self.master.fh.rootDir
            path_above = path_above.replace(self.master.fh.rootDir, u'Media')
        if Osi.LINUX:
            for i in legal_paths:
                if path_above.startswith(i):
                    path_above = '/'.join(path_above.split('/')[3:])
                    break
        path_above_list = path_above.split('/')
        while '' in path_above_list:
            path_above_list.remove('')
        #print path_above_list

        # make html
        html = u''

        if os.path.isdir(path):
            for i in self.master.fh.getExplorerDirContent(path):
                if self.master.fh.isLegalDir(path + u'/' + i) and not i.startswith(u'$'):
                    html += u'''<p name="exp_item"
                    id="dir_%s" ondblclick="doubleClick(event)" onclick="togglebgcolor(event)"><img border="0"
                     class="exp_img" src="file:///%s/static/images/folder.png" alt="Ordner" height="36" width="41"
                     style="vertical-align:bottom"> %s </p>''' %(path + '/' + i, os.path.abspath('.'), i)
                elif self.master.fh.isLegalExplorerFile(path + u'/' + i): # todo dateiendungen mit dict / list
                    if (i.endswith(u'.mp3') or i.endswith(u'.wav')):
                        html += u'''<p name="exp_item"
                        id="%s_%s" ondblclick="doubleClick(event)" onclick="togglebgcolor(event)">
                        <img class="exp_img" border="0" src="file:///%s/static/images/music.png" alt="Music" height="36" width="37"
                         style="vertical-align:bottom"> %s </p>''' % (i[-3:], path + u'/' + i, os.path.abspath(u'.'), i)
                    else:
                        html += u'''<p name="exp_item"
                        id="fil_%s" onclick="togglebgcolor(event)">
                        <img class="exp_img" "border="0" src="file:///%s/static/images/file.png" alt="Datei" height="36" width="28"
                         style="vertical-align:bottom"> %s </p>''' % (path + u'/' + i, os.path.abspath(u'.'), i)
            if html == u'':
                html = u'Der Pfad ist leer oder die Dateien können nicht dargestellt werden'
        else:
            html = u'Der Pfad existiert nicht (mehr)'


        # read record data
        record_data = ''
        if 'record_info.xml' in os.listdir(path):
            try:
                record_info = self.master.rh.read_record_info(path)
            except:
                record_data = u'Datei "record_info.xml" ist nicht auslesbar.'
            else:
                record_data = u'<table  class="table table-striped"><thead><th>record_info.xml</th><th/></thead>'
                record_data += u'<tbody>'
                record_data += u'<tr><th>Version</th><td>%i</td></tr>' % record_info['version']
                record_data += u'<tr><th>Tag</th><td>%i</td></tr>' % record_info['day']
                record_data += u'<tr><th>Monat</th><td>%s</td></tr>' % MDConst.MONTH_DICT_GE[record_info['month']]
                record_data += u'<tr><th>Jahr</th><td>%i</td></tr>' % record_info['year']
                record_data += u'<tr><th>Ort</th><td>%s</td></tr>' % record_info['church']
                record_data += u'<tr><th>Veranstaltung</th><td>%s</td></tr>' % record_info['event']
                record_data += u'<tr><th>Zusätzliche Informationen</th><td>%s</td></tr>' % record_info['info']
                if record_info['version']:
                    record_data += u'<tr><th>Reihenfolge</th><td>%s</td></tr>' % record_info['order']
                else:
                    record_data += u'<tr class="danger"><th>Reihenfolge</th><td>%s</td></tr>' % record_info['order']
                record_data += u'</tbody></table>'

        return html, path_above_list, record_data

    def getExplorerDropdown(self):
        html = u'''<li><p onclick="getContent('dir_Media')">Media</p></li>'''
        for i in self.master.dh.drives:
            html += u'''\n<li><p onclick="getContent('dir_%s')">%s</p></li>''' %(i.mountpoint, i)
        return html.replace('\\', '/')

    def getCompoundData(self):
        '''
        Rückgabe:
            [id, Belegung in %, freeSpace, totalSpace, Status, Fortschritt in % , x-te Datei von, X wird kopiert}
        '''
        compounds = []
        for i in self.master.dh.drives:
            jobs = self.master.jh.getJobsForDrive(i.id)
            progress = 1
            totalFiles = 0
            progess_percent = 0
            left_time = u'---'
            if jobs:
                progress, totalFiles = jobs[0].getProgress()
                progess_percent = jobs[0].getProgressSizePercent()
                left_time = jobs[0].getLeftTime()
            #for j in jobs:
            #    temp = j.getProgress()
            #    progress += temp[0]
            #    totalFiles += temp[1]
            if progress > totalFiles:
                progress = totalFiles
            compounds.append({u'id': i.id, u'ussage': i.getUsage(),
                              u'freeSpace': self.master.fh.getSpaceStr(i.freeSpace),
                              u'totalSpace': self.master.fh.getSpaceStr(i.totalSpace), u'state': i.state,
                              u'progress': progress, u'progess_percent': progess_percent, u'totalFiles': totalFiles,
                              u'name': unicode(i), u'left_time': left_time})
        return compounds

    def getRecord(self, date):
        data = list()
        records = self.master.rh.getRecordsByDate(date)
        for i in records:
            data.append({u'id': i.id, u'name': i.event, u'size': i.size})
        return data

    def getAllRecords(self):
        return self.master.rh.getAllRecords()

    def startRecordJob(self, drive_id, records):
        self.master.jh.addRecordJob(drive_id, records)

    def getAllMessages(self):
        return self.master.mh.getAllMessages()

    def checkRecordJob(self, driveId, recordsIdList):
        return self.master.jh.checkRecordJob(driveId, recordsIdList)

    def cancelRecordJob(self, drive_id):
        job = self.master.jh.getRecordJobByDriveId(drive_id)
        if not job is None:
            job.cancel()
            return True
        return False

    def checkDelete(self, drive_id):
        return self.master.dh.checkDelete(drive_id)

    def deleteDrive(self, drive_id):
        drive = self.master.dh.getDriveById(drive_id)
        if isinstance(drive, Drive):
            drive.start_delete_thread()

    def stopDeleteDrive(self, drive_id):
        drive = self.master.dh.getDriveById(drive_id)
        if not drive is None:
            drive.stop_deleting()

    def reloadDhSettings(self):
        self.master.dh.load_drivesettings()  # todo testen

    def reloadTimeSettings(self):
        self.master.master.th.reload_timer()

    def lock(self, ip):
        temp = self.master.sh.checkRight(ip, EncrConst.UNLOCK)
        if temp[0]:
            self.master.master.th.lock_manually()
        else:
            self.master.mh.addMessage(temp[1], level=MDConst.MESSAGE_WARNING, requested=False, ip=ip)

    def unlock(self, ip):
        temp =  self.master.sh.checkRight(ip, EncrConst.UNLOCK)
        if temp[0]:
            self.master.master.th.unlock()

    def isLocked(self):
        return self.master.master.th.is_locked

    def reloadRecords(self):
        self.master.rh.request_scan = True

    def getAllInfos(self):
        return self.master.master.i.get_all_infos()

    def set_auto_shutdown(self, ip):
        if self.master.sh.checkRight(ip, EncrConst.SHUTDOWN)[0]:
            self.master.master.th.set_auto_shutdown()
            return True
        else:
            return False

    def unset_auto_shutdown(self, ip):
        if self.master.sh.checkRight(ip, EncrConst.SHUTDOWN)[0]:
            self.master.master.th.unset_auto_shutdown()
            return True
        else:
            return False

    def set_logger_output(self, output, ip):
        if self.master.sh.checkRight(ip, EncrConst.LOGGERMODE)[0]:
            self.master.master.i.write_info(u'Logger Mode', MDConst.LOGGERDICT[output])
            self.master.l.setOutput(output)
            return True
        else:
            return False

    def logOn(self, ip, name, password):
        return self.master.sh.logOn(ip, name, password)

    def logOff(self, ip):
        self.master.sh.logOffByIp(ip)

    def checkSession(self, ip):
        session = self.master.sh.getSessionByIp(ip)
        if session:
            return session.name
        else:
            return u''

    def quit(self, ip):
        temp = self.master.sh.checkRight(ip, EncrConst.QUIT)
        if temp[0]:
            self.master.master.th.request_quit()
        else:
            self.master.mh.addMessage(temp[1], level=MDConst.MESSAGE_WARNING, requested=False, ip=ip)

    def shutdown(self, ip):
        temp = self.master.sh.checkRight(ip, EncrConst.SHUTDOWN)
        if temp[0]:
            self.master.master.th.request_shutdown()
        else:
            self.master.mh.addMessage(temp[1], level=MDConst.MESSAGE_WARNING, requested=False, ip=ip)

    def get_state_html(self):
        state = self.master.master.th.state
        if state in (0, 1, 2, 5):
            return u''
        elif state == 3:
            return u'<div class="panel panel-danger"><div class="panel-heading"><h3 class="panel-title">Hinweis</h3>' \
                   u'</div><div class="panel-body">Der Rechner wird nach Beendigung aller Aufträge heruntergefahren.' \
                   u'<br><button type="button" onclick="send_ajax_request(\'cancel_sd\')" class="btn btn-default">' \
                   u'Abbrechen</button></div></div>'
        elif state == 7:
            return u'<div class="panel panel-danger"><div class="panel-heading"><h3 class="panel-title">Hinweis</h3>' \
                   u'</div><div class="panel-body">Der Rechner wird gleich heruntergefahren.' \
                   u'</div></div>'
        elif state == 8 :
            return u'<div class="panel panel-danger"><div class="panel-heading"><h3 class="panel-title">Hinweis</h3>' \
                   u'</div><div class="panel-body">Das Programm wird nach Beendigung aller Aufträge beendet.<br>' \
                   u'<button type="button" onclick="send_ajax_request(\'cancel_quit\')" class="btn btn-default">' \
                   u'Abbrechen</button></div></div>'
        elif state == 9:
            return u'<div class="panel panel-danger"><div class="panel-heading"><h3 class="panel-title">Hinweis</h3>' \
                   u'</div><div class="panel-body">Das Programm wird gleich beendet.' \
                   u'</div></div>'
        elif state == 6:
            return u'<div class="panel panel-danger"><div class="panel-heading"><h3 class="panel-title">Hinweis</h3>' \
                   u'</div><div class="panel-body">Der Rechner wird in %s heruntergefahren.<br><button type="button" ' \
                   u'onclick="send_ajax_request(\'cancel_sd_start\')" class="btn btn-default">Abbrechen' \
                   u'</button></div></div>' % self.master.master.th.get_left_time()
        elif state == 4:
            return u'<div class="panel panel-danger"><div class="panel-heading"><h3 class="panel-title">Hinweis</h3>' \
                   u'</div><div class="panel-body">Der Rechner wird in %s heruntergefahren.<br><button type="button" ' \
                   u'onclick="send_ajax_request(\'cancel_sd_timer\')" class="btn btn-default">Verschieben</button>' \
                   u'</div></div>' % self.master.master.th.get_left_time()
        return u'<div class="panel panel-danger"><div class="panel-heading"><h3 class="panel-title">Hinweis</h3>' \
               u'</div><div class="panel-body">Unbekannter Zustand.</div></div>'

    def cancel_sd_timer(self):
        self.master.master.th.set_run_timer()

    def cancel_sd_start(self):
        self.master.master.th.cancel_sd_start()

    def get_fsm_state(self):
        return self.master.master.th.state

    def cancel_sd(self):
        self.master.master.th.set_run_timer()

    def cancel_quit(self):
        self.master.master.th.set_run_timer()

    def set_run(self, ip):
        check = self.master.sh.checkRight(ip, EncrConst.MANIPULATE_FSM)
        if check[0]:
            self.master.master.th.set_run()
        else:
            self.master.mh.addMessage(check[1], level=MDConst.MESSAGE_WARNING, requested=False, ip=ip)

    def get_records_stat(self, ip):
        check = self.master.sh.checkRight(ip, EncrConst.STATISTICS)
        if check[0]:
            html = u'<table  class="table table-striped">'
            html += u'<tbody>'
            for i, j in self.master.stat.getStatCopyRecords(): # todo try/ catch
                html += u'<tr><td>%s</td><td>%i</td></tr>' % (i, j)
            html += u'</tbody></table>'
            return html
        else:
            return u'Sie haben keine Berechtigung diese Daten zu sehen: %s' % check[1]

    def get_records_stat_month_table(self, ip, month, year):
        check = self.master.sh.checkRight(ip, EncrConst.STATISTICS)
        if check[0]:
            data = self.master.stat.getStatCopyRecordsByDay()
            temp  = calendar.monthrange(year, month)
            month_range  = range(1, temp[1] + 1)
            daily_stats = list()
            html_body = ''
            for day in month_range:
                date = datetime.datetime(year, month, day, hour=12)
                delta = date - MDConst.EPOCH_TIME
                new_time = delta.total_seconds()
                html_body += '<h3>%s %s</h3>\n' % (day, MDConst.DAY_DICT_GE[calendar.weekday(year, month, day)])
                if data.has_key(new_time):
                    counter = 0
                    temp_html = 'Gesamt: %i<br>\n'
                    table_html = '<table class="table table-striped" style="width:100%">\n' \
                                 '<tr>\n<th>Anzahl</th>' \
                                 '<th>Ordner</th>\n</tr>'
                    for key, value in data[new_time].iteritems():
                        table_html += '<tr> <td>%i</td> <td>%s</td> </tr>' % (value, key)
                        counter += value
                    daily_stats.append(counter)
                    table_html += '</table>'
                    html_body += temp_html % counter + table_html
                else:
                    daily_stats.append(0)
                html_body += '<hr>\n'

            return html_body
        else:
            return u'Sie haben keine Berechtigung diese Daten zu sehen: %s' % check[1]

    def get_files_stat(self, ip):
        check = self.master.sh.checkRight(ip, EncrConst.STATISTICS)
        if check[0]:
            html = u'<table  class="table table-striped">'
            html += u'<tbody>'
            for i, j in self.master.stat.getStatCopyFiles(): # todo try/ catch
                html += u'<tr><td>%s</td><td>%i</td></tr>' % (i, j)
            html += u'</tbody></table>'
            return html
        else:
            return u'Sie haben keine Berechtigung diese Daten zu sehen: %s ' % check[1]

    def get_calendar_stat(self, ip):
        check = self.master.sh.checkRight(ip, EncrConst.STATISTICS)
        if check[0]:
            data = self.master.stat.getStatCopyRecordsByDay()
            new_data = []
            for key, value in data.iteritems():
                counter = 0
                for key2, value2 in value.iteritems():
                    counter += value2
                new_data.append([key*1000, counter])
            return check[0], new_data
        else:
            return check[0], u'Sie haben keine Berechtigung diese Daten zu sehen: %s ' % check[1]

    def getChurches(self):
        return self.master.rh.getChurches()

    def showNormal(self, ip):
        check = self.master.sh.checkRight(ip, EncrConst.SHOWNORMAL)
        if check[0]:
            self.master.master.b.b.showNormal()
        else:
            self.master.mh.addMessage(check[1], level=MDConst.MESSAGE_WARNING, requested=False, ip=ip)

    def showFullScreen(self):
        self.master.master.b.b.showFullScreen()

    def orderCD(self, record_date, record_name, name, church, format=None, comment=None, count=None):
        row_id = None
        try:
            row_id = self.master.coh.order(record_date, record_name, name, church, format, comment, count)
        except Exception as e:
            self.master.l.logEngine(u'Datenbankzugriff bei Anlegen von CD Bestellung fehlgeschlagen: %s' % e.message,
                                    logLevel=MDConst.LOG_ERROR)
            self.master.mh.addMessage(u'Datenbankzugriff bei Anlegen von CD Bestellung fehlgeschlagen: %s' % e.message,
                                      level=MDConst.MESSAGE_ERROR)
            self.master.mh.addMessage(u'Datenbankzugriff fehlgeschlagen. Weitere Details in der Log-Datei.',
                                      level=MDConst.MESSAGE_ERROR, requested=False)
        return row_id

    def getCDFormats(self):
        data = {}
        try:
            data = self.master.coh.get_formats()
        except Exception as e:
            self.master.l.logEngine(u'Datenbankzugriff bei Abruf von CD Formaten fehlgeschlagen: %s' % e.message,
                                    logLevel=MDConst.LOG_ERROR)
            self.master.mh.addMessage(u'Datenbankzugriff bei Abruf von CD Formaten fehlgeschlagen: %s' % e.message,
                                      level=MDConst.MESSAGE_ERROR)
            self.master.mh.addMessage(u'Datenbankzugriff fehlgeschlagen. Weitere Details in der Log-Datei.',
                                      level=MDConst.MESSAGE_ERROR, requested=False)
        return data

    def getCDOrderActivated(self):
        return self.master.coh.get_activated()

    def getCDOrders(self, state):
        data = {}
        try:
            data = self.master.coh.get_orders(state)
        except Exception as e:
            self.master.l.logEngine(u'Datenbankzugriff bei Abruf von CD Aufträgen fehlgeschlagen: %s' % e.message,
                                    logLevel=MDConst.LOG_ERROR)
            self.master.mh.addMessage(u'Datenbankzugriff bei Abruf von CD Aufträgen fehlgeschlagen: %s' % e.message,
                                      level=MDConst.MESSAGE_ERROR)
            self.master.mh.addMessage(u'Datenbankzugriff fehlgeschlagen. Weitere Details in der Log-Datei.',
                                      level=MDConst.MESSAGE_ERROR, requested=False)
        return data

    def getGeneralSettings(self):
        check = self.master.sh.checkRight('127.0.0.1', EncrConst.SETTINGS)
        if not check[0]:
            self.master.master.mh.addMessage(u'Ein Fehler ist auf getreten: ' + check[1] + u'.',
                                             level=MDConst.MESSAGE_ERROR,
                                             requested=False)
            return {}
        return self.master.master.sth.get_general_settings()

    def checkGeneralSettings(self, settings):
        check = self.master.sh.checkRight('127.0.0.1', EncrConst.SETTINGS)
        if not check[0]:
            self.master.master.mh.addMessage(u'Ein Fehler ist auf getreten: ' + check[1] + u'.',
                                             level=MDConst.MESSAGE_ERROR,
                                             requested=False)
            return {}, [check[1]]
        new_settings, messages = self.master.master.sth.check_general_settings(settings)
        if messages:
            html = u'<h4> Folgende Einstellungen sind fehlerhaft: </h4><ul>'  # todo
            for message in messages:
                html += u'<li>' + message + u'</li>'
            html += u'</ul>'
            self.master.mh.addMessage(html, level=MDConst.MESSAGE_WARNING, requested=False)
        else:
            self.master.mh.addMessage(u'Alles in Ordnung', level=MDConst.MESSAGE_INFO, requested=False)
        return new_settings, messages

    def getGeneralDefaultSettings(self):
        check = self.master.sh.checkRight('127.0.0.1', EncrConst.SETTINGS)
        if not check[0]:
            self.master.master.mh.addMessage(u'Ein Fehler ist auf getreten: ' + check[1] + u'.',
                                             level=MDConst.MESSAGE_ERROR,
                                             requested=False)
            return {}
        return DefaultSettings.general

    def setGeneralSettings(self, settings):
        check = self.master.sh.checkRight('127.0.0.1', EncrConst.SETTINGS)
        if not check[0]:
            self.master.master.mh.addMessage(u'Ein Fehler ist auf getreten: ' + check[1] + u'.',
                                             level=MDConst.MESSAGE_ERROR,
                                             requested=False)
            return {}, [check[1]]
        new_settings, messages = self.master.master.sth.check_general_settings(settings)
        self.master.master.sth.general_settings = new_settings
        self.master.master.sth.write_general_settings()
        MDConst.events.general_load_settings.emit()
        if messages:
            html = u'Einstellungen wurden korrigiert und gespeichert.'
            self.master.mh.addMessage(html, level=MDConst.MESSAGE_WARNING, requested=False)
        else:
            html = u'Einstellungen wurden gespeichert.'
            self.master.mh.addMessage(html, level=MDConst.MESSAGE_INFO, requested=False)
        return new_settings, messages

    def getExistingDir(self, default_dir=None):
        check = self.master.sh.checkRight('127.0.0.1', EncrConst.SETTINGS)
        if not check[0]:
            self.master.master.mh.addMessage(u'Ein Fehler ist auf getreten: ' + check[1] + u'.',
                                             level=MDConst.MESSAGE_ERROR,
                                             requested=False)
            return None
        return self.master.master.b.getExistingDir(default_dir)

    def getFileSettings(self):
        check = self.master.sh.checkRight('127.0.0.1', EncrConst.SETTINGS)
        if not check[0]:
            self.master.master.mh.addMessage(u'Ein Fehler ist auf getreten: ' + check[1] + u'.',
                                             level=MDConst.MESSAGE_ERROR,
                                             requested=False)
            return {}
        return self.master.master.sth.get_files_settings()

    def checkFileSettings(self, settings):
        check = self.master.sh.checkRight('127.0.0.1', EncrConst.SETTINGS)
        if not check[0]:
            self.master.master.mh.addMessage(u'Ein Fehler ist auf getreten: ' + check[1] + u'.',
                                             level=MDConst.MESSAGE_ERROR,
                                             requested=False)
            return {}, [check[1]]
        new_settings, messages =  self.master.master.sth.check_files_settings(settings)
        if messages:
            html = u'<h4> Folgende Einstellungen sind fehlerhaft: </h4><ul>'  # todo
            for message in messages:
                html += u'<li>' + message + u'</li>'
            html += u'</ul>'
            self.master.mh.addMessage(html, level=MDConst.MESSAGE_WARNING, requested=False)
        else:
            self.master.mh.addMessage(u'Alles in Ordnung', level=MDConst.MESSAGE_INFO, requested=False)
        return new_settings, messages

    def setFileSettings(self, settings):
        check = self.master.sh.checkRight('127.0.0.1', EncrConst.SETTINGS)
        if not check[0]:
            self.master.master.mh.addMessage(u'Ein Fehler ist auf getreten: ' + check[1] + u'.',
                                             level=MDConst.MESSAGE_ERROR,
                                             requested=False)
            return {}, [check[1]]
        new_settings, messages = self.master.master.sth.check_files_settings(settings)
        self.master.master.sth.files_settings = new_settings
        self.master.master.sth.write_files_settings()
        MDConst.events.files_load_settings.emit()
        if messages:
            html = u'Einstellungen wurden korrigiert und gespeichert.'
            self.master.mh.addMessage(html, level=MDConst.MESSAGE_WARNING, requested=False)
        else:
            html = u'Einstellungen wurden gespeichert.'
            self.master.mh.addMessage(html, level=MDConst.MESSAGE_INFO, requested=False)
        return new_settings, messages

    def getFileDefaultSettings(self):
        check = self.master.sh.checkRight('127.0.0.1', EncrConst.SETTINGS)
        if not check[0]:
            self.master.master.mh.addMessage(u'Ein Fehler ist auf getreten: ' + check[1] + u'.',
                                             level=MDConst.MESSAGE_ERROR,
                                             requested=False)
            return {}
        return DefaultSettings.files

    def getDbSettings(self):
        check = self.master.sh.checkRight('127.0.0.1', EncrConst.SETTINGS)
        if not check[0]:
            self.master.master.mh.addMessage(u'Ein Fehler ist auf getreten: ' + check[1] + u'.',
                                             level=MDConst.MESSAGE_ERROR,
                                             requested=False)
            return {}
        return self.master.master.sth.get_db_settings()

    def checkDbSettings(self, settings):
        check = self.master.sh.checkRight('127.0.0.1', EncrConst.SETTINGS)
        if not check[0]:
            self.master.master.mh.addMessage(u'Ein Fehler ist auf getreten: ' + check[1] + u'.',
                                             level=MDConst.MESSAGE_ERROR,
                                             requested=False)
            return {}, [check[1]]
        new_settings, messages = self.master.master.sth.check_db_settings(settings)
        if messages:
            html = u'<h4> Folgende Einstellungen sind fehlerhaft: </h4><ul>'  # todo
            for message in messages:
                html += u'<li>' + message + u'</li>'
            html += u'</ul>'
            self.master.mh.addMessage(html, level=MDConst.MESSAGE_WARNING, requested=False)
        else:
            self.master.mh.addMessage(u'Alles in Ordnung', level=MDConst.MESSAGE_INFO, requested=False)
        return new_settings, messages

    def setDbSettings(self, settings):
        check = self.master.sh.checkRight('127.0.0.1', EncrConst.SETTINGS)
        if not check[0]:
            self.master.master.mh.addMessage(u'Ein Fehler ist auf getreten: ' + check[1] + u'.',
                                             level=MDConst.MESSAGE_ERROR,
                                             requested=False)
            return {}, [check[1]]
        new_settings, messages = self.master.master.sth.check_db_settings(settings)
        self.master.master.sth.db_settings = new_settings
        self.master.master.sth.write_db_settings()
        MDConst.events.db_load_settings.emit()
        if messages:
            html = u'Einstellungen wurden korrigiert und gespeichert.'
            self.master.mh.addMessage(html, level=MDConst.MESSAGE_WARNING, requested=False)
        else:
            html = u'Einstellungen wurden gespeichert.'
            self.master.mh.addMessage(html, level=MDConst.MESSAGE_INFO, requested=False)
        return new_settings, messages

    def getDbDefaultSettings(self):
        check = self.master.sh.checkRight('127.0.0.1', EncrConst.SETTINGS)
        if not check[0]:
            self.master.master.mh.addMessage(u'Ein Fehler ist auf getreten: ' + check[1] + u'.',
                                             level=MDConst.MESSAGE_ERROR,
                                             requested=False)
            return {}
        return DefaultSettings.db

    def checkDbConnection(self, connection_settings):
        check = self.master.sh.checkRight('127.0.0.1', EncrConst.SETTINGS)
        if not check[0]:
            self.master.master.mh.addMessage(u'Ein Fehler ist auf getreten: ' + check[1] + u'.',
                                             level=MDConst.MESSAGE_ERROR,
                                             requested=False)
            return False
        new_settings, messages = self.master.master.sth.check_db_settings(connection_settings)
        if messages:
            html = u'<h4> Folgende Einstellungen sind fehlerhaft: </h4><ul>'  # todo
            for message in messages:
                html += u'<li>' + message + u'</li>'
            html += u'</ul>'
            self.master.mh.addMessage(html, level=MDConst.MESSAGE_WARNING, requested=False)
            return False
        else:
            return CDOrderHandler.check_db_connection(new_settings)

    def getDefaultChurch(self):
        return self.master.rh.get_default_church()

    def deleteStatistics(self):
        check = self.master.sh.checkRight('127.0.0.1', EncrConst.STATISTICS)
        if not check[0]:
            self.master.master.mh.addMessage(u'Ein Fehler ist auf getreten: ' + check[1] + u'.',
                                             level=MDConst.MESSAGE_ERROR,
                                             requested=False)
        error_list = self.master.stat.deleteStat()
        if error_list:
            self.master.master.mh.addMessage(u'Beim Löschen der Statistik traten %i Fehler auf. Für weitere '
                                             u'Informationen siehe Logfile.' % len(error_list),
                                             level=MDConst.MESSAGE_ERROR,
                                             requested=False)
        else:
            self.master.master.mh.addMessage(u'Statistik erfolgreich gelöscht.',
                                             level=MDConst.MESSAGE_INFO,
                                             requested=False)


class SessionHandler(object):
    maxId = 0
    def __init__(self, master):
        self.master = master
        self.l = master.l
        self.mh = master.mh
        self.pws = EncryptedPasswordSave(MDConst.SETTINGSPATH + u'/epws')
        self.sessions = list()
        self.sessionTimeOut = datetime.timedelta(seconds=MDConst.LOGON_TIMEOUT)
        if not MDConst.FROZEN:
            self.logOn('127.0.0.1', u'admin', u'admin')

    def __unicode__(self):
        return u'; '.join([unicode(i) for i in self.sessions])

    def getSessionByIp(self, ip):
        for i in self.sessions:
            if i.ip == ip:
                return i
        return None

    def checkSessionStart(self, ip, name, password):
        if self.getSessionByIp(ip):
            return False, u'Benutzer von diesem Rechner ist schon eingeloggt'
        check = self.pws.getUserData(name, password)
        #check = self.pws.check_pw(name, password)
        if check[0]:
            return True, check[1]
        else:
            return False, check[1]

    def logOn(self, ip, name, password):
        #name = self.master.fh.decodeURL(name)
        #password = self.master.fh.decodeURL(password)
        check = self.checkSessionStart(ip, name, password)
        if check[0]:
            self.sessions.append(Session(SessionHandler.maxId, ip, name, check[1][0], check[1][1]))
            self.l.logSession(u'Logon: id: %i, name: %s, ip: %s' % (SessionHandler.maxId, name, ip),
                              logLevel=MDConst.LOG_INFO)
            self.mh.addMessage(u'%s hat sich eingeloggt' % name, level=MDConst.MESSAGE_INFO, ip=ip)
            SessionHandler.maxId += 1
            return True, u'Benutzer wurde eingeloggt'
        else:
            self.l.logSession(u'Fehlgeschlagener Anmedeversuch. Name: %s, IP: %s' % (name, ip),
                              logLevel=MDConst.LOG_ERROR)
            self.mh.addMessage(u'Fehlgeschlagener Anmedeversuch.', level=MDConst.MESSAGE_ERROR, ip=ip)
            return False, check[1]

    def logOffByIp(self, ip):
        session = self.getSessionByIp(ip)
        if session:
            self.mh.addMessage(u'%s hat sich ausgeloggt' % session.name, level=MDConst.MESSAGE_INFO, ip=ip)
            self.logOff(session)

    def logOff(self, session):
        self.l.logSession(u'Logout: id: %i, name: %s, ip: %s' % (session.id, session.name, session.ip),
                          logLevel=MDConst.LOG_INFO)
        self.sessions.remove(session)
        del session

    def checkRight(self, ip, right):
        session = self.getSessionByIp(ip)
        if not session:
            return False, u'Kein Benutzer ist eingeloggt'
        else:
            if right >= len(session.rights):
                return False, u'Das angeforderte Recht existiert nicht'
            elif session.rights[right] == '1':
                session.action()
                return True, u'Die Aktion darf ausgeführt werden'
            else:
                return False, u'Der Benutzer besitzt nicht das angeforderte Recht'

    def update(self):
        for i in self.sessions[:]:
            if datetime.datetime.now() - i.lastActionTime > self.sessionTimeOut:
                self.mh.addMessage(u'%s wurde wegen Zeitüberschreitung ausgeloggt.' % i.name,
                                   level=MDConst.MESSAGE_INFO, ip=i.ip)
                self.logOff(i)
        self.master.master.i.write_info(u'Sessions', unicode(self))


class Session(object):
    def __init__(self, id, ip, name, rights, users_data):
        self.id = id
        self.ip = ip
        self.name = name
        self.rights = rights
        self.users_data = users_data
        self.logOnTime = datetime.datetime.now()
        self.lastActionTime = datetime.datetime.now()

    def __unicode__(self):
        return u'Session id: %i, ip: %s, name: %s' % (self.id, self.ip, self.name)

    def action(self):
        self.lastActionTime = datetime.datetime.now()


class EncryptedPasswordSave(object):
    def __init__(self, filename):
        self.filename = filename
        if not os.path.isfile(self.filename):
            self.create_epws()
        #encr_file = open(filename, 'a')

        self.__alphabet = [u'a', u'b', u'c', u'd', u'e', u'f', u'g', u'h', u'i', u'j', u'k', u'l', u'm', u'n', u'o',
                           u'p', u'q', u'r', u's', u't', u'u', u'v', u'w', u'x', u'y', u'z', u'A', u'B', u'C', u'D',
                           u'E', u'F', u'G', u'H', u'I', u'J', u'K', u'L', u'M', u'N', u'O', u'P', u'Q', u'R', u'S',
                           u'T', u'U', u'V', u'W', u'X', u'Y', u'Z', u'ä', u'ö', u'ü', u'ß', u'Ä', u'Ö', u'Ü', u':',
                           u' ', u'1', u'2', u'3', u'4', u'5', u'6', u'7', u'8', u'9', u'0']

        self.__master_name = u'admin'
        self.__master_encr_pw = \
                                "scrypt\x00\x0e\x00\x00\x00\x08\x00\x00\x00\x01nS\x94\xf4Rs\x18W\xef'\rl\xcb\x83\x11" \
                                "\x9a\xb4\x95\xbd\xb2\x91l\x9c\x8b\xf2mX\x87\x00o\x94 &E\x95\x9c\xc5\x8e\xbdDS\x88" \
                                "\xc1\xab\x05\x81\xbe\x19\xd7\x8f\x8d\xca\x85\xedP\x85Dn\x9fKa%\xb7\x9d\x80\x8b\x84" \
                                "\xa2-\x9f'\xe4S\xf3w\xa9*\xb6\xa4\x91K\x06\xdem\xea]N\xfde3\x03\xf6\xea)\x83f\xe1#eF" \
                                "\xcb\x04i\xf8\xf1~\xe4\xa1\x9e\xab\xea\x12PP\xbd?\xa9\x1b\r\x98\xcb)j\xa9\x89::\x94`" \
                                "\xf6\xc5\rq\t\x1f\xcc\xe3\xed\x0b\xaf\xaa5{~@\x19x\xecf\xba\xf9H\xafm-\xf3\xcdA1q" \
                                "\x93\xd1\xa3\x87\x1b\x06\x16o.\xae\n\x08\xab\xd6\x02\x99\xd4\xe3\x9b\x0bm\xc7\xc6b" \
                                "\xce\x85\xa6"
        '''
        self.__master_encr_pw = \
                              "scrypt\x00\x0e\x00\x00\x00\x08\x00\x00\x00\x015\x88qk\x02\x07X\x01\xe5\x05\xb2\xf8\xac" \
                              "\x8e\xab\xfc`f\x12\x8c\nX\x9a\xae1\x92\x05\xff\xfe\xc2\xe9\xe6\x00\x0c\x96\xbc\x18'" \
                              "\xd24q)\\\x92\xd8-\xb3~\xf7\xf0\x90m<\xce\xbd\xff\x83\x1a\x0209\x81w#\x8c\xe9\x7fI8" \
                              "\xea\x8a\xcb\xe1\xce\xba lq\xf0\x1a\xba+\xc3-X\xdb5\x03\xa1\xaa\xae\x8b\xed\xbf\x14}#" \
                              "\x05\x98Q\xb8\xc1'{2L\xfd5}\xaeh\xb1\xfc\x0f4\x10!~\x89\xd4J(\x0eF\xeeDt\x91X#\x89\x99" \
                              "\xa0|\xe2LF\xd2O\x95\xe9\x1e\xcc/\xc9\xdc\xc3\x12\xc0R\xbd,$\xcb\x04\xb1\xd5\xd91\xbe" \
                              "\x87\x82^=\x85\xe4o-\xd9\xdf\xe5Plk\x9b\x0bx\x19\xc3\xda\xb3\x1fs|\xf9"
        '''

    def create_epws(self):
        data = {'user':{}, 'rights': ''}  # todo create admin password
        try:
            basepath = os.path.basename(self.filename)
            if not os.path.isdir(basepath):
                os.makedirs(basepath)
            epws_file = open(self.filename, 'w')
        except:
            print "can't create epws"
            pass # todo message
        else:
            pickle.dump(data, epws_file)
            epws_file.close()

    def check_letters(self, text):
        for i in text:
            if i not in self.__alphabet: # or i == '\n' or i == ':':
                return False
        return True

    def __readEPWSFileContent(self):
        epws_file = open(self.filename, 'r')
        data = pickle.loads(epws_file)
        epws_file.close()
        if not isinstance(data, dict):
            raise ValueError(u'Invalid file content.')
        if not data.has_key('users'):
            raise ValueError(u'Invalid file content.')
        if not isinstance(data['users'], dict):
            raise ValueError(u'Invalid file content.')
        for i in data['users'].values():
            if not isinstance(i, str):
                raise ValueError(u'Invalid file content.')
        if not data.has_key('rights'):
            raise ValueError(u'Invalid file content.')
        if not isinstance(data['rights'], str):
            raise ValueError(u'Invalid file content.')
        return data

    def __writeEPWSFileContent(self, data):
        if not isinstance(data, dict):
            raise ValueError(u'Invalid file content.')
        if not data.has_key('users'):
            raise ValueError(u'Invalid file content.')
        if not isinstance(data['users'], dict):
            raise ValueError(u'Invalid file content.')
        for i in data['users'].values():
            if not isinstance(i, str):
                raise ValueError(u'Invalid file content.')
        if not data.has_key('rights'):
            raise ValueError(u'Invalid file content.')
        if not isinstance(data['rights'], str):
            raise ValueError(u'Invalid file content.')
        epws_file = open(self.filename, 'w')
        pickle.dump(data, epws_file)
        epws_file.close()

    def getUserData(self, name, password):
        if not isinstance(name, unicode):
            raise Exception('Internal Error')
        if not isinstance(password, unicode):
            raise Exception('Internal Error')
        if name == self.__master_name:
            try:
                right_pw = scrypt.decrypt(self.__master_encr_pw, password.encode('utf-8'))
            except:
                return False, u'Falsches Passwort oder falscher Benutzername.'
            else:
                try:
                    data = self.__readEPWSFileContent()
                except:
                    # todo message
                    return True, (EncrConst.MASTER_RIGHTS, {u'master': EncrConst.MASTER_RIGHTS})
                else:
                    users = {}
                    all_rights = scrypt.decrypt(data['rights'], right_pw).decode('utf-8')
                    for i in all_rights.split('++'):
                        for j in i.split('+'):
                            if len(j) == 2:
                                users[j[0]] = j[1]
                    users[self.__master_name] = EncrConst.MASTER_RIGHTS
                    return True, (EncrConst.MASTER_RIGHTS, users)
        else:
            try:
                data = self.__readEPWSFileContent()
            except:
                return False, u'Interner Fehler.'
            else:
                if not name in data['users'].keys():
                    return False, u'Falsches Passwort oder falscher Benutzername.'
                else:
                    try:
                        right_pw = scrypt.decrypt(self.__master_encr_pw, password.encode('utf-8'))
                    except:
                        return False, u'Falsches Passwort oder falscher Benutzername.'
                    else:
                        users = {}
                        all_rights = scrypt.decrypt(data['rights'], right_pw).decode('utf-8')
                        for i in all_rights.split('++'):
                            for j in i.split('+'):
                                if len(j) == 2:
                                    users[j[0]] = j[1]
                        #users[u'master'] = EncrConst.MASTER_RIGHTS
                        if not users.has_key(name):
                            return False, u'Interner Fehler.'
                        else:
                            try:
                                temp = bin(int(users[name]))
                                temp = temp[2:]
                                temp = '%16s' % temp
                                rights = temp.replace(' ', '0')
                            except:
                                return False, u'Interner Fehler.'
                            else:
                                return_users_data = {}
                                if rights[EncrConst.VIEW] == u'1':
                                    for key in users.keys():
                                        return_users_data[key] = None
                                if rights[EncrConst.ROOT] == u'1':
                                    return_users_data = users
                                return True, (rights, return_users_data)

    def check_pw_fitness(self, pw):
        if not isinstance(pw, (unicode,)):
            return False, u'Passwort muss vom Typ unicode sein.'
        if len(pw) < 8:
            return False, u'Passwort ist zu kurz. Es muss mindestens 8 Zeichen haben.'
        if len(pw) > 20:
            return False, u'Passwort ist zu Lang. Es darf maximal 20 Zeichen lang sein.'
        if not self.check_letters(pw):
            return False, u'Es befinden sich unzulässige Zeichen im Passwort'
        # todo weitere checks

        return True, u'Passwort ist in Ordnung.'

    def check_name_fitness(self, name):
        if not isinstance(name, (unicode,)):
            return False, u'Name muss unicode oder str sein.'
        if len(name) < 4:
            return False, u'Name ist zu kurz. Er muss mindestens 8 Zeichen haben.'
        if len(name) > 20:
            return False, u'Name ist zu Lang. Er darf maximal 20 Zeichen lang sein.'
        if not self.check_letters(name):
            return False, u'Es befinden sich unzulässige Zeichen im Namen'
        # todo weitere checks

        try:
            data = self.__readEPWSFileContent()
        except:
            return False, u'Interner Fehler.'
        else:
            if name in data['users'].keys() or name == self.__master_name:
                return False, u'Name bereits vorhanden.'

        return True, u'Name ist in Ordnung.'

    def addUser(self, name, password,  new_name, new_password1, new_password2, new_rights):
        return
        # check data types
        if not isinstance(name, unicode):
            raise Exception('Internal Error')
        if not isinstance(password, unicode):
            raise Exception('Internal Error')
        if not isinstance(new_name, unicode):
            raise Exception('Internal Error')
        if not isinstance(new_password1, unicode):
            raise Exception('Internal Error')
        if not isinstance(new_password2, unicode):
            raise Exception('Internal Error')
        if not isinstance(new_rights, basestring):
            raise Exception('Internal Error')
        if not len(new_rights) == 16:
            raise Exception('Internal Error')
        for i in new_rights:
            if i not in ('0', '1', u'0', u'1'):
                raise Exception('Internal Error')

        # check new user data
        if not new_password1 == new_password2:
            return False, u'Passwöter sind nicht gleich'
        check = self.check_name_fitness(new_name)
        if not check[0]:
            return check
        check = self.check_pw_fitness(new_password1)
        if not check[0]:
            return check

        user_data = self.getUserData(name, password)
        rights = user_data[1][0]
        if not rights[EncrConst.ROOT] == 1:
            return False, u'Keine Berechtigung'
        # todo from here taken from getUserData
        if name == self.__master_name:
            try:
                right_pw = scrypt.decrypt(self.__master_encr_pw, password.encode('utf-8'))
            except:
                return False, u'Falsches Passwort oder falscher Benutzername.'
            else:
                try:
                    data = self.__readEPWSFileContent()
                except:
                    # todo message
                    return True, (EncrConst.MASTER_RIGHTS, {u'master': EncrConst.MASTER_RIGHTS})
                else:
                    users = {}
                    all_rights = scrypt.decrypt(data['rights'], right_pw).decode('utf-8')
                    for i in all_rights.split('++'):
                        for j in i.split('+'):
                            if len(j) == 2:
                                users[j[0]] = j[1]


        else:
            try:
                data = self.__readEPWSFileContent()
            except:
                return False, u'Interner Fehler.'
            else:
                if not name in data['users'].keys():
                    return False, u'Falsches Passwort oder falscher Benutzername.'
                else:
                    try:
                        right_pw = scrypt.decrypt(self.__master_encr_pw, password.encode('utf-8'))
                    except:
                        return False, u'Falsches Passwort oder falscher Benutzername.'
                    else:
                        users = {}
                        all_rights = scrypt.decrypt(data['rights'], right_pw).decode('utf-8')
                        for i in all_rights.split('++'):
                            for j in i.split('+'):
                                if len(j) == 2:
                                    users[j[0]] = j[1]
                        #users[u'master'] = EncrConst.MASTER_RIGHTS
                        if not users.has_key(name):
                            return False, u'Interner Fehler.'
                        else:
                            try:
                                temp = bin(int(users[name]))
                                temp = temp[2:]
                                temp = '%16s' % temp
                                rights = temp.replace(' ', '0')
                            except:
                                return False, u'Interner Fehler.'
                            else:
                                return_users_data = {}
                                if rights[EncrConst.VIEW] == u'1':
                                    for key in users.keys():
                                        return_users_data[key] = None
                                if rights[EncrConst.ROOT] == u'1':
                                    return_users_data = users
                                return True, (rights, return_users_data)


        raise  Exception('Not implemented') # todo

    def changePw(self, name, password, new_password1, new_password2):
        raise  Exception('Not implemented') # todo

    def deleteUser(self, name, password, delete_name, delete_password):
        raise  Exception('Not implemented') # todo
