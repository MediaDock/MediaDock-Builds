# -*- coding: utf-8 -*-

import random
import scrypt
import codecs

a = [u'a', u'b', u'c', u'd', u'e', u'f', u'g', u'h', u'i', u'j', u'k', u'l', u'm', u'n', u'o',
                           u'p', u'q', u'r', u's', u't', u'u', u'v', u'w', u'x', u'y', u'z', u'A', u'B', u'C', u'D',
                           u'E', u'F', u'G', u'H', u'I', u'J', u'K', u'L', u'M', u'N', u'O', u'P', u'Q', u'R', u'S',
                           u'T', u'U', u'V', u'W', u'X', u'Y', u'Z', u'ä', u'ö', u'ü', u'ß', u'Ä', u'Ö', u'Ü', u':',
                           u' ', u'1', u'2', u'3', u'4', u'5', u'6', u'7', u'8', u'9', u'0']

b = u''
for i in range(64):
    b += random.choice(a)

print 'b',b
c = scrypt.encrypt(b.encode('utf-8'), u'admin'.encode('utf-8'), maxtime=0.1)
#c = scrypt.encrypt(b.encode('utf-8'), u'admin'.encode('utf-8'), maxtime=0.1)
print type(c)
#file = codecs.open('C:\Users\Peter\Desktop\secret.txt', 'w', 'utf-8')
#file.write(c.decode('utf-8'))
#file.close()
print len(c)
print c
print 'c ', c
print '\n\n'
print repr(c)
e = scrypt.decrypt(c, u'admin'.encode('utf-8'))
print 'e ', e
#print e == unicode(b)
f = e.decode('utf-8')
print f == b

