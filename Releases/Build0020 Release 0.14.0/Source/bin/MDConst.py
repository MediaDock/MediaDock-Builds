# -*- coding: utf-8 -*-

__author__ = 'Peter'

import os
import sys
from datetime import datetime
import OsInterface
import PySide.QtCore as QtCore

MONTH_DICT_GE = {1: u'Januar',
                 2: u'Februar',
                 3: u'März',
                 4: u'April',
                 5: u'Mai',
                 6: u'Juni',
                 7: u'Juli',
                 8: u'August',
                 9: u'September',
                 10: u'Oktober',
                 11: u'November',
                 12: u'Dezember'
                 }

DAY_DICT_GE = {0: u'Montag',
               1: u'Dienstag',
               2: u'Mittwoch',
               3: u'Donnerstag',
               4: u'Freitag',
               5: u'Samstag',
               6: u'Sonntag'
               }

LOCALAPPDATAPATH = OsInterface.getLocalAppDataPath()
LOGPATH = os.path.join(LOCALAPPDATAPATH, u'log')
SETTINGSPATH = os.path.join(LOCALAPPDATAPATH, u'settings')
STATPATH = os.path.join(LOCALAPPDATAPATH, u'stat')
COLLECTIONSDIR = u'MediaDockCollections'

GENERAL_JSON = u'general.json'
TIMER_JSON = u'timer.json'
FILES_JSON = u'files.json'
DB_JSON = u'db.json'

# For Logger
LOGGERFILE = 1
LOGGERSTDOUT = 2
LOGGERFILESTDOUT = 0
LOGGERDICT = {LOGGERFILESTDOUT: u'File and Stdout', LOGGERFILE: u'File', LOGGERSTDOUT: u'Stdout'}
LOG_DEBUG = 0
LOG_INFO = 1
LOG_WARNING = 2
LOG_ERROR = 3

# for copy files
COPY_MODE = 0

LOGON_TIMEOUT = 300 # in seconds

# for Messages
MESSAGE_DEBUG = 0
MESSAGE_INFO = 1
MESSAGE_WARNING = 2
MESSAGE_ERROR = 3

# Engine update cycles
UPDATE_TIME_DH = 5
UPDATE_TIME_JH = 5
UPDATE_TIME_MH = 5
UPDATE_TIME_RH = 3000
UPDATE_TIME_SH = 5
UPDATE_TIME_STAT = 600

if getattr(sys, 'frozen', False):
    FROZEN = True
else :
    FROZEN = False

EPOCH_TIME = datetime.utcfromtimestamp(0)

class DRIVE(object):
    STATE_DICT = {-1: u'error', 0: u'idle', 1: u'record job runs', 2: u'file job runs', 3: u'deletes', 4: u'ejecting'}
    ERROR = -1
    IDLE = 0
    RECORD_JOB_RUNS = 1
    FILE_JOB_RUNS = 2
    DELETES = 3
    EJECTING = 4


class Events(QtCore.QObject):
    general_load_settings = QtCore.Signal()
    files_load_settings = QtCore.Signal()
    timer_load_settings = QtCore.Signal()
    db_load_settings = QtCore.Signal()

events = Events()
