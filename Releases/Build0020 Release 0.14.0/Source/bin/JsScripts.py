__author__ = 'Peter'

import codecs

init_ask_password = ''
init_calendar = '''getCompounds();
updateCalendar();
showDefaultChurch();
setInterval(getCompounds, 1000);
//setInterval(updateCalendar, 3000);'''
init_check_locked = '''checkLock();
setInterval(checkLock, 3000);'''
init_drives = '''get_drives();
get_volume_messages();
setInterval(get_drives, 3000);
setInterval(get_volume_messages, 1000);'''
init_explorer = '''makeDropdown();
getContent('dir_Media');
setInterval(makeDropdown, 2000);'''
init_fsm = '''check_fsm_graph();
setInterval(check_fsm_graph, 1000);'''
init_getJson = ''
init_info = '''getAllInfos();
setInterval(getAllInfos, 3000);'''
init_lock = '''getCompounds();
checkLock();
setInterval(checkLock, 3000);
setInterval(getCompounds, 3000);
setInterval(getEngineRun, 3000); // todo was macht das hier???'''
init_messages = '''getAllMessages();
setInterval(getAllMessages, 3000);'''
init_modalMessages = '''setInterval(getModalMessages, 1000);'''
init_service = ''
init_session = '''checkSession();
setInterval(checkSession, 1000);
checkState();
setInterval(checkState, 1000);'''
init_settings = 'load_all_settings();'
init_sidebar_data = '''get_volumes();
get_jobs();
get_messages();
setInterval(get_volumes, 3000);
setInterval(get_jobs, 3000);
setInterval(get_messages, 3000);'''
init_statistics = '''getStats();
calendar_set_today();'''
init_cd_orders = '''
checkCDOrdersActivated();
'''
init_cd_orders_state = '''
insertAllCDOrders();
'''

about_init = '\n'.join([init_sidebar_data, init_modalMessages, init_session, init_cd_orders])
collections_init = '\n'.join([init_modalMessages, init_check_locked, init_calendar, init_session, init_cd_orders])
default_init = '\n'.join([init_sidebar_data, init_modalMessages, init_session, init_cd_orders])
drives_init = '\n'.join([init_check_locked, init_sidebar_data, init_drives, init_modalMessages, init_session,
                         init_cd_orders])
explorer_init = '\n'.join([init_check_locked, init_sidebar_data, init_explorer, init_modalMessages, init_session,
                           init_cd_orders])
fsm_init = '\n'.join([init_sidebar_data, init_fsm, init_modalMessages, init_session, init_cd_orders])
index_init = '''
getModalMessages();
setInterval(getModalMessages, 1000);
checkLock();
setInterval(checkLock, 3000);
updateCalendar();
setInterval(updateCalendar, 3000);
getCompounds();
setInterval(getCompounds, 1000);
checkSession();
setInterval(checkSession, 1000);
checkState();
setInterval(checkState, 1000);
showDefaultChurch();
'''
index_init += init_cd_orders
info_init = '\n'.join([init_sidebar_data, init_info, init_modalMessages, init_session, init_cd_orders])
license_init = '\n'.join([init_sidebar_data, init_modalMessages, init_session, init_cd_orders])
lock_init = '\n'.join([init_modalMessages, init_service, init_lock, init_ask_password, init_session, init_cd_orders])
menu_init = '\n'.join([init_check_locked, init_sidebar_data, init_modalMessages, init_session, init_cd_orders])
messages_init = '\n'.join([init_check_locked, init_sidebar_data, init_messages, init_modalMessages, init_session,
                           init_cd_orders])
new_job_init = '\n'.join([init_check_locked, init_sidebar_data, init_explorer, init_modalMessages, init_session,
                          init_cd_orders])
search_init = '\n'.join([init_modalMessages, init_check_locked, init_calendar, init_session, init_cd_orders])
service_init = '\n'.join([init_sidebar_data, init_service, init_ask_password, init_modalMessages, init_session,
                          init_cd_orders])
settings_init = '\n'.join([init_sidebar_data, init_modalMessages, init_session, init_settings, init_cd_orders])
statistics_init = '\n'.join([init_sidebar_data, init_modalMessages, init_session, init_settings, init_statistics,
                             init_cd_orders])
terms_or_use_init = '\n'.join([init_sidebar_data, init_modalMessages, init_session, init_cd_orders])
cd_orders_init = '\n'.join([init_check_locked, init_sidebar_data, init_modalMessages, init_session, init_cd_orders,
                            init_cd_orders_state])


def load_js_file(name):
    js_file = codecs.open('static/%s.js' % name, 'r', 'utf-8')
    content = js_file.read()
    js_file.close()
    return content

ask_password = load_js_file('ask_password')
calendar = load_js_file('calendar')
check_locked = load_js_file('check_locked')
drives = load_js_file('drives')
explorer = load_js_file('explorer')
fsm = load_js_file('fsm')
MediaDockLibrary = load_js_file('MediaDockLibrary')
info = load_js_file('info')
lock = load_js_file('lock')
messages = load_js_file('messages')
modalMessages = load_js_file('modalMessages')
service = load_js_file('service')
session = load_js_file('session')
settings = load_js_file('settings')
sidebar_data = load_js_file('sidebar_data')
statistics = load_js_file('statistics')
cd_orders = load_js_file('cd_orders')
