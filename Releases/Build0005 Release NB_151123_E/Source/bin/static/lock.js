function getCompounds(){
    data = getJsonData({'function': 'getCompoundData'}, true);
    insertCompounds(data.info);
}

function insertCompounds(data){
    var compoundElem = document.getElementById('compound-elements');
    var html = '';
    for (var i in data){
        html += '<div class="compound-element">' + data[i].name +
                '<br><div class="progress"><div class="progress-bar" role="progressbar" style="width:' + data[i].ussage
                + ';"><div style="color: black;">Speicherplatz</div></div></div>' + data[i].freeSpace + ' frei von ' + data[i].totalSpace + '<br>';
        switch(data[i].state){
            case -1:
                html += 'Schwerwiegender Fehler';
                break;
            case 0:
                html += '<div class="btn-group-vertical">  <button type="button" class="btn btn-default" onclick="eject_drive('
                        + data[i].id + ')">  <span class="glyphicon glyphicon-eject">  </span>  Auswerfen  </button>  </div>';
                break;
            case 1:
                html += '<div class="progress"> <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" style="width: '
                        + data[i].progess_percent + '%;min-width: 2em;"> '
                        + data[i].progess_percent + '% </div>  </div>  Datei '
                        + (parseInt(data[i].progress) + 1) + ' von '
                        + data[i].totalFiles + ' <br>  Vorraussichtliche Restdauer: '
                        + data[i].left_time + ' <br> <button type="button" class="btn btn-default" onclick="cancelRecordJob(' +
                        + data[i].id + ')">  <span class="glyphicon glyphicon-remove"></span>  Abbrechen  </button>';
                break;
            case 2:
                break;
            case 3:
                html += 'Laufwerk wird gelöscht <br>  <button type="button" class="btn btn-default" onclick="stopDeleteDrive(' +
                        + data[i].id + ')">  <span class="glyphicon glyphicon-remove"></span>  Abbrechen  </button>';
                break;
        }
        html += '</div>';
    }
    compoundElem.innerHTML = html;
}

function eject_drive(id) {
    getJsonData({'function': "eject_drive_" + id});
}

function cancelRecordJob(drive_id){
    getJsonData({'function': 'cancelRecordJob', 'drive_id': drive_id});
}

function stopDeleteDrive(drive_id){
    getJsonData({'function': 'stopDeleteDrive', 'drive_id': drive_id});
}

function checkLock(){
    data = getJsonData({'function': 'isLocked'}, true);
    if (! data.info){
        pserver.send('loadPage', 'index');
    }
}

getCompounds();
checkLock();
setInterval(checkLock, 3000);
setInterval(getCompounds, 3000);
setInterval(getEngineRun, 3000); // todo was macht das hier???
