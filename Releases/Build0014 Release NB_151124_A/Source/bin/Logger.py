# coding=utf-8

from datetime import datetime as dt
from datetime import timedelta as td
import codecs
import os
import re

import DefaultSettings
import MDConst

class Logger:
    logLevelStr = {0: u'Debug', 1: u'Info', 2: u'Warning', 3: u'Error'}

    def __init__(self, master, log_path):
        self.master = master
        self.log_path = log_path
        self.logLevel = DefaultSettings.general[u'logger'][u'level']
        self.clean = DefaultSettings.general[u'logger'][u'clean']

    def load_settings(self):
        settings = self.master.sth.get_logger_settings()
        self.logLevel = settings[u'level']
        self.clean = settings[u'clean']

    def cleanup(self):
        if self.clean:
            now = dt.now()
            errors = 0
            deleted = 0
            for i in os.listdir(self.log_path):
                if re.match(r'.*\-\d\d\d\d-\d\d-\d\d[(\.log)|(\-\b*\.json)]',i):
                    try:
                        name_list = i.rstrip('.log').split('-')
                        date = dt(int(name_list[1]), int(name_list[2]), int(name_list[3]))
                        delta = now - date
                        if delta.days > 31:
                            #print 'remove', os.path.join(self.log_path, i)
                            os.remove(os.path.join(self.log_path, i))
                            deleted += 1
                    except:
                        errors += 1
            if errors > 0:
                log_level = 2
            else:
                log_level = 1
            self.logLogger('Cleaned log files with %i errors. %i files deleted.' % (errors, deleted),
                           logLevel=log_level)

    def log(self, name, message, logLevel):
        #if logLevel not in Logger.logLevelStr.items:
        if not Logger.logLevelStr.has_key(logLevel):
            logLevel = 0
        if logLevel >= self.logLevel:
            now_str = str(dt.now())
            if len(now_str) <= 19:
                now_str += '.000000'
            now_str = now_str[0:23]
            #if self.output == 1 or self.output == 0:
            try:
                #tempFile = codecs.open(u'%s/%s-%s.log' %(self.log_path, name, now_str[:10]), 'a', 'utf-8')
                tempFile = codecs.open(u'%s/%s-%s.log' %(self.log_path, 'MediaDock', now_str[:10]), 'a', 'utf-8')
                tempFile.write(u'%s\t%s\t%s\t%s\n' % (name, now_str, Logger.logLevelStr[logLevel], message.replace('\n', '')))
                tempFile.close()
            except:
                try:
                    #tempFile = codecs.open(u'%s/%s-%s.log' %(self.log_path, name, now_str[:10]), 'a', 'utf-8')
                    tempFile = codecs.open(u'%s/%s-%s.log' %(self.log_path, 'MediaDock', now_str[:10]), 'a', 'utf-8')
                    tempFile.write(u'%s\t%s\t%s\tError in Logger %s\n' % (name, now_str, Logger.logLevelStr[logLevel], repr(message.replace('\n', ''))))
                    tempFile.close()
                except:
                    pass
            if not MDConst.FROZEN:
                print u'%s\t%s\t%s\t%s' % (name, now_str, Logger.logLevelStr[logLevel], message.replace('\n', ''))
            #if self.output == 2 or self.output == 0:
            #    try:
            #        #print u'%s\t%s\t%s' % (now_str, Logger.logLevelStr[logLevel], message.replace('\n', ''))
            #        print u'%s\t%s\t%s\t%s' % (name, now_str, Logger.logLevelStr[logLevel], message.replace('\n', ''))
            #    except Exception as e:
            #        #print e
            #        print u'%s\t%s\tError in Logger %s' % (now_str, Logger.logLevelStr[logLevel],
            #                                                repr(message.replace('\n', '')))

    def logServer(self, message='', logLevel=0):
        self.log(u'Server', message, logLevel)

    def logPServer(self, message='', logLevel=0):
        self.log(u'PServer', message, logLevel)

    def logEngine(self, message='', logLevel=0):
        self.log(u'Engine', message, logLevel)

    def logLogger(self, message='', logLevel=0):
        self.log(u'Logger', message, logLevel)

    def logMessage(self, message='', logLevel=0):
        self.log(u'Messages', message, logLevel)

    def logMediaDock(self, message='', logLevel=0):
        self.log(u'MediaDock', message, logLevel)

    def logCopy(self, message='', logLevel=0):
        self.log(u'Copy', message, logLevel)

    def logSession(self, message='', logLevel=0):
        self.log(u'Session', message, logLevel)
