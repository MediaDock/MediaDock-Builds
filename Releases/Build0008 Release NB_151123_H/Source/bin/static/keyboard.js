/*************************************************************
 * This script is developed by Arturs Sosins aka ar2rsawseen, http://webcodingeasy.com
 * Feel free to distribute and modify code, but keep reference to its creator
 *
 * Keyboard class creates an onscreen keyboard for websites. 
 * It emulates normal keyboard behavior by providing functions like Tab, 
 * Capslock, Shift, Enter, Backspace, AltGr, Home and End, and 
 * it can manipulate data in textareas and input fields with text or password type.
 * Keyboard outlook is adjustable and users can be allowed to change size and language of keyboard
 *
 * For more information, examples and online documentation visit: 
 * http://webcodingeasy.com/JS-classes/On-screen-Keyboard
**************************************************************/
var keyboard = function(id, config) {

	//element where to create keyboard graphic
	this.elem = document.getElementById(id);
	this.active_input;
	this.shift_once = false;
	
	this.conf = {	
		width: 30,
		height: 30,
		borderColor: "#808080",
		backgroundColor: "#d3d3d3",
		borderWidth : 2,
		borderStyle : "solid",
		borderRadius : 5,
		fontColor: "black",
		keymap : "us",
		allowChangeKeymap : true,
		allowChangeSize : true,
		position: "relative"
	};
	
	this.state = 0;
	this.locale_open;
	
	this.construct = function(ob){
		for(var opt in config){
			this.conf[opt]= config[opt];
		}
		this.conf.keymap;
		ob.selection = new Object();
		var inps = document.getElementsByTagName("input");
		for(var i = 0; i <= inps.length; i++)
		{
			if(inps[i] && (inps[i].type.toLowerCase() == "text" || inps[i].type.toLowerCase() == "password" || inps[i].type.toLowerCase() == "email" || inps[i].type.toLowerCase() == "url" || inps[i].type.toLowerCase() == "number" || inps[i].type.toLowerCase() == "search"))
			{
				add_event(inps[i], 'focus', function(e){	
					var target = get_event_target(e);
					ob.active_input = target;
					if(ob.selection.start == undefined || ob.selection.end == undefined)
					{
						if (document.selection) {
							var r = document.selection.createRange().duplicate();
							r.moveEnd('character', ob.active_input.value.length);
							if (r.text == '')
							{
								ob.selection.start = ob.active_input.value.length;
							}
							else
							{
								ob.selection.start = ob.active_input.value.lastIndexOf(r.text);
							}
							var d = document.selection.createRange().duplicate();
							d.moveStart('character', -ob.active_input.value.length);
							ob.selection.end = d.text.length;
						}
						else if (ob.active_input.selectionStart || ob.active_input.selectionStart=='0') {
							ob.selection.start = ob.active_input.selectionStart;
							ob.selection.end = ob.active_input.selectionEnd;
						}
					}
				});
				add_event(inps[i], 'mouseup', function(e){	
					var target = get_event_target(e);
					ob.active_input = target;
					ob.active_input.focus();
					if (document.selection) {
						var r = document.selection.createRange().duplicate();
						r.moveEnd('character', ob.active_input.value.length);
						if (r.text == '')
						{
							ob.selection.start = ob.active_input.value.length;
						}
						else
						{
							ob.selection.start = ob.active_input.value.lastIndexOf(r.text);
						}
						var d = document.selection.createRange().duplicate();
						d.moveStart('character', -ob.active_input.value.length);
						ob.selection.end = d.text.length;
					}
					else if (ob.active_input.selectionStart || ob.active_input.selectionStart=='0') {
						ob.selection.start = ob.active_input.selectionStart;
						ob.selection.end = ob.active_input.selectionEnd;
					}
					//alert(ob.selection.start + ' ' + ob.selection.end);
				});
				add_event(inps[i], 'keyup', function(e){	
					var target = get_event_target(e);
					ob.active_input = target;
					ob.active_input.focus();
					if (document.selection) {
						var r = document.selection.createRange().duplicate();
						r.moveEnd('character', ob.active_input.value.length);
						if (r.text == '')
						{
							ob.selection.start = ob.active_input.value.length;
						}
						else
						{
							ob.selection.start = ob.active_input.value.lastIndexOf(r.text);
						}
						var d = document.selection.createRange().duplicate();
						d.moveStart('character', -ob.active_input.value.length);
						ob.selection.end = d.text.length;
					}
					else if (ob.active_input.selectionStart || ob.active_input.selectionStart=='0') {
						ob.selection.start = ob.active_input.selectionStart;
						ob.selection.end = ob.active_input.selectionEnd;
					}
				});
				add_event(inps[i], 'change', function(e){	
					var target = get_event_target(e);
					ob.active_input = target;
					ob.active_input.focus();
					if (document.selection) {
						var r = document.selection.createRange().duplicate();
						r.moveEnd('character', ob.active_input.value.length);
						if (r.text == '')
						{
							ob.selection.start = ob.active_input.value.length;
						}
						else
						{
							ob.selection.start = ob.active_input.value.lastIndexOf(r.text);
						}
						var d = document.selection.createRange().duplicate();
						d.moveStart('character', -ob.active_input.value.length);
						ob.selection.end = d.text.length;
					}
					else if (ob.active_input.selectionStart || ob.active_input.selectionStart=='0') {
						ob.selection.start = ob.active_input.selectionStart;
						ob.selection.end = ob.active_input.selectionEnd;
					}
				});
			}
			else if(inps[i] && inps[i].nodeType == 1)
			{
				add_event(inps[i], 'focus', function(e){	
					var target = get_event_target(e);
					ob.active_input = target;
				});
			}
		}
		var inps = document.getElementsByTagName("textarea");
		for(var i = 0; i <= inps.length; i++)
		{
			if(inps[i])
			{
				add_event(inps[i], 'focus', function(e){	
					var target = get_event_target(e);
					ob.active_input = target;
					if(ob.selection.start == undefined || ob.selection.end == undefined)
					{
						if (document.selection) {
							var range = document.selection.createRange(); 
							var stored_range = range.duplicate(); 
							stored_range.moveToElementText( ob.active_input ); 
							stored_range.setEndPoint( 'EndToEnd', range ); 
							ob.selection.start = stored_range.text.length - range.text.length; 
							ob.selection.end = stored_range.text.length; 
						}
						else if (ob.active_input.selectionStart || ob.active_input.selectionStart=='0') {
							ob.selection.start = ob.active_input.selectionStart;
							ob.selection.end = ob.active_input.selectionEnd;
						}
					}
				});
				add_event(inps[i], 'mouseup', function(e){	
					var target = get_event_target(e);
					ob.active_input = target;
					if (document.selection) {
						var range = document.selection.createRange(); 
						var stored_range = range.duplicate(); 
						stored_range.moveToElementText( ob.active_input ); 
						stored_range.setEndPoint( 'EndToEnd', range ); 
						ob.selection.start = stored_range.text.length - range.text.length; 
						ob.selection.end = stored_range.text.length; 
					}
					else if (ob.active_input.selectionStart || ob.active_input.selectionStart=='0') {
						ob.selection.start = ob.active_input.selectionStart;
						ob.selection.end = ob.active_input.selectionEnd;
					}
				});
				add_event(inps[i], 'keyup', function(e){	
					var target = get_event_target(e);
					ob.active_input = target;
					if (document.selection) {
						var range = document.selection.createRange(); 
						var stored_range = range.duplicate(); 
						stored_range.moveToElementText( ob.active_input ); 
						stored_range.setEndPoint( 'EndToEnd', range ); 
						ob.selection.start = stored_range.text.length - range.text.length; 
						ob.selection.end = stored_range.text.length; 
					}
					else if (ob.active_input.selectionStart || ob.active_input.selectionStart=='0') {
						ob.selection.start = ob.active_input.selectionStart;
						ob.selection.end = ob.active_input.selectionEnd;
					}
				});
				add_event(inps[i], 'change', function(e){	
					var target = get_event_target(e);
					ob.active_input = target;
					if (document.selection) {
						var range = document.selection.createRange(); 
						var stored_range = range.duplicate(); 
						stored_range.moveToElementText( ob.active_input ); 
						stored_range.setEndPoint( 'EndToEnd', range ); 
						ob.selection.start = stored_range.text.length - range.text.length; 
						ob.selection.end = stored_range.text.length; 
					}
					else if (ob.active_input.selectionStart || ob.active_input.selectionStart=='0') {
						ob.selection.start = ob.active_input.selectionStart;
						ob.selection.end = ob.active_input.selectionEnd;
					}
				});

			}
		}
		this.generate_keyboard(ob);
	};
	
	this.generate_keyboard = function(ob){
		this.elem.style.width = (3*(15)+4)*parseInt(this.conf.borderWidth) + parseInt(this.conf.width*16) + 'px';
		this.elem.style.height = (3*(4)+4)*parseInt(this.conf.borderWidth) + parseInt(this.conf.height*5) + 'px';
		this.elem.style.position = this.conf.position;
		this.elem.style.borderColor = this.conf.borderColor;
		this.elem.style.borderWidth = this.conf.borderWidth + 'px';
		this.elem.style.borderStyle = this.conf.borderStyle;
		this.elem.style.backgroundColor = this.conf.backgroundColor;
		this.elem.style.color = this.conf.fontColor;
		for(var i = 0; i < this.layout.row.length; i++)
		{
			var total = parseInt(this.conf.borderWidth);
			for(var j = 0; j < this.layout.row[i].col.length; j++)
			{
				var div = document.createElement("div");
				if(this.layout.row[i].col[j].type != "none")
				{
					if(this.layout.row[i].col[j].type == "char")
					{
						if(this.keymap[this.conf.keymap].symbols[this.layout.row[i].col[j].id][this.state])
						{
							div.style.borderColor = this.conf.borderColor;
							div.style.borderWidth = this.conf.borderWidth + 'px';
							div.style.borderStyle = this.conf.borderStyle;
							div.style.cursor = "hand";
							div.style.cursor = "pointer";
							div.style.borderRadius = this.conf.borderRadius + 'px'; // standard
							div.style.MozBorderRadius = this.conf.borderRadius + 'px'; // Mozilla
							div.style.WebkitBorderRadius = this.conf.borderRadius + 'px'; // WebKit
							div.style.fontSize = Math.round((this.conf.width + this.conf.height)/3) + 'px';
							div.innerHTML = this.keymap[this.conf.keymap].symbols[this.layout.row[i].col[j].id][this.state];
							div.setAttribute("id", this.elem.id + '_' + this.layout.row[i].col[j].id + '_' + this.state);
							add_event(div, 'click', function(e){
								var target = get_event_target(e);
								procedure(ob, target);
								if(ob.active_input && check_valid_input(ob.active_input))
								{
									var parts = target.id.split("_");
									var inf = parts.length-3;
									var character = ob.keymap[ob.conf.keymap].symbols[parts[inf+1]][ob.state];
									ob.active_input.value = ob.active_input.value.substring(0,ob.selection.start) +  character + ob.active_input.value.substring(ob.selection.end, ob.active_input.value.length);
									if(ob.selection.start != ob.selection.end)
									{
										set_cursor(ob, ob.active_input, ob.selection.start + 1);
									}
									else
									{
										set_cursor(ob, ob.active_input, ob.selection.end+1);
									}
									if(ob.shift_once)
									{
										ob.shift_once = false;
										if(ob.state == 1)
										{
											ob.state = 0;
										}
										else if(ob.state == 3)
										{
											ob.state = 2;
										}
										remove_children(ob.elem);
										ob.generate_keyboard(ob);
									}
								}
							});
						}
					}
					else if(this.is_allowed(this.layout.row[i].col[j].name))
					{
						div.style.borderColor = this.conf.borderColor;
						div.style.borderWidth = this.conf.borderWidth + 'px';
						div.style.borderStyle = this.conf.borderStyle;
						div.style.cursor = "hand";
						div.style.cursor = "pointer";
						div.style.borderRadius = this.conf.borderRadius + 'px'; // standard
						div.style.MozBorderRadius = this.conf.borderRadius + 'px'; // Mozilla
						div.style.WebkitBorderRadius = this.conf.borderRadius + 'px'; // WebKit
						div.style.fontSize = Math.round((this.conf.width + this.conf.height)/3) + 'px';
						div.innerHTML = this.layout.row[i].col[j].name;
						switch(this.layout.row[i].col[j].name)
						{
							case "":
								add_event(div, 'click', function(e){	
									var target = get_event_target(e);
									procedure(ob, target);
									if(ob.active_input && check_valid_input(ob.active_input))
									{
										ob.active_input.value = ob.active_input.value.substring(0,ob.selection.start) + " " + ob.active_input.value.substring(ob.selection.end, ob.active_input.value.length);
										if(ob.selection.start != ob.selection.end)
										{
											set_cursor(ob, ob.active_input, ob.selection.start + 1);
										}
										else
										{
											set_cursor(ob, ob.active_input, ob.selection.end+1);
										}
										ob.active_input.focus();
									}
								});
								break;
							case "Backspace":
								add_event(div, 'click', function(e){	
									var target = get_event_target(e);
									procedure(ob, target);
									if(ob.active_input && check_valid_input(ob.active_input))
									{
										if(ob.selection.start != ob.selection.end)
										{
											ob.active_input.value = ob.active_input.value.substr(0, ob.selection.start) + ob.active_input.value.substr(ob.selection.end);
											set_cursor(ob, ob.active_input, ob.selection.start);
										}
										else
										{
											ob.active_input.value = ob.active_input.value.substr(0, ob.selection.start-1) + ob.active_input.value.substr(ob.selection.end);
											set_cursor(ob, ob.active_input, ob.selection.end-1);
										}
									}
								});
								break;
							case "Enter":
								add_event(div, 'click', function(e){	
									var target = get_event_target(e);
									procedure(ob, target);
									if(ob.active_input)
									{
										if(ob.active_input.nodeName.toLowerCase() == "textarea")
										{
											ob.active_input.value = ob.active_input.value.substring(0,ob.selection.start) + "\n" + ob.active_input.value.substring(ob.selection.end, ob.active_input.value.length);
											var step = 1;
											if(document.selection)
											{
												step = 2;
											}
											if(ob.selection.start != ob.selection.end)
											{
												set_cursor(ob, ob.active_input, ob.selection.start + step);
											}
											else
											{
												set_cursor(ob, ob.active_input, ob.selection.end + step);
											}
										}
										else
										{
											var elem = ob.active_input;
											while(elem != undefined && elem.nodeName.toLowerCase() != "form")
											{
												elem = elem.parentNode;
											}
											if(elem && elem.nodeName.toLowerCase() == "form")
											{
												elem.submit();
											}
										}
									}
								});
								break;
							case "Caps":
								add_event(div, 'click', function(e){	
									var target = get_event_target(e);
									procedure(ob, target);
									ob.shift_once = false;
									switch(ob.state)	
									{
										case 0:
											ob.state = 1;
										break;
										case 1:
											ob.state = 0;
										break;
										case 2:
											ob.state = 3;
										break;
										case 3:
											ob.state = 2;
										break;
									}
									remove_children(ob.elem);
									ob.generate_keyboard(ob);
								});
								break;
							case "AltGr":
								add_event(div, 'click', function(e){	
									var target = get_event_target(e);
									procedure(ob, target);
									switch(ob.state)	
									{
										case 0:
											ob.state = 2;
										break;
										case 1:
											ob.state = 3;
										break;
										case 2:
											ob.state = 0;
										break;
										case 3:
											ob.state = 1;
										break;
									}
									remove_children(ob.elem);
									ob.generate_keyboard(ob);
								});
								break;
							case "Shift":
								add_event(div, 'click', function(e){	
									var target = get_event_target(e);
									procedure(ob, target);
									switch(ob.state)	
									{
										case 0:
											ob.state = 1;
											ob.shift_once = true;
										break;
										case 1:
											ob.state = 0;
											ob.shift_once = false;
										break;
										case 2:
											ob.state = 3;
											ob.shift_once = true;
										break;
										case 3:
											ob.state = 2;
											ob.shift_once = false;
										break;
									}
									remove_children(ob.elem);
									ob.generate_keyboard(ob);
								});
								break;
							case "+":
								add_event(div, 'click', function(e){
									var target = get_event_target(e);
									procedure(ob, target);
									ob.conf.width = parseInt(ob.conf.width) + 5;
									ob.conf.height = parseInt(ob.conf.height) + 5;
									remove_children(ob.elem);
									ob.generate_keyboard(ob);
								});
								break;
							case "-":
								add_event(div, 'click', function(e){
									var target = get_event_target(e);
									procedure(ob, target);
									if(ob.conf.width - 5 < 10)
									{
										ob.conf.width = 10;
									}
									else
									{
										ob.conf.width = parseInt(ob.conf.width) - 5;										
									}
									if(ob.conf.height - 5 < 10)
									{
										ob.conf.height = 10;
									}
									else
									{
										ob.conf.height = parseInt(ob.conf.height) - 5;										
									}
									remove_children(ob.elem);
									ob.generate_keyboard(ob);
								});
								break;
							case "Tab":
								add_event(div, 'click', function(e){
									var target = get_event_target(e);
									procedure(ob, target);
									if(ob.active_input)
									{
										var next = find_next(ob.active_input.parentNode);
										if(next)
										{
											next.focus();
										}
									}
									else
									{
										var first = find_first(document);
										if(first)
										{
											first.focus();
										}
									}
								});
								break;
							case "Lang":
								add_event(div, 'click', function(e){
									var target = get_event_target(e);
									disableSelection(target);
									if(ob.locale_open)
									{
										ob.elem.removeChild(ob.locale_open);
										ob.locale_open = undefined;
									}
									else
									{
										ob.show_locale(ob, target);
									}
								});
								break;
							case "Home":
								add_event(div, 'click', function(e){
									var target = get_event_target(e);
									disableSelection(target);
									if(ob.active_input)
									{
										set_cursor(ob, ob.active_input, 0);
									}
								});
								break;
							case "End":
								add_event(div, 'click', function(e){
									var target = get_event_target(e);
									disableSelection(target);
									if(ob.active_input)
									{
										set_cursor(ob, ob.active_input, ob.active_input.value.length);
									}
								});
								break;
						}
					}
				}
				div.style.width = ((parseInt(this.conf.width) * this.layout.row[i].col[j].size) + ((this.layout.row[i].col[j].size-1)*3*this.conf.borderWidth)) + 'px';
				div.style.height = this.conf.height + 'px';
				div.style.position = "absolute";
				div.style.top = ((((3*i)+1)*parseInt(this.conf.borderWidth)) + (i*parseInt(this.conf.height))) + 'px';
				div.style.left = total + 'px';
				div.style.textAlign = 'center';
				div.style.lineHeight = this.conf.height + 'px';
				total += parseInt(div.style.width) + (3*parseInt(this.conf.borderWidth));
				this.elem.appendChild(div);
			}
		}
	};
	
	this.show_locale = function(ob, elem){
		var div = document.createElement("div");
		div.style.borderColor = ob.conf.borderColor;
		div.style.borderWidth = ob.conf.borderWidth + 'px';
		div.style.borderStyle = ob.conf.borderStyle;
		div.style.borderRadius = ob.conf.borderRadius + 'px'; // standard
		div.style.MozBorderRadius = ob.conf.borderRadius + 'px'; // Mozilla
		div.style.WebkitBorderRadius = ob.conf.borderRadius + 'px'; // WebKit
		div.style.backgroundColor = ob.conf.backgroundColor;
		div.style.position = "absolute";
		div.style.padding = "5px";
		div.style.top = (3*(4)+4)*parseInt(ob.conf.borderWidth) + parseInt((ob.conf.height*5)-parseInt(ob.conf.borderWidth*2)) + 'px';
		div.style.left = (0*parseInt(ob.conf.width) + parseInt(ob.conf.borderWidth*1)) + 'px';
		div.style.height = Math.round((ob.conf.width + ob.conf.height)/3)*5 + 'px';
		div.style.overflow = "auto";
		var ul = document.createElement("ul");
		ul.style.listStyle = "none";
		ul.style.padding = '0px';
		ul.style.margin = '0px';
		for(var lang in ob.keymap){
			var li = document.createElement("li");
			li.style.textAlign = 'left';
			li.style.padding = "5px";
			li.style.fontSize = Math.round((ob.conf.width + ob.conf.height)/4) + 'px';
			li.innerHTML = ob.keymap[lang].name;
			li.setAttribute("id", ob.elem.id + '_' + lang);
			li.style.cursor = "hand";
			li.style.cursor = "pointer";
			add_event(li, 'mouseover', function(e){	
				var target = get_event_target(e);
				target.style.backgroundColor = ob.conf.fontColor;
				target.style.color = ob.conf.backgroundColor;
			});
			add_event(li, 'mouseout', function(e){	
				var target = get_event_target(e);
				target.style.backgroundColor = ob.conf.backgroundColor;
				target.style.color = ob.conf.fontColor;
			});
			add_event(li, 'click', function(e){	
				var target = get_event_target(e);
				var id = target.id.split("_");
				var inf = id.length-2;
				ob.conf.keymap = id[inf+1];
				ob.elem.removeChild(ob.locale_open);
				ob.locale_open = undefined;
				remove_children(ob.elem);
				ob.generate_keyboard(ob);
			});
			ul.appendChild(li);
		}
		div.appendChild(ul);
		ob.elem.appendChild(div);
		ob.locale_open = div;
	};
	
	//check if button should be shown
	this.is_allowed = function(name){
		if(name == "+" || name == "-")
		{
			if(this.conf.allowChangeSize)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else if(name == "Lang")
		{
			if(this.conf.allowChangeKeymap)
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return true;
		}
	};
	
	//find next input element
	var find_next = function(elem){
		if(check_input(elem))
		{
			return elem;
		}
		result = undefined;
		while(elem.nextSibling && elem.nextSibling.nodeType != 1)
		{
			elem = elem.nextSibling;
		}
		if(elem.nextSibling)
		{
			result = find_first(elem.nextSibling);
			if(result)
			{
				return result;
			}
			result = find_next(elem.nextSibling);
		}
		if(result)
		{
			return result;
		}
		while(elem.parentNode && !result) 
		{
			elem = elem.parentNode;
			result = find_next(elem);
		}
		if(elem.parentNode)
		{
			return result;
		}
		else
		{
			return find_first(document);
		}
	};
	
	//find first valid input element
	var find_first = function(elem){
		if(check_input(elem))
		{
			return elem;
		}
		//debug(elem);
		result = undefined;
		if(elem.firstChild)
		{
			result = find_first(elem.firstChild);
		}
		if(result)
		{
			return result;
		}
		while(elem.nextSibling && !result) 
		{
			elem = elem.nextSibling;
			result = find_first(elem);
		}
		if(result)
		{
			return result;
		}
		else
		{
			return find_next(elem.parentNode);
		}
	};
	
	//check if input
	var check_input = function(el){
		if(el.nodeName.toLowerCase() == "textarea" || el.nodeName.toLowerCase() == "input")
		{
			return true;
		}
		else
		{
			return false;
		}
	};
	
	//check if valid input
	var check_valid_input = function(el){
		if(el.nodeName.toLowerCase() == "textarea" || (el.nodeName.toLowerCase() == "input" && (el.type.toLowerCase() == "text" || el.type.toLowerCase() == "password" || el.type.toLowerCase() == "email" || el.type.toLowerCase() == "url" || el.type.toLowerCase() == "number" || el.type.toLowerCase() == "search")))
		{
			return true;
		}
		else
		{
			return false;
		}
	};
	
	var procedure = function(ob, target){
		if(ob.locale_open)
		{
			ob.elem.removeChild(ob.locale_open);
			ob.locale_open = undefined;
		}
		disableSelection(target);
	};
	
	//disable text selection
	var disableSelection = function (target){
		if (typeof target.onselectstart!="undefined") //IE route
		{
			target.onselectstart=function()
			{
				return false;
			}
		}

		else if (typeof target.style.MozUserSelect!="undefined") //Firefox route
		{
			target.style.MozUserSelect="none";
		}
		else //All other route (ie: Opera)
		{
			target.onmousedown=function()
			{
				return false;
			}
		}
	};
	
	//set cursor position
	var set_cursor = function(ob, oField, iCaretPos) {
		// IE Support
		if (document.selection) { 

			//count special characters to count them es one character
			var discard = count_spec_chars(oField.value);
			// Create empty selection range
			var range = oField.createTextRange(); 
			// Move selection start and end to 0 position
			range.collapse(true); 
			// Move selection start and end to desired position counting each special character as one
			range.moveEnd('character', iCaretPos - discard); 
			range.moveStart('character', iCaretPos - discard); 
			range.select(); 
		}
		// Firefox support
		else 
		{
			oField.selectionStart = iCaretPos;
			oField.selectionEnd = iCaretPos;
		}
		ob.selection.start = iCaretPos;
		ob.selection.end = iCaretPos;
		ob.active_input.focus();
		//alert(ob.selection.start + '-' + ob.selection.end);
	};
	
	//count special chars in string
	var count_spec_chars = function(val){
		return val.length - val.replace(new RegExp("[\n\'\"\&\\\t\b\f]","g"), '').length;
	};
	
	//get if of element that fired event
	var get_event_target = function(event){
		if(!event)
		{
			return window.event.srcElement;
		}
		else if(event.target)
		{
			return event.target; 
		}
		else
		{
			return event.srcElement;
		}
	};	
	
	//add event
	var add_event = function(element, type, listener){
		if(element.addEventListener)
			element.addEventListener(type, listener, false);
		else
			element.attachEvent('on' +  type, listener);
	};
	
	//remove all children of parent
	var remove_children = function(elem){
		while (elem.childNodes[0]) 
		{
			elem.removeChild(elem.childNodes[0]);
		}
	};
	
	this.layout = {
		row : [
		{
			col : [
				{type: "char", size: 1, id:0},
				{type: "char", size: 1, id:1},
				{type: "char", size: 1, id:2},
				{type: "char", size: 1, id:3},
				{type: "char", size: 1, id:4},
				{type: "char", size: 1, id:5},
				{type: "char", size: 1, id:6},
				{type: "char", size: 1, id:7},
				{type: "char", size: 1, id:8},
				{type: "char", size: 1, id:9},
				{type: "char", size: 1, id:10},
				{type: "char", size: 1, id:11},
				{type: "char", size: 1, id:12},
				{type: "function", size: 3, name: "Backspace"}
			]
		},
		{
			col : [
				{type: "function", size: 1.5, name: "Tab"},
				{type: "char", size: 1, id:13},
				{type: "char", size: 1, id:14},
				{type: "char", size: 1, id:15},
				{type: "char", size: 1, id:16},
				{type: "char", size: 1, id:17},
				{type: "char", size: 1, id:18},
				{type: "char", size: 1, id:19},
				{type: "char", size: 1, id:20},
				{type: "char", size: 1, id:21},
				{type: "char", size: 1, id:22},
				{type: "char", size: 1, id:23},
				{type: "char", size: 1, id:24},
				{type: "char", size: 2.5, id:25}
			]
		},
		{
			col : [
				{type: "function", size: 2, name: "Caps"},
				{type: "char", size: 1, id:26},
				{type: "char", size: 1, id:27},
				{type: "char", size: 1, id:28},
				{type: "char", size: 1, id:29},
				{type: "char", size: 1, id:30},
				{type: "char", size: 1, id:31},
				{type: "char", size: 1, id:32},
				{type: "char", size: 1, id:33},
				{type: "char", size: 1, id:34},
				{type: "char", size: 1, id:35},
				{type: "char", size: 1, id:36},
				{type: "function", size: 3, name: "Enter"}
			]
		},
		{
			col : [
				{type: "function", size: 2.5, name: "Shift"},
				{type: "char", size: 1, id:37},
				{type: "char", size: 1, id:38},
				{type: "char", size: 1, id:39},
				{type: "char", size: 1, id:40},
				{type: "char", size: 1, id:41},
				{type: "char", size: 1, id:42},
				{type: "char", size: 1, id:43},
				{type: "char", size: 1, id:44},
				{type: "char", size: 1, id:45},
				{type: "char", size: 1, id:46},
				{type: "function", size: 3.5, name: "Shift"}
			]
		},
		{
			col : [
				{type: "function", size: 2, name: "Lang"},
				{type: "function", size: 1, name: "+"},
				{type: "function", size: 1, name: "-"},
				{type: "function", size: 6, name: ""},
				{type: "function", size: 2, name: "AltGr"},
				{type: "function", size: 2, name: "Home"},
				{type: "function", size: 2, name: "End"}
			]
		}]
	};
	
	this.keymap = {
		"us" : {
			'name': "US International", 
			'symbols': [
				["`", "~"], ["1", "!", "\u00a1", "\u00b9"], ["2", "@", "\u00b2"], ["3", "#", "\u00b3"], ["4", "$", "\u00a4", "\u00a3"], ["5", "%", "\u20ac"], ["6", "^", "\u00bc"], ["7", "&", "\u00bd"], ["8", "*", "\u00be"], ["9", "(", "\u2018"], ["0", ")", "\u2019"], ["-", "_", "\u00a5"], ["=", "+", "\u00d7", "\u00f7"], ["q", "Q", "\u00e4", "\u00c4"], ["w", "W", "\u00e5", "\u00c5"], ["e", "E", "\u00e9", "\u00c9"], ["r", "R", "\u00ae"], ["t", "T", "\u00fe", "\u00de"], ["y", "Y", "\u00fc", "\u00dc"], ["u", "U", "\u00fa", "\u00da"], ["i", "I", "\u00ed", "\u00cd"], ["o", "O", "\u00f3", "\u00d3"], ["p", "P", "\u00f6", "\u00d6"], ["[", "{", "\u00ab"], ["]", "}", "\u00bb"], ["\\", "|", "\u00ac", "\u00a6"], ["a", "A", "\u00e1", "\u00c1"], ["s", "S", "\u00df", "\u00a7"], ["d", "D", "\u00f0", "\u00d0"], ["f", "F"], ["g", "G"], ["h", "H"], ["j", "J"], ["k", "K"], ["l", "L", "\u00f8", "\u00d8"], [";", ":", "\u00b6", "\u00b0"], ["'", '"', "\u00b4", "\u00a8"], ["z", "Z", "\u00e6", "\u00c6"], ["x", "X"], ["c", "C", "\u00a9", "\u00a2"], ["v", "V"], ["b", "B"], ["n", "N", "\u00f1", "\u00d1"], ["m", "M", "\u00b5"], [",", "<", "\u00e7", "\u00c7"], [".", ">"], ["/", "?", "\u00bf"]
			]
		}
	};
	
	this.add_keymap = function(key, map){
		this.keymap[key] = map;
	};
	
	this.show = function(){
		this.construct(this);
	};
}