function ajax_request(parurl, pardata) {
    getJsonData(pardata);
}

/*
function quit_programm() {
    ajax_request("ajax", "quit");
}
*/

function reload_filesettings() {
    ajax_request("ajax", {'function': "reload_filesettings"});
}

function reload_dh_settings() {
    ajax_request("ajax", {'function': "reload_dh_settings"});
}

function reload_time_settings() {
    ajax_request("ajax", {'function': "reload_time_settings"});
}

function reload_records() {
    ajax_request("ajax", {'function': "reload_records"});
}

function lock() {
    ajax_request("ajax", {'function': "lock"});
}

function showNormal(){
    ajax_request("ajax", {'function': 'showNormal'})
}

function showFullScreen(){
    ajax_request("ajax", {'function': 'showFullScreen'})
}
