function togglebgcolor(event){
    var target = event.target || event.srcElement;
    if(String(target.style.backgroundColor) == 'rgb(171, 205, 239)'){
       target.style.backgroundColor = document.getElementById('explorer').style.backgroundColor; // color of explorer
    }
    else{
        target.style.backgroundColor = '#abcdef';
    }
}

function selectAll(){
    var elements = document.getElementsByName('exp_item');
    for(var i = 0; i < elements.length; i++){
        elements[i].style.backgroundColor = '#abcdef';
    }
}

function unSelectAll(){
    var elements = document.getElementsByName('exp_item');
    var col = document.getElementById('explorer').style.backgroundColor;
    for(var i = 0; i < elements.length; i++){
        elements[i].style.backgroundColor = col;
    }
}

function doubleClick(event){
    var target = event.target || event.srcElement;
    var id = target.getAttribute("id");
    if(id.substring(0,3) == 'dir'){
       getContent(id);
    }
}

function getContent(path){
    var elem = document.getElementById('explorer');
    elem.innerHTML = '<img border="0" src="../static/images/wait.gif" alt="Bitte warten" height="40">';
    document.getElementById('record_data').innerHTML = '';
    try {
        data = getJsonData({'path': path.substring(4, path.length), 'function': "explorerGetDir"}, true);// todo später path
        insertContent(data);
    }
    catch(Error){
        explorerAjaxFail();
    }
}

function explorerAjaxFail(){
    console.log("ajax fail");
    var elem = document.getElementById('explorer');
    elem.innerHTML = 'Anfrage fehlgeschlagen';
}

function insertContent(data){
    var elem = document.getElementById('explorer');
    elem.innerHTML = data.info;
    //$("#button_folder").replaceWith('<button id="button_folder" type="button" class="btn btn-default" onclick="getContent(' + "'" + 'dir_' + data.prev_path + "'" + ')">Übergeordneter Ordner</button>');
    var html = '';
    var prev_dir = '';
    if (data.prev_path == 'Media'){
        data.prev_path = ['Media']
    }
    for	(var i = 0; i < data.prev_path.length; i++) {
        if(i==0){
            html += '<li><a onclick="getContent(' + "'dir_" + data.prev_path[i] + "'" + ')">' + data.prev_path[i] + '</a></li>';
            prev_dir += data.prev_path[i];
        }
        else{
            prev_dir += '/' + data.prev_path[i];
            html += '<li><a onclick="getContent(' + "'dir_" + prev_dir + "'" + ')">' + data.prev_path[i] + '</a></li>';
        }
    } ;
    document.getElementById('breadcrumb').innerHTML = html;
    document.getElementById('record_data').innerHTML = data.record_data;
}

function insertDropdown(data){
    var elem = document.getElementById('dropdown_inner');
    elem.innerHTML = data.info;
}

function makeDropdown() {
    data = getJsonData({'function': 'getExplorerDropdown'}, true);
    insertDropdown(data);
}

makeDropdown();
getContent('dir_Media');
setInterval(makeDropdown, 2000);