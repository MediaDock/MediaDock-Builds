# -*- coding: utf-8 -*-

__author__ = 'Peter'

ROOT = 0
VIEW = 1
UNLOCK = 2
QUIT = 3
SHUTDOWN = 4
SHOWNORMAL = 5
LOGGERMODE = 6  # todo remove this
MANIPULATE_FSM = 7
STATISTICS = 8
SETTINGS = 9
CONST10 = 10
CONST11 = 11
CONST12 = 12
CONST13 = 13
CONST14 = 14
CONST15 = 15

'''
    Berechtigungen

    Für alle User:
        Ändern des eigenen Passwortes,
        Löschen des eigenen Accounts

    0 b x x x x x x x x x x x x x x x x
        | | | | | | | | | | | | | | | |
        | | | | | | | | | | | | | | | +-
        | | | | | | | | | | | | | | +---
        | | | | | | | | | | | | | +-----
        | | | | | | | | | | | | +-------
        | | | | | | | | | | | +---------
        | | | | | | | | | | +-----------
        | | | | | | | | | +-------------
        | | | | | | | | +--------------- Change Settings
        | | | | | | | +----------------- Manipulate FSM
        | | | | | | +------------------- LoggerMode
        | | | | | +--------------------- ShowNormal
        | | | | +----------------------- Shutdown
        | | | +------------------------- Quit
        | | +--------------------------- Unlock
        | +----------------------------- Namen aller Benutzer sehen
        +------------------------------- Ändern von Berechtigungen, Ändern von Passwörtern anderer User, Anlegen neuer User,
                                         Löschen von Usern, Alle Rechte sehen
'''

SIZE = 16
MASTER_RIGHTS = u'1' * SIZE