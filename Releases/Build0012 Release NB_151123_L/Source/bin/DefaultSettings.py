# -*- coding: utf-8 -*-

__author__ = 'Peter'

general = {u'autostart': True,
           u'statistics': True,
           u'drives': {u'excludedDrives':[], # example: [u'C:\\', u'/dev/sda1']
                       u'useExclusionList':False,
                       u'removableDrivesOnly':True
                       },
           u'messages': {u'level': 1,
                         u'default_live_span': 10000
                         },
           u'logger': {u'level': 1,
                       u'clean': True
                       }
           }

timer = {u'nr_days': [],
         u'sd_time': {u'weekly': {u'0': [u'23:59'], u'1': [u'23:59'], u'2': [u'23:59'], u'3': [u'23:59'],
                                  u'4': [u'23:59'], u'5': [u'23:59'], u'6': [u'23:59']},
                      u'yearly': {}},
         u'lc_time': {u'weekly': {}, u'yearly': {}},
         u'ul_time': {u'weekly': {}, u'yearly': {}},
         u'time_transition_4_7': 5.0,   # time in minutes
         u'time_transition_5_2': 10.0,   # time in minutes
         u'time_transition_6_7': 5.0   # time in minutes
         }

files = {u'rootdir': None,
         u'explorer_file_extensions': [u'mp3',
                                       u'wav',
                                       u'pdf',
                                       u'txt',
                                       u'xml',
                                       u'png',
                                       u'jpg',
                                       u'jpeg',
                                       u'ppt',
                                       u'pptx',
                                       u'xls',
                                       u'xlsx',
                                       u'doc',
                                       u'docx',
                                       u'gif',
                                       u'png',
                                       u'svg',
                                       u'txt'],
         u'copy_file_extensions':[u'mp3',
                                  u'wav',
                                  u'jpg',
                                  u'jpeg',
                                  u'png']
          }

