# coding=utf-8

import os
import tkMessageBox
import Tkinter
import shutil

master_window = Tkinter.Tk()

# read latest build number
builds_file = open('builds.txt', 'r')
build = '%05i' % int(builds_file.read().split('\n')[-1])
builds_file.close()

# read latest release name
release_file = open('releases.txt', 'r')
release = release_file.read().split('\n')[-2]
release_file.close()

# check if build dir exists
root_release_path = os.path.abspath(os.path.join('..', 'Releases\\Build %s-%s' % (build, release)))
if not os.path.isdir(root_release_path):
    tkMessageBox.showerror(u'Error', u'The release path %s doesn\'t exists' % root_release_path)
    exit()

# ask if the latest release should really be removed
ans = tkMessageBox.askyesno(u'Remove Release', u'Should the release %s really be removed?' % root_release_path)
if not ans:
    exit()

# remove dir "Build<Number> Release <Release>"
shutil.rmtree(root_release_path)

# remove latest build number from file
build_file = open('builds.txt', 'r')
content = build_file.read()
build_file.close()
content_list = content.split('\n')
content_list_new = content_list[:-1]
content_new = '\n'.join(content_list_new)
build_file = open('builds.txt', 'w')
build_file.write(content_new)
build_file.close()

# remove latest release name from file
release_file = open('releases.txt', 'r')
content = release_file.read()
release_file.close()
content_list = content.split('\n')
content_list_new = content_list[:-2]
content_new = '\n'.join(content_list_new) + '\n'
release_file = open('releases.txt', 'w')
release_file.write(content_new)
release_file.close()

# say if it was successful
tkMessageBox.showinfo(u'Remove Release', u'The release %s was removed successfully!' % root_release_path)

exit()
