# coding=utf-8

import os
from datetime import datetime as dt
import tkMessageBox as messageBox
import tkSimpleDialog as simpleDialog
import codecs
import inspect
import json
import shutil
import sys
import tarfile
import time
import subprocess
import platform

__author__ = 'Peter'

script_dir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
builds_number_file_path = os.path.join(script_dir, 'builds.txt')


class Builder(object):

    def __init__(self):
        self.log_counter = 0
        self.log_file = None
        self.log_messages = list()
        self.nightly_build = True
        self.build_number = 'XXXXX'
        self.version = ''
        self.release = None
        self.source_root_dir = os.path.abspath(os.path.join('..', '..', 'MediaDock', 'Source'))
        self.target_root_dir = None
        self.target_source_dir = None
        self.icon_path = ''
        now = dt.now()
        self.replace_dict = dict()
        self.replace_dict['# +++ remove this +++'] = ''
        self.replace_dict['+++ replace full date +++'] = '%02i.%02i.%04i' % (now.day, now.month, now.year)
        self.replace_dict['+++ replace year +++'] = str(now.year)
        self.replace_dict['+++ replace license path +++'] = os.path.abspath(os.path.join(script_dir,
                                                                                         'MediaDock_license.txt'))

    def check_platform(self):
        self.log('Check platform...')
        if not platform.system() == 'Windows':
            messageBox.showerror('Error', 'You are using a wrong operation system. It should be Windows!')
            sys.exit(-1)
        self.log('Platform is OK')

    @staticmethod
    def get_now_str():
        return str(dt.now())[0:23]

    def log(self, message):
        log_text = '%s : %04i : %s\n' % (self.get_now_str(), self.log_counter, message.replace('\n', ''))
        self.log_counter += 1
        print log_text

        self.log_messages.append(log_text)

        if self.log_file:
            for message in self.log_messages[:]:
                self.log_file.write(message)
            self.log_messages = list()
            self.log_file.flush()

    def ask_nightly_build(self):
        self.log('Ask nightly build...')
        if not messageBox.askyesno('Build Type', 'NightlyBuild ausführen?'):
            self.nightly_build = False
            self.log('Nightly build chosen')
        else:
            self.log('Release build chosen')

    def get_build_number(self):
        self.log('Get build number...')
        if not self.nightly_build:
            builds_file = open(builds_number_file_path, 'r')
            builds = [int(line) for line in builds_file.read().split('\n')]
            self.build_number = '%05i' % (max(builds) + 1, )
        self.replace_dict['+++ replace build +++'] = self.build_number
        self.log('Got build number %s' % self.build_number)

    def get_version(self):
        self.log('Get version')
        if self.nightly_build:
            counter = 65
            release = 'NB_%s_%s' % (dt.now().strftime('%y%m%d'), chr(counter))
            target_dir = os.path.join(script_dir, '..', 'NightlyBuilds', 'Build %s' % release)
            while os.path.isdir(target_dir):
                counter += 1
                release = 'NB_%s_%s' % (dt.now().strftime('%y%m%d'), chr(counter))
                target_dir = os.path.join(script_dir, '..', 'NightlyBuilds', 'Build %s' % release)
            self.release = release
            self.version = release

        else:
            ans = simpleDialog.askstring('Release', 'Insert the version of the release:')
            if ans is None:
                sys.exit()
            self.version = ans
            self.release = '%s-%s' % (self.build_number, self.version)
            builds_file = open(builds_number_file_path, 'a')
            builds_file.write('\n%i' % int(self.build_number))
            releases_file = codecs.open('releases.txt', 'a', 'utf-8')
            releases_file.write(self.version + '\n')
            releases_file.close()
        self.replace_dict['+++ replace release +++'] = self.version
        self.log('Got version %s' % self.version)

    def generate_paths(self):
        self.log('Start generate paths...')
        if self.nightly_build:
            self.target_root_dir = os.path.abspath(os.path.join(script_dir, '..', 'NightlyBuilds',
                                                                'Build ' + self.release))
        else:
            self.target_root_dir = os.path.abspath(os.path.join(script_dir, '..', 'Releases', 'Build ' + self.release))
        os.makedirs(self.target_root_dir)
        self.target_source_dir = os.path.join(self.target_root_dir, 'Source')
        self.icon_path = os.path.abspath(os.path.join(self.target_source_dir, 'bin\\files\\music-folder.ico'))
        self.replace_dict['+++ replace setup icon path +++'] = self.icon_path
        self.log_file = codecs.open(os.path.join(self.target_root_dir, 'log.txt'), 'a', 'utf-8')
        self.log('Paths generated')

    def copy_sources(self):
        self.log('Start copy sources...')
        shutil.copytree(self.source_root_dir, self.target_source_dir,
                        ignore=shutil.ignore_patterns('*.pyc',
                                                      'tmp*',
                                                      '*TestExecutionQueue.txt',
                                                      '*TestResult.txt',
                                                      '*splash_screen.png'))

        # remove ignored files
        ignored_files = GitHandler.get_ignored_files_from_dir(self.source_root_dir)
        for ignored_file in ignored_files:
            ignored_file_path = os.path.join(self.target_source_dir, ignored_file)
            if os.path.exists(ignored_file_path):
                self.log('Remove ignored file: {}'.format(ignored_file))
                os.remove(ignored_file_path)

        self.log('Sources copied')

    def modify_file(self, input_filename, output_filename=None, encoding='utf-8'):
        if output_filename is None:
            output_filename = input_filename
        self.log('Start modify "%s"...' % input_filename)
        src_file = codecs.open(input_filename, 'r', encoding=encoding)
        src_content = src_file.readlines()
        src_file.close()
        j = -1
        temp_src_content = src_content[:]
        for k in src_content:
            j += 1
            if '+++ remove this line +++' in k:
                del temp_src_content[j]
                j -= 1
                continue
            temp_line = k
            for l, m in self.replace_dict.iteritems():
                temp_line = temp_line.replace(l, m)
            temp_src_content[j] = temp_line
        src_file = codecs.open(output_filename, 'w', encoding=encoding)
        src_file.writelines(temp_src_content)
        src_file.close()
        self.log('Modify file finished')

    def modify_sources(self):
        self.log('Start modify sources...')
        target_bin_dir = os.path.join(self.target_source_dir, 'bin')
        for root, dirs, files in os.walk(target_bin_dir):
            for file_ in files:
                file_path = os.path.join(root, file_)
                _, ext = os.path.splitext(file_path)
                if ext in ('.py', '.html', '.js'):
                    self.modify_file(file_path)
        self.log('Sources modified')

    def compile_less(self):
        self.log('Start compile less files...')
        files_to_delete = list()
        for root, dirs, files in os.walk(self.target_source_dir, followlinks=False):
            for file_ in files:
                if file_.endswith('.less'):
                    file_path = os.path.join(root, file_)
                    self.compile_less_file(file_path)
                    files_to_delete.append(file_path)
        for file_path in files_to_delete:
            os.remove(file_path)
        self.log('Compiled less files')

    def compile_less_file(self, input_filename, output_filename=None):
        if output_filename is None:
            output_filename = input_filename[:-4] + 'css'
        self.log('Start compile "%s"...' % input_filename)
        if os.path.isfile(output_filename):
            os.remove(output_filename)
        process = subprocess.Popen('lessc.cmd "{0}" "{1}"'.format(input_filename, output_filename),
                                   stdout=self.log_file, stderr=self.log_file)
        process.wait()
        self.log('Compile file finished')

    def convert_svg_to_png(self, svg_file_path, png_file_path=None, remove_source=False):
        abs_svg_file_path = os.path.abspath(svg_file_path)
        if not png_file_path:
            tmp, _ = os.path.splitext(abs_svg_file_path)
            abs_png_file_path = os.path.abspath(tmp + '.png')
        else:
            abs_png_file_path = os.path.abspath(png_file_path)

        command = 'inkscape -z -e "{}" "{}"'.format(abs_png_file_path, abs_svg_file_path)
        process = subprocess.Popen(command, stdout=self.log_file, stderr=self.log_file)
        process.wait()
        if not os.path.exists(abs_png_file_path):
            raise Exception('Generated png "{}" could not be found'.format(abs_png_file_path))
        if remove_source:
            os.remove(svg_file_path)

    def generate_splash_screen(self):
        self.log('Start generate splash screen image...')
        splash_screen_svg_path = os.path.join(self.target_source_dir, 'bin', 'files', 'splash_screen.svg')
        self.modify_file(splash_screen_svg_path)
        self.convert_svg_to_png(splash_screen_svg_path, remove_source=True)
        self.log('Splash screen image generated')

    def generate_help_images(self):
        self.log('Start generating help images...')
        images_source_dir = os.path.join(self.target_source_dir, 'images')
        images_target_dir = os.path.join(self.target_source_dir, 'bin', 'HTML', 'static', 'images')
        for item in os.listdir(images_source_dir):
            item_path = os.path.join(images_source_dir, item)
            if os.path.isfile(item_path) and item_path.endswith('.svg'):
                name, _ = os.path.splitext(item)
                self.convert_svg_to_png(item_path, os.path.join(images_target_dir, '{}.png'.format(name)))
        self.log('Help images generated')

    def pack_sources(self):
        self.log('Start packing sources...')
        with tarfile.open(os.path.join(self.target_root_dir, 'Source.tar.gz'), "w:gz") as tar:
            tar.add(self.target_source_dir, arcname='Source')
        self.log('Sources packed')

    def build_binaries(self):
        self.log('Start building binaries...')
        os.chdir(os.path.join(self.target_source_dir, 'bin'))

        process = subprocess.Popen(
            r'python C:\PyInstaller-2.1\pyinstaller.py -w -n MediaDock MediaDock.py --icon="%s"' % self.icon_path,
            stdout=self.log_file, stderr=self.log_file)
        process.wait()

        time.sleep(10)

        shutil.copytree(os.path.join(self.target_source_dir, 'bin\\HTML'),
                        os.path.join(self.target_source_dir, 'bin\\dist\\MediaDock\\HTML'))
        shutil.copytree(os.path.join(self.target_source_dir, 'bin\\files'),
                        os.path.join(self.target_source_dir, 'bin\\dist\\MediaDock\\files'))

        shutil.copyfile(os.path.join(self.target_source_dir, 'bin\\RemoveDrive.exe'),
                        os.path.join(self.target_source_dir, 'bin\\dist\\MediaDock\\RemoveDrive.exe'))

        # remove files
        os.remove(os.path.join(self.target_source_dir, 'bin\\dist\\MediaDock\\Microsoft.VC90.CRT.manifest'))
        os.remove(os.path.join(self.target_source_dir, 'bin\\dist\\MediaDock\\msvcm90.dll'))
        os.remove(os.path.join(self.target_source_dir, 'bin\\dist\\MediaDock\\msvcp90.dll'))
        os.remove(os.path.join(self.target_source_dir, 'bin\\dist\\MediaDock\\msvcr90.dll'))

        # remove files
        shutil.rmtree(os.path.join(self.target_source_dir, 'bin\\dist\\MediaDock\\_MEI'))
        shutil.rmtree(os.path.join(self.target_source_dir, 'bin\\dist\\MediaDock\\Include'))

        os.chdir(script_dir)
        self.log('Build binaries finished')

    def copy_licenses(self):
        self.log('Start copy licenses...')
        shutil.copytree(os.path.join(self.target_source_dir, 'licenses'),
                        os.path.join(self.target_source_dir, 'bin\\dist\\MediaDock\\Lizenzen'))
        self.log('Finished copy licenses')

    def create_release_information_file(self):
        self.log('Start create release information file')
        info_file = open(os.path.join(self.target_root_dir, 'MediaDock'), 'w')
        json.dump({'Version': self.version}, info_file)
        info_file.close()
        self.log('Finished creating release information file')

    def pack_binaries(self):
        self.log('Start packing binaries...')
        with tarfile.open(os.path.join(self.target_root_dir, 'Binaries.tar.gz'), "w:gz") as tar:
            tar.add(os.path.join(self.target_source_dir, 'bin\\dist\\MediaDock'), arcname='Binaries')
        self.log('Finished packing binaries')

    def create_installer(self):
        self.log('Start creating installer...')
        self.modify_file(os.path.join(script_dir, 'InstallerScript.iss'),
                         os.path.join(self.target_root_dir, 'InstallerScript.iss'),
                         'latin-1')

        process = subprocess.Popen('iscc.exe "%s"' % os.path.join(self.target_root_dir, 'InstallerScript.iss'),
                                   stdout=self.log_file, stderr=self.log_file)
        process.wait()
        self.log('Finished creating installer')

    def remove_sources(self):
        self.log('Start deleting sources...')
        shutil.rmtree(self.target_source_dir)
        self.log('Finished deleting sources')

    def build(self):
        self.log('Start creating build...')

        self.check_platform()

        self.ask_nightly_build()
        self.get_build_number()
        self.get_version()

        self.generate_paths()

        self.copy_sources()

        # replace version, ...
        self.modify_sources()

        self.compile_less()

        # create splash screen png from svg
        self.generate_splash_screen()

        # create help images png files from svg
        self.generate_help_images()

        # pack sources for archive
        self.pack_sources()

        # build binaries
        self.build_binaries()

        # copy licenses
        self.copy_licenses()

        # create MediaDock file (installation info)
        self.create_release_information_file()

        # pack binaries
        self.pack_binaries()

        # create installer
        self.create_installer()

        # remove source dir
        self.remove_sources()

        self.log('Build finished')


class GitHandler(object):

    @classmethod
    def get_ignored_files_from_dir(cls, directory):
        ignored_files = list()
        cwd = os.getcwdu()

        os.chdir(directory)
        try:
            command = 'git status --ignored'
            process = subprocess.Popen(command, stdout=subprocess.PIPE)
            process.wait()
            result = process.stdout.readlines()
            for line in result:
                if line.startswith('\t'):
                    candidate = line.strip()
                    if not candidate.startswith('.'):
                        ignored_files.append(candidate)
        except:
            pass
        os.chdir(cwd)

        return ignored_files


if __name__ == '__main__':
    builder = Builder()
    builder.build()
