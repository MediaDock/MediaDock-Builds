Version 2.0, Januar 2004

http://www.apache.org/licenses/

BEDINGUNGEN F�R DIE NUTZUNG, VERVIELF�LTIGUNG UND VERBREITUNG

1. Definitionen.

�Lizenz� bezeichnet die Bedingungen f�r die Nutzung, Vervielf�ltigung und Verbreitung gem�� Definition in den
Abschnitten 1 bis 9 dieses Dokuments.

�Lizenzgeber� bezeichnet den Urheberrechtsinhaber oder den vom Urheberrechtsinhaber autorisierten Rechtstr�ger, der die
Lizenz gew�hrt.

�Juristische Person� bezeichnet die Vereinigung des handelnden Rechtstr�gers und aller anderen Rechtstr�ger, die diesen
Rechtstr�ger kontrollieren, von diesem kontrolliert werden oder mit diesem unter gemeinsamer Kontrolle stehen. Im Sinne
dieser Definition bedeutet �Kontrolle� (i) die Befugnis, direkt oder indirekt auf vertraglicher oder sonstiger Grundlage
die F�hrung oder Leitung dieses Rechtstr�gers zu veranlassen, (ii) Eigentum von mindestens f�nfzig Prozent (50 %) der
ausgegebenen Anteile oder (iii) Nie�brauch dieses Rechtstr�gers.

�Sie� (oder �Ihr�) bezeichnet eine nat�rliche oder juristische Person, welche die durch diese Lizenz gew�hrten
Berechtigungen aus�bt.

�Quellform� bezeichnet die bevorzugte Form f�r die Durchf�hrung von �nderungen, insbesondere Softwarequellcode,
Dokumentationsquelle und Konfigurationsdateien.

�Objektform� bezeichnet jede Form, die aus der mechanischen Umwandlung oder �bersetzung einer Quellform entsteht,
insbesondere kompilierter Objektcode, erzeugte Dokumentation und Konvertierung in andere Medienarten.

�Werk� bezeichnet die urheberrechtlichen Werke in Quell- oder Objektform, die im Rahmen der Lizenz gem�� einem in das
Werk eingef�gten oder daran angeh�ngten Urheberrechtshinweis zur Verf�gung gestellt werden (siehe Beispiel im
nachstehenden Anhang).

�Bearbeitungen� bezeichnet Werke in Quell- oder Objektform, die auf den Werken basieren (oder davon abgeleitet sind) und
bei dem die redaktionellen �berarbeitungen, Kommentare, Ausarbeitungen oder sonstigen �nderungen zusammen ein
urheberrechtliches Originalwerk darstellen. Im Sinne dieser Lizenz umfassen Bearbeitungen keine Werke, die sich vom Werk
und zugeh�rigen Bearbeitungen trennen lassen oder lediglich zu Schnittstellen des Werks oder zugeh�rigen Bearbeitungen
verlinken (oder per Name-Binding damit verbinden).

�Beitrag� bezeichnet jedes urheberrechtliche Werk, einschlie�lich der Originalversion des Werks und jeglicher �nderungen
an oder Zus�tze zu diesem Werk sowie Bearbeitungen des Werks, das der Urheberrechtsinhaber oder eine nat�rliche oder
juristische Person, die zur Einreichung im Auftrag des Urheberrechtsinhabers befugt ist, dem Lizenzgeber bewusst zur
Aufnahme in das Werk einreicht. Im Sinne dieser Definition bedeutet �eingereicht� jede Form von elektronischer, verbaler
oder schriftlicher Mitteilung, die zur Besprechung oder Verbesserung des Werks an den Lizenzgeber oder seine
Stellvertreter �bermittelt wird, insbesondere Mitteilungen �ber elektronische Mailinglisten, Quellcode-Kontrollsysteme
und Issue-Tracking-Systeme; ausgeschlossen sind jedoch Mitteilungen, die vom Urheberrechtsinhaber deutlich als �Kein
Beitrag� gekennzeichnet oder anderweitig schriftlich entsprechend bezeichnet sind.

�Beitragsleistender� bezeichnet den Lizenzgeber und jede nat�rliche oder juristische Person, in deren Auftrag der
Lizenzgeber einen Beitrag erh�lt, der anschlie�end in das Werk aufgenommen wurde.

2. Gew�hrung einer Urheberrechtslizenz. Vorbehaltlich den Bedingungen dieser Lizenz gew�hrt Ihnen hiermit jeder
Beitragsleistende eine unbefristete, weltweite, nicht ausschlie�liche, kostenlose, geb�hrenfreie, unwiderrufliche
Urheberrechtslizenz zur Vervielf�ltigung, Anfertigung von Bearbeitungen, zur �ffentlichen Ausstellung, Auff�hrung,
Unterlizenzierung und Verbreitung des Werks und derartiger Bearbeitungen in Quell- oder Objektform.

3. Gew�hrung einer Patentlizenz. Vorbehaltlich den Bedingungen dieser Lizenz gew�hrt Ihnen hiermit jeder
Beitragsleistende eine unbefristete, weltweite, nicht ausschlie�liche, kostenlose, geb�hrenfreie, unwiderrufliche
(ausgenommen gem�� den Angaben in diesem Abschnitt) Patentlizenz, um das Werk herzustellen, herstellen zu lassen, zu
verwenden, es zum Verkauf anzubieten, zu verkaufen, zu importieren und anderweitig zu �bertragen, wobei diese Lizenz nur
f�r Patentanspr�che von Beitragsleistenden gilt, sofern deren Beitr�ge allein oder die Kombination ihrer Beitr�ge mit
dem Werk, f�r das diese Beitr�ge eingereicht wurden, dieses Patent verletzen. Falls Sie gegen einen Rechtstr�ger ein
gerichtliches Patentverfahren einleiten (einschlie�lich Gegenforderung oder Gegenklage in einem Rechtsstreit) und dabei
vorbringen, dass das Werk oder ein in das Werk eingearbeiteter Beitrag eine direkte Patentverletzung oder einen dazu
beitragenden Faktor darstellt, so enden alle Patentlizenzen, die Ihnen im Rahmen dieser Lizenz f�r dieses Werk gew�hrt
wurden, mit dem Datum, an dem diese Klage eingereicht wird.

4. Weiterverbreitung. Sie d�rfen Kopien des Werks oder von Bearbeitungen auf jedem Medium, mit oder ohne �nderungen und
in Quell- oder Objektform vervielf�ltigen und verbreiten, vorausgesetzt, Sie erf�llen die folgenden Bedingungen:

    a) Sie m�ssen allen anderen Empf�ngern des Werks oder von Bearbeitungen eine Kopie dieser Lizenz �bergeben.

    b) Sie m�ssen veranlassen, dass ge�nderte Dateien auff�llige Hinweise darauf enthalten, dass Sie die Dateien
    ge�ndert haben.

    c) Sie m�ssen in der Quellform aller von Ihnen verbreiteten Bearbeitungen alle Urheberrechts-, Patent-, Marken- und
    Namensnennungshinweise aus der Quellform des Werks beibehalten, ausgenommen jedoch die Hinweise, die zu keinem Teil
    der Bearbeitungen geh�ren.

    d) Wenn das Werk als Teil der Verbreitung eine Textdatei namens �NOTICE� (Hinweis) enth�lt, so m�ssen alle von Ihnen
    verbreiteten Bearbeitungen an mindestens einer der folgenden Stellen eine lesbare Kopie des Namensnennungshinweises
    enthalten, der in dieser NOTICE-Datei enthalten ist, ausgenommen jedoch die Hinweise, die zu keinem Teil der
    Bearbeitungen geh�ren: in der Quellform oder Dokumentation, falls mit den Bearbeitungen bereitgestellt, oder in
    einer durch die Bearbeitungen erzeugten Anzeige, sofern und wo solche Hinweise Dritter normalerweise erscheinen.
    Der Inhalt der NOTICE-Datei dient nur Informationszwecken und stellt keine �nderung der Lizenz dar. Sie k�nnen den
    von Ihnen verbreiteten Bearbeitungen eigene Namensnennungshinweise hinzuf�gen, zus�tzlich oder erg�nzend zu dem
    NOTICE-Text aus dem Werk, vorausgesetzt, dass diese zus�tzlichen Namensnennungshinweise nicht als �nderung der
    Lizenz ausgelegt werden k�nnen.

    Sie k�nnen Ihren �nderungen eigene Urheberrechtshinweise hinzuf�gen und zus�tzliche
    oder andere Lizenzbedingungen und Bedingungen f�r die Nutzung, Vervielf�ltigung oder Verbreitung Ihrer �nderungen
    oder f�r diese Bearbeitungen als Ganzes angeben, vorausgesetzt, dass Ihre Nutzung, Vervielf�ltigung und Verbreitung
    des Werks ansonsten den in dieser Lizenz angegebenen Bedingungen entspricht.

5. Einreichung von Beitr�gen. Sofern nichts ausdr�cklich anderes angegeben, unterliegt jeder Beitrag, den Sie dem
Lizenzgeber bewusst zur Aufnahme in das Werk eingereicht haben, den Bedingungen dieser Lizenz, ohne dass zus�tzliche
Bedingungen gelten. Ungeachtet des Vorstehenden ersetzt oder �ndert keine der hierin enthaltenen Bestimmungen die
Bedingungen einer separaten Lizenzvereinbarung, die Sie m�glicherweise mit dem Lizenzgeber f�r solche Beitr�ge
abgeschlossen haben.

6. Marken. Mit dieser Lizenz wird keine Genehmigung zur Nutzung der Handelsnamen, Marken, Dienstleistungsmarken oder
Produktnamen des Lizenzgebers erteilt, mit Ausnahme der Erfordernisse der angemessenen und �blichen Nutzung zur
Beschreibung der Herkunft des Werks und zur Wiedergabe des Inhaltes der NOTICE-Datei.

7. Gew�hrleistungsausschluss. Sofern nicht gem�� geltendem Recht erforderlich oder schriftlich vereinbart, stellt der
Lizenzgeber das Werk (und stellt jeder Beitragsleistende seine Beitr�ge) WIE BESEHEN OHNE GEW�HR ODER VORBEHALTE �
ganz gleich, ob ausdr�cklich oder stillschweigend � bereit, insbesondere Gew�hrleistungen oder Vorbehalten des
EIGENTUMS, NICHTVERLETZUNG VON RECHTEN DRITTER, HANDELS�BLICHKEIT oder EIGNUNG F�R EINEN BESTIMMTEN ZWECK. Sie allein
sind verantwortlich f�r die Beurteilung, ob die Nutzung oder Weiterverbreitung des Werks angemessen ist, und �bernehmen
die Risiken, die mit Ihrer Aus�bung der Genehmigungen gem�� dieser Lizenz verbunden sind.

8. Haftungsbeschr�nkung. In keinem Fall und auf keiner Rechtsgrundlage, sei es aufgrund unerlaubter Handlung
(einschlie�lich Fahrl�ssigkeit), Vertrag, oder anderer Grundlage, soweit nicht gem�� geltendem Recht vorgeschrieben
(z. B. absichtliche und grob fahrl�ssige Handlungen) oder schriftlich vereinbart, haftet der Beitragsleistende Ihnen
gegen�ber f�r Sch�den, einschlie�lich direkter, indirekter, konkreter, beil�ufig entstandener Sch�den oder Folgesch�den
jeglicher Art, die infolge dieser Lizenz oder aufgrund der Nutzung oder der Unf�higkeit zur Nutzung des Werks entstehen
(insbesondere Sch�den durch Verlust des Firmenwerts, Arbeitsunterbrechung, Computerausfall oder Betriebsst�rung oder
alle sonstigen wirtschaftlichen Sch�den oder Verluste), selbst dann, wenn diese Beitragsleistenden auf die M�glichkeit
solcher Sch�den hingewiesen wurden.

9. �bernahme von Gew�hrleistung oder zus�tzlicher Haftung. Bei der Weiterverbreitung des Werks oder der Bearbeitungen
desselben steht es Ihnen frei, die �bernahme von Support, Gew�hrleistung, Schadenersatz oder sonstiger
Haftungsverpflichtungen und/oder Rechte gem�� dieser Lizenz anzubieten und eine Geb�hr daf�r zu erheben. Bei der
�bernahme solcher Verpflichtungen k�nnen Sie jedoch nur in eigenem Namen und auf eigene Verantwortung handeln, nicht
jedoch im Namen anderer Beitragsleistender, und nur dann, wenn Sie einwilligen, jeden Beitragsleistenden zu
entsch�digen, zu verteidigen, und von jeder Haftung, die durch diesen Beitragsleistenden aufgrund der �bernahme dieser
Gew�hrleistung oder zus�tzlicher Haftung eingegangen wird, oder von gegen ihn erhobenen Anspr�chen, schadlos zu halten.

ENDE DER LIZENZBEDINGUNGEN
ANHANG: Anwendung der Apache-Lizenz auf Ihr Werk

Zur Anwendung der Apache-Lizenz auf Ihr Werk h�ngen Sie ihm den folgenden Standardhinweis an, wobei Sie die von eckigen
Klammern �[]� eingeschlossenen Felder durch Ihre eigenen Angaben ersetzen. (Die eckigen Klammern dabei weglassen!) Der
Text muss von der entsprechenden Kommentarsyntax f�r das Dateiformat umschlossen sein. Wir empfehlen au�erdem, einen
Datei- oder Klassennamen und eine Beschreibung des Zwecks auf derselben Druckseite wie der Urheberrechtshinweis
anzugeben, damit das Auffinden in den Archiven Dritter erleichtert wird.

  Copyright [JJJJ] [Name des Urheberrechtsinhabers]

   Lizenziert gem�� Apache Licence Version 2.0 (die �Lizenz�); Nutzung dieser Datei nur in �bereinstimmung mit der
   Lizenz erlaubt.
   Eine Kopie der Lizenz erhalten Sie auf http://www.apache.org/licenses/LICENSE-2.0.

   Sofern nicht gem�� geltendem Recht vorgeschrieben oder schriftlich vereinbart, erfolgt die Bereitstellung der im
   Rahmen der Lizenz verbreiteten Software OHNE GEW�HR ODER VORBEHALTE � ganz gleich, ob ausdr�cklich oder
   stillschweigend.
   Informationen �ber die jeweiligen Bedingungen f�r Genehmigungen und Einschr�nkungen im Rahmen der Lizenz finden Sie
   in der Lizenz.
